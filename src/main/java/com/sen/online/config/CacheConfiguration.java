package com.sen.online.config;

import java.time.Duration;
import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.*;
import tech.jhipster.config.JHipsterProperties;
import tech.jhipster.config.cache.PrefixedKeyGenerator;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private GitProperties gitProperties;
    private BuildProperties buildProperties;
    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration =
            Eh107Configuration.fromEhcacheCacheConfiguration(
                CacheConfigurationBuilder
                    .newCacheConfigurationBuilder(Object.class, Object.class, ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                    .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                    .build()
            );
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, com.sen.online.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, com.sen.online.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, com.sen.online.domain.RefCustomerTypes.class.getName());
            createCache(cm, com.sen.online.domain.RefCustomerTypes.class.getName() + ".customers");
            createCache(cm, com.sen.online.domain.RefAddressTypes.class.getName());
            createCache(cm, com.sen.online.domain.RefAddressTypes.class.getName() + ".addresses");
            createCache(cm, com.sen.online.domain.RefAccountTypes.class.getName());
            createCache(cm, com.sen.online.domain.RefAccountTypes.class.getName() + ".accounts");
            createCache(cm, com.sen.online.domain.RefTransactionTypes.class.getName());
            createCache(cm, com.sen.online.domain.RefTransactionTypes.class.getName() + ".transactions");
            createCache(cm, com.sen.online.domain.Transactions.class.getName());
            createCache(cm, com.sen.online.domain.Customers.class.getName());
            createCache(cm, com.sen.online.domain.Customers.class.getName() + ".accounts");
            createCache(cm, com.sen.online.domain.Customers.class.getName() + ".customerAddresses");
            createCache(cm, com.sen.online.domain.Customers.class.getName() + ".customerPurchases");
            createCache(cm, com.sen.online.domain.Addresses.class.getName());
            createCache(cm, com.sen.online.domain.Addresses.class.getName() + ".customerAddresses");
            createCache(cm, com.sen.online.domain.Accounts.class.getName());
            createCache(cm, com.sen.online.domain.Accounts.class.getName() + ".balanceHistories");
            createCache(cm, com.sen.online.domain.Accounts.class.getName() + ".transactions");
            createCache(cm, com.sen.online.domain.CustomerAddresses.class.getName());
            createCache(cm, com.sen.online.domain.CustomerPurchase.class.getName());
            createCache(cm, com.sen.online.domain.CustomerPurchase.class.getName() + ".transactions");
            createCache(cm, com.sen.online.domain.Merchants.class.getName());
            createCache(cm, com.sen.online.domain.BalanceHistory.class.getName());
            createCache(cm, com.sen.online.domain.ProductAndServices.class.getName());
            createCache(cm, com.sen.online.domain.ProductAndServices.class.getName() + ".customerPurchases");
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cache.clear();
        } else {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

    @Autowired(required = false)
    public void setGitProperties(GitProperties gitProperties) {
        this.gitProperties = gitProperties;
    }

    @Autowired(required = false)
    public void setBuildProperties(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new PrefixedKeyGenerator(this.gitProperties, this.buildProperties);
    }
}
