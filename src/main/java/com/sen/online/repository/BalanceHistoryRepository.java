package com.sen.online.repository;

import com.sen.online.domain.BalanceHistory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the BalanceHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BalanceHistoryRepository extends JpaRepository<BalanceHistory, Long> {}
