package com.sen.online.repository;

import com.sen.online.domain.RefCustomerTypes;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the RefCustomerTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RefCustomerTypesRepository extends JpaRepository<RefCustomerTypes, Long> {}
