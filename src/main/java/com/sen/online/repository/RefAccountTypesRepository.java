package com.sen.online.repository;

import com.sen.online.domain.RefAccountTypes;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the RefAccountTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RefAccountTypesRepository extends JpaRepository<RefAccountTypes, Long> {}
