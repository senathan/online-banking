package com.sen.online.repository;

import com.sen.online.domain.CustomerAddresses;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the CustomerAddresses entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerAddressesRepository extends JpaRepository<CustomerAddresses, Long> {}
