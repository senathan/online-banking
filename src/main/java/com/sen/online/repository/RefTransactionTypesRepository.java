package com.sen.online.repository;

import com.sen.online.domain.RefTransactionTypes;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the RefTransactionTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RefTransactionTypesRepository extends JpaRepository<RefTransactionTypes, Long> {}
