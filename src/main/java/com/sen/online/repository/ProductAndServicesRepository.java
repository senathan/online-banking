package com.sen.online.repository;

import com.sen.online.domain.ProductAndServices;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ProductAndServices entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductAndServicesRepository extends JpaRepository<ProductAndServices, Long> {}
