package com.sen.online.repository;

import com.sen.online.domain.Merchants;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Merchants entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MerchantsRepository extends JpaRepository<Merchants, Long> {}
