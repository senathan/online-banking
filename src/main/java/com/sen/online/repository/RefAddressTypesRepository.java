package com.sen.online.repository;

import com.sen.online.domain.RefAddressTypes;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the RefAddressTypes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RefAddressTypesRepository extends JpaRepository<RefAddressTypes, Long> {}
