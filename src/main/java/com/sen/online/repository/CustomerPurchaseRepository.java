package com.sen.online.repository;

import com.sen.online.domain.CustomerPurchase;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the CustomerPurchase entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerPurchaseRepository extends JpaRepository<CustomerPurchase, Long> {}
