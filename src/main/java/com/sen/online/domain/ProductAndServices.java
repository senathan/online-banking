package com.sen.online.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ProductAndServices.
 */
@Entity
@Table(name = "product_and_services")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ProductAndServices implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "productAndServices")
    @JsonIgnoreProperties(value = { "customers", "productAndServices", "transactions" }, allowSetters = true)
    private Set<CustomerPurchase> customerPurchases = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ProductAndServices id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<CustomerPurchase> getCustomerPurchases() {
        return this.customerPurchases;
    }

    public void setCustomerPurchases(Set<CustomerPurchase> customerPurchases) {
        if (this.customerPurchases != null) {
            this.customerPurchases.forEach(i -> i.setProductAndServices(null));
        }
        if (customerPurchases != null) {
            customerPurchases.forEach(i -> i.setProductAndServices(this));
        }
        this.customerPurchases = customerPurchases;
    }

    public ProductAndServices customerPurchases(Set<CustomerPurchase> customerPurchases) {
        this.setCustomerPurchases(customerPurchases);
        return this;
    }

    public ProductAndServices addCustomerPurchase(CustomerPurchase customerPurchase) {
        this.customerPurchases.add(customerPurchase);
        customerPurchase.setProductAndServices(this);
        return this;
    }

    public ProductAndServices removeCustomerPurchase(CustomerPurchase customerPurchase) {
        this.customerPurchases.remove(customerPurchase);
        customerPurchase.setProductAndServices(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductAndServices)) {
            return false;
        }
        return getId() != null && getId().equals(((ProductAndServices) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductAndServices{" +
            "id=" + getId() +
            "}";
    }
}
