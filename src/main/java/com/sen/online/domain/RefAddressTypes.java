package com.sen.online.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A RefAddressTypes.
 */
@Entity
@Table(name = "ref_address_types")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class RefAddressTypes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "address_type_code", nullable = false)
    private String addressTypeCode;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
    @JsonIgnoreProperties(value = { "address", "customerAddresses" }, allowSetters = true)
    private Set<Addresses> addresses = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public RefAddressTypes id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddressTypeCode() {
        return this.addressTypeCode;
    }

    public RefAddressTypes addressTypeCode(String addressTypeCode) {
        this.setAddressTypeCode(addressTypeCode);
        return this;
    }

    public void setAddressTypeCode(String addressTypeCode) {
        this.addressTypeCode = addressTypeCode;
    }

    public Set<Addresses> getAddresses() {
        return this.addresses;
    }

    public void setAddresses(Set<Addresses> addresses) {
        if (this.addresses != null) {
            this.addresses.forEach(i -> i.setAddress(null));
        }
        if (addresses != null) {
            addresses.forEach(i -> i.setAddress(this));
        }
        this.addresses = addresses;
    }

    public RefAddressTypes addresses(Set<Addresses> addresses) {
        this.setAddresses(addresses);
        return this;
    }

    public RefAddressTypes addAddresses(Addresses addresses) {
        this.addresses.add(addresses);
        addresses.setAddress(this);
        return this;
    }

    public RefAddressTypes removeAddresses(Addresses addresses) {
        this.addresses.remove(addresses);
        addresses.setAddress(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RefAddressTypes)) {
            return false;
        }
        return getId() != null && getId().equals(((RefAddressTypes) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RefAddressTypes{" +
            "id=" + getId() +
            ", addressTypeCode='" + getAddressTypeCode() + "'" +
            "}";
    }
}
