package com.sen.online.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A RefTransactionTypes.
 */
@Entity
@Table(name = "ref_transaction_types")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class RefTransactionTypes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "transaction_type_code", nullable = false)
    private String transactionTypeCode;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transaction")
    @JsonIgnoreProperties(value = { "accounts", "customerPurchase", "transaction" }, allowSetters = true)
    private Set<Transactions> transactions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public RefTransactionTypes id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionTypeCode() {
        return this.transactionTypeCode;
    }

    public RefTransactionTypes transactionTypeCode(String transactionTypeCode) {
        this.setTransactionTypeCode(transactionTypeCode);
        return this;
    }

    public void setTransactionTypeCode(String transactionTypeCode) {
        this.transactionTypeCode = transactionTypeCode;
    }

    public Set<Transactions> getTransactions() {
        return this.transactions;
    }

    public void setTransactions(Set<Transactions> transactions) {
        if (this.transactions != null) {
            this.transactions.forEach(i -> i.setTransaction(null));
        }
        if (transactions != null) {
            transactions.forEach(i -> i.setTransaction(this));
        }
        this.transactions = transactions;
    }

    public RefTransactionTypes transactions(Set<Transactions> transactions) {
        this.setTransactions(transactions);
        return this;
    }

    public RefTransactionTypes addTransactions(Transactions transactions) {
        this.transactions.add(transactions);
        transactions.setTransaction(this);
        return this;
    }

    public RefTransactionTypes removeTransactions(Transactions transactions) {
        this.transactions.remove(transactions);
        transactions.setTransaction(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RefTransactionTypes)) {
            return false;
        }
        return getId() != null && getId().equals(((RefTransactionTypes) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RefTransactionTypes{" +
            "id=" + getId() +
            ", transactionTypeCode='" + getTransactionTypeCode() + "'" +
            "}";
    }
}
