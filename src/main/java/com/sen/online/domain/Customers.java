package com.sen.online.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Customers.
 */
@Entity
@Table(name = "customers")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Customers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "customer_name")
    private String customerName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customers")
    @JsonIgnoreProperties(value = { "account", "customers", "balanceHistories", "transactions" }, allowSetters = true)
    private Set<Accounts> accounts = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customers")
    @JsonIgnoreProperties(value = { "addresses", "customers" }, allowSetters = true)
    private Set<CustomerAddresses> customerAddresses = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customers")
    @JsonIgnoreProperties(value = { "customers", "productAndServices", "transactions" }, allowSetters = true)
    private Set<CustomerPurchase> customerPurchases = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "customers" }, allowSetters = true)
    private RefCustomerTypes customer;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Customers id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public Customers customerName(String customerName) {
        this.setCustomerName(customerName);
        return this;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Set<Accounts> getAccounts() {
        return this.accounts;
    }

    public void setAccounts(Set<Accounts> accounts) {
        if (this.accounts != null) {
            this.accounts.forEach(i -> i.setCustomers(null));
        }
        if (accounts != null) {
            accounts.forEach(i -> i.setCustomers(this));
        }
        this.accounts = accounts;
    }

    public Customers accounts(Set<Accounts> accounts) {
        this.setAccounts(accounts);
        return this;
    }

    public Customers addAccounts(Accounts accounts) {
        this.accounts.add(accounts);
        accounts.setCustomers(this);
        return this;
    }

    public Customers removeAccounts(Accounts accounts) {
        this.accounts.remove(accounts);
        accounts.setCustomers(null);
        return this;
    }

    public Set<CustomerAddresses> getCustomerAddresses() {
        return this.customerAddresses;
    }

    public void setCustomerAddresses(Set<CustomerAddresses> customerAddresses) {
        if (this.customerAddresses != null) {
            this.customerAddresses.forEach(i -> i.setCustomers(null));
        }
        if (customerAddresses != null) {
            customerAddresses.forEach(i -> i.setCustomers(this));
        }
        this.customerAddresses = customerAddresses;
    }

    public Customers customerAddresses(Set<CustomerAddresses> customerAddresses) {
        this.setCustomerAddresses(customerAddresses);
        return this;
    }

    public Customers addCustomerAddresses(CustomerAddresses customerAddresses) {
        this.customerAddresses.add(customerAddresses);
        customerAddresses.setCustomers(this);
        return this;
    }

    public Customers removeCustomerAddresses(CustomerAddresses customerAddresses) {
        this.customerAddresses.remove(customerAddresses);
        customerAddresses.setCustomers(null);
        return this;
    }

    public Set<CustomerPurchase> getCustomerPurchases() {
        return this.customerPurchases;
    }

    public void setCustomerPurchases(Set<CustomerPurchase> customerPurchases) {
        if (this.customerPurchases != null) {
            this.customerPurchases.forEach(i -> i.setCustomers(null));
        }
        if (customerPurchases != null) {
            customerPurchases.forEach(i -> i.setCustomers(this));
        }
        this.customerPurchases = customerPurchases;
    }

    public Customers customerPurchases(Set<CustomerPurchase> customerPurchases) {
        this.setCustomerPurchases(customerPurchases);
        return this;
    }

    public Customers addCustomerPurchase(CustomerPurchase customerPurchase) {
        this.customerPurchases.add(customerPurchase);
        customerPurchase.setCustomers(this);
        return this;
    }

    public Customers removeCustomerPurchase(CustomerPurchase customerPurchase) {
        this.customerPurchases.remove(customerPurchase);
        customerPurchase.setCustomers(null);
        return this;
    }

    public RefCustomerTypes getCustomer() {
        return this.customer;
    }

    public void setCustomer(RefCustomerTypes refCustomerTypes) {
        this.customer = refCustomerTypes;
    }

    public Customers customer(RefCustomerTypes refCustomerTypes) {
        this.setCustomer(refCustomerTypes);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Customers)) {
            return false;
        }
        return getId() != null && getId().equals(((Customers) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Customers{" +
            "id=" + getId() +
            ", customerName='" + getCustomerName() + "'" +
            "}";
    }
}
