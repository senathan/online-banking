package com.sen.online.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A CustomerPurchase.
 */
@Entity
@Table(name = "customer_purchase")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CustomerPurchase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "accounts", "customerAddresses", "customerPurchases", "customer" }, allowSetters = true)
    private Customers customers;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "customerPurchases" }, allowSetters = true)
    private ProductAndServices productAndServices;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customerPurchase")
    @JsonIgnoreProperties(value = { "accounts", "customerPurchase", "transaction" }, allowSetters = true)
    private Set<Transactions> transactions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CustomerPurchase id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customers getCustomers() {
        return this.customers;
    }

    public void setCustomers(Customers customers) {
        this.customers = customers;
    }

    public CustomerPurchase customers(Customers customers) {
        this.setCustomers(customers);
        return this;
    }

    public ProductAndServices getProductAndServices() {
        return this.productAndServices;
    }

    public void setProductAndServices(ProductAndServices productAndServices) {
        this.productAndServices = productAndServices;
    }

    public CustomerPurchase productAndServices(ProductAndServices productAndServices) {
        this.setProductAndServices(productAndServices);
        return this;
    }

    public Set<Transactions> getTransactions() {
        return this.transactions;
    }

    public void setTransactions(Set<Transactions> transactions) {
        if (this.transactions != null) {
            this.transactions.forEach(i -> i.setCustomerPurchase(null));
        }
        if (transactions != null) {
            transactions.forEach(i -> i.setCustomerPurchase(this));
        }
        this.transactions = transactions;
    }

    public CustomerPurchase transactions(Set<Transactions> transactions) {
        this.setTransactions(transactions);
        return this;
    }

    public CustomerPurchase addTransactions(Transactions transactions) {
        this.transactions.add(transactions);
        transactions.setCustomerPurchase(this);
        return this;
    }

    public CustomerPurchase removeTransactions(Transactions transactions) {
        this.transactions.remove(transactions);
        transactions.setCustomerPurchase(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CustomerPurchase)) {
            return false;
        }
        return getId() != null && getId().equals(((CustomerPurchase) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CustomerPurchase{" +
            "id=" + getId() +
            "}";
    }
}
