package com.sen.online.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Accounts.
 */
@Entity
@Table(name = "accounts")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Accounts implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "account_desc")
    private String accountDesc;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "accounts" }, allowSetters = true)
    private RefAccountTypes account;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "accounts", "customerAddresses", "customerPurchases", "customer" }, allowSetters = true)
    private Customers customers;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "accounts")
    @JsonIgnoreProperties(value = { "accounts" }, allowSetters = true)
    private Set<BalanceHistory> balanceHistories = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "accounts")
    @JsonIgnoreProperties(value = { "accounts", "customerPurchase", "transaction" }, allowSetters = true)
    private Set<Transactions> transactions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Accounts id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountDesc() {
        return this.accountDesc;
    }

    public Accounts accountDesc(String accountDesc) {
        this.setAccountDesc(accountDesc);
        return this;
    }

    public void setAccountDesc(String accountDesc) {
        this.accountDesc = accountDesc;
    }

    public RefAccountTypes getAccount() {
        return this.account;
    }

    public void setAccount(RefAccountTypes refAccountTypes) {
        this.account = refAccountTypes;
    }

    public Accounts account(RefAccountTypes refAccountTypes) {
        this.setAccount(refAccountTypes);
        return this;
    }

    public Customers getCustomers() {
        return this.customers;
    }

    public void setCustomers(Customers customers) {
        this.customers = customers;
    }

    public Accounts customers(Customers customers) {
        this.setCustomers(customers);
        return this;
    }

    public Set<BalanceHistory> getBalanceHistories() {
        return this.balanceHistories;
    }

    public void setBalanceHistories(Set<BalanceHistory> balanceHistories) {
        if (this.balanceHistories != null) {
            this.balanceHistories.forEach(i -> i.setAccounts(null));
        }
        if (balanceHistories != null) {
            balanceHistories.forEach(i -> i.setAccounts(this));
        }
        this.balanceHistories = balanceHistories;
    }

    public Accounts balanceHistories(Set<BalanceHistory> balanceHistories) {
        this.setBalanceHistories(balanceHistories);
        return this;
    }

    public Accounts addBalanceHistory(BalanceHistory balanceHistory) {
        this.balanceHistories.add(balanceHistory);
        balanceHistory.setAccounts(this);
        return this;
    }

    public Accounts removeBalanceHistory(BalanceHistory balanceHistory) {
        this.balanceHistories.remove(balanceHistory);
        balanceHistory.setAccounts(null);
        return this;
    }

    public Set<Transactions> getTransactions() {
        return this.transactions;
    }

    public void setTransactions(Set<Transactions> transactions) {
        if (this.transactions != null) {
            this.transactions.forEach(i -> i.setAccounts(null));
        }
        if (transactions != null) {
            transactions.forEach(i -> i.setAccounts(this));
        }
        this.transactions = transactions;
    }

    public Accounts transactions(Set<Transactions> transactions) {
        this.setTransactions(transactions);
        return this;
    }

    public Accounts addTransactions(Transactions transactions) {
        this.transactions.add(transactions);
        transactions.setAccounts(this);
        return this;
    }

    public Accounts removeTransactions(Transactions transactions) {
        this.transactions.remove(transactions);
        transactions.setAccounts(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Accounts)) {
            return false;
        }
        return getId() != null && getId().equals(((Accounts) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Accounts{" +
            "id=" + getId() +
            ", accountDesc='" + getAccountDesc() + "'" +
            "}";
    }
}
