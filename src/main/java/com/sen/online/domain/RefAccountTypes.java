package com.sen.online.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A RefAccountTypes.
 */
@Entity
@Table(name = "ref_account_types")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class RefAccountTypes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "account_type_code", nullable = false)
    private String accountTypeCode;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    @JsonIgnoreProperties(value = { "account", "customers", "balanceHistories", "transactions" }, allowSetters = true)
    private Set<Accounts> accounts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public RefAccountTypes id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountTypeCode() {
        return this.accountTypeCode;
    }

    public RefAccountTypes accountTypeCode(String accountTypeCode) {
        this.setAccountTypeCode(accountTypeCode);
        return this;
    }

    public void setAccountTypeCode(String accountTypeCode) {
        this.accountTypeCode = accountTypeCode;
    }

    public Set<Accounts> getAccounts() {
        return this.accounts;
    }

    public void setAccounts(Set<Accounts> accounts) {
        if (this.accounts != null) {
            this.accounts.forEach(i -> i.setAccount(null));
        }
        if (accounts != null) {
            accounts.forEach(i -> i.setAccount(this));
        }
        this.accounts = accounts;
    }

    public RefAccountTypes accounts(Set<Accounts> accounts) {
        this.setAccounts(accounts);
        return this;
    }

    public RefAccountTypes addAccounts(Accounts accounts) {
        this.accounts.add(accounts);
        accounts.setAccount(this);
        return this;
    }

    public RefAccountTypes removeAccounts(Accounts accounts) {
        this.accounts.remove(accounts);
        accounts.setAccount(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RefAccountTypes)) {
            return false;
        }
        return getId() != null && getId().equals(((RefAccountTypes) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RefAccountTypes{" +
            "id=" + getId() +
            ", accountTypeCode='" + getAccountTypeCode() + "'" +
            "}";
    }
}
