package com.sen.online.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;

/**
 * A Transactions.
 */
@Entity
@Table(name = "transactions")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Transactions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "transaction_unique_code")
    private String transactionUniqueCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "account", "customers", "balanceHistories", "transactions" }, allowSetters = true)
    private Accounts accounts;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "customers", "productAndServices", "transactions" }, allowSetters = true)
    private CustomerPurchase customerPurchase;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "transactions" }, allowSetters = true)
    private RefTransactionTypes transaction;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Transactions id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionUniqueCode() {
        return this.transactionUniqueCode;
    }

    public Transactions transactionUniqueCode(String transactionUniqueCode) {
        this.setTransactionUniqueCode(transactionUniqueCode);
        return this;
    }

    public void setTransactionUniqueCode(String transactionUniqueCode) {
        this.transactionUniqueCode = transactionUniqueCode;
    }

    public Accounts getAccounts() {
        return this.accounts;
    }

    public void setAccounts(Accounts accounts) {
        this.accounts = accounts;
    }

    public Transactions accounts(Accounts accounts) {
        this.setAccounts(accounts);
        return this;
    }

    public CustomerPurchase getCustomerPurchase() {
        return this.customerPurchase;
    }

    public void setCustomerPurchase(CustomerPurchase customerPurchase) {
        this.customerPurchase = customerPurchase;
    }

    public Transactions customerPurchase(CustomerPurchase customerPurchase) {
        this.setCustomerPurchase(customerPurchase);
        return this;
    }

    public RefTransactionTypes getTransaction() {
        return this.transaction;
    }

    public void setTransaction(RefTransactionTypes refTransactionTypes) {
        this.transaction = refTransactionTypes;
    }

    public Transactions transaction(RefTransactionTypes refTransactionTypes) {
        this.setTransaction(refTransactionTypes);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Transactions)) {
            return false;
        }
        return getId() != null && getId().equals(((Transactions) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Transactions{" +
            "id=" + getId() +
            ", transactionUniqueCode='" + getTransactionUniqueCode() + "'" +
            "}";
    }
}
