package com.sen.online.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;

/**
 * A Merchants.
 */
@Entity
@Table(name = "merchants")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Merchants implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "customerPurchases" }, allowSetters = true)
    private ProductAndServices merchant;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Merchants id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductAndServices getMerchant() {
        return this.merchant;
    }

    public void setMerchant(ProductAndServices productAndServices) {
        this.merchant = productAndServices;
    }

    public Merchants merchant(ProductAndServices productAndServices) {
        this.setMerchant(productAndServices);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Merchants)) {
            return false;
        }
        return getId() != null && getId().equals(((Merchants) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Merchants{" +
            "id=" + getId() +
            "}";
    }
}
