package com.sen.online.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Addresses.
 */
@Entity
@Table(name = "addresses")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Addresses implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "address_line_1")
    private String addressLine1;

    @Column(name = "address_line_2")
    private String addressLine2;

    @Column(name = "postcode")
    private String postcode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "addresses" }, allowSetters = true)
    private RefAddressTypes address;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "addresses")
    @JsonIgnoreProperties(value = { "addresses", "customers" }, allowSetters = true)
    private Set<CustomerAddresses> customerAddresses = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Addresses id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddressLine1() {
        return this.addressLine1;
    }

    public Addresses addressLine1(String addressLine1) {
        this.setAddressLine1(addressLine1);
        return this;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return this.addressLine2;
    }

    public Addresses addressLine2(String addressLine2) {
        this.setAddressLine2(addressLine2);
        return this;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getPostcode() {
        return this.postcode;
    }

    public Addresses postcode(String postcode) {
        this.setPostcode(postcode);
        return this;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public RefAddressTypes getAddress() {
        return this.address;
    }

    public void setAddress(RefAddressTypes refAddressTypes) {
        this.address = refAddressTypes;
    }

    public Addresses address(RefAddressTypes refAddressTypes) {
        this.setAddress(refAddressTypes);
        return this;
    }

    public Set<CustomerAddresses> getCustomerAddresses() {
        return this.customerAddresses;
    }

    public void setCustomerAddresses(Set<CustomerAddresses> customerAddresses) {
        if (this.customerAddresses != null) {
            this.customerAddresses.forEach(i -> i.setAddresses(null));
        }
        if (customerAddresses != null) {
            customerAddresses.forEach(i -> i.setAddresses(this));
        }
        this.customerAddresses = customerAddresses;
    }

    public Addresses customerAddresses(Set<CustomerAddresses> customerAddresses) {
        this.setCustomerAddresses(customerAddresses);
        return this;
    }

    public Addresses addCustomerAddresses(CustomerAddresses customerAddresses) {
        this.customerAddresses.add(customerAddresses);
        customerAddresses.setAddresses(this);
        return this;
    }

    public Addresses removeCustomerAddresses(CustomerAddresses customerAddresses) {
        this.customerAddresses.remove(customerAddresses);
        customerAddresses.setAddresses(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Addresses)) {
            return false;
        }
        return getId() != null && getId().equals(((Addresses) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Addresses{" +
            "id=" + getId() +
            ", addressLine1='" + getAddressLine1() + "'" +
            ", addressLine2='" + getAddressLine2() + "'" +
            ", postcode='" + getPostcode() + "'" +
            "}";
    }
}
