package com.sen.online.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A RefCustomerTypes.
 */
@Entity
@Table(name = "ref_customer_types")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class RefCustomerTypes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "customer_type_code", nullable = false)
    private String customerTypeCode;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
    @JsonIgnoreProperties(value = { "accounts", "customerAddresses", "customerPurchases", "customer" }, allowSetters = true)
    private Set<Customers> customers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public RefCustomerTypes id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerTypeCode() {
        return this.customerTypeCode;
    }

    public RefCustomerTypes customerTypeCode(String customerTypeCode) {
        this.setCustomerTypeCode(customerTypeCode);
        return this;
    }

    public void setCustomerTypeCode(String customerTypeCode) {
        this.customerTypeCode = customerTypeCode;
    }

    public Set<Customers> getCustomers() {
        return this.customers;
    }

    public void setCustomers(Set<Customers> customers) {
        if (this.customers != null) {
            this.customers.forEach(i -> i.setCustomer(null));
        }
        if (customers != null) {
            customers.forEach(i -> i.setCustomer(this));
        }
        this.customers = customers;
    }

    public RefCustomerTypes customers(Set<Customers> customers) {
        this.setCustomers(customers);
        return this;
    }

    public RefCustomerTypes addCustomers(Customers customers) {
        this.customers.add(customers);
        customers.setCustomer(this);
        return this;
    }

    public RefCustomerTypes removeCustomers(Customers customers) {
        this.customers.remove(customers);
        customers.setCustomer(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RefCustomerTypes)) {
            return false;
        }
        return getId() != null && getId().equals(((RefCustomerTypes) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RefCustomerTypes{" +
            "id=" + getId() +
            ", customerTypeCode='" + getCustomerTypeCode() + "'" +
            "}";
    }
}
