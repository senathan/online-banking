package com.sen.online.web.rest;

import com.sen.online.domain.RefAddressTypes;
import com.sen.online.repository.RefAddressTypesRepository;
import com.sen.online.service.RefAddressTypesService;
import com.sen.online.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.sen.online.domain.RefAddressTypes}.
 */
@RestController
@RequestMapping("/api/ref-address-types")
public class RefAddressTypesResource {

    private final Logger log = LoggerFactory.getLogger(RefAddressTypesResource.class);

    private static final String ENTITY_NAME = "refAddressTypes";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RefAddressTypesService refAddressTypesService;

    private final RefAddressTypesRepository refAddressTypesRepository;

    public RefAddressTypesResource(RefAddressTypesService refAddressTypesService, RefAddressTypesRepository refAddressTypesRepository) {
        this.refAddressTypesService = refAddressTypesService;
        this.refAddressTypesRepository = refAddressTypesRepository;
    }

    /**
     * {@code POST  /ref-address-types} : Create a new refAddressTypes.
     *
     * @param refAddressTypes the refAddressTypes to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new refAddressTypes, or with status {@code 400 (Bad Request)} if the refAddressTypes has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<RefAddressTypes> createRefAddressTypes(@Valid @RequestBody RefAddressTypes refAddressTypes)
        throws URISyntaxException {
        log.debug("REST request to save RefAddressTypes : {}", refAddressTypes);
        if (refAddressTypes.getId() != null) {
            throw new BadRequestAlertException("A new refAddressTypes cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RefAddressTypes result = refAddressTypesService.save(refAddressTypes);
        return ResponseEntity
            .created(new URI("/api/ref-address-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ref-address-types/:id} : Updates an existing refAddressTypes.
     *
     * @param id the id of the refAddressTypes to save.
     * @param refAddressTypes the refAddressTypes to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated refAddressTypes,
     * or with status {@code 400 (Bad Request)} if the refAddressTypes is not valid,
     * or with status {@code 500 (Internal Server Error)} if the refAddressTypes couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<RefAddressTypes> updateRefAddressTypes(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody RefAddressTypes refAddressTypes
    ) throws URISyntaxException {
        log.debug("REST request to update RefAddressTypes : {}, {}", id, refAddressTypes);
        if (refAddressTypes.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, refAddressTypes.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!refAddressTypesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RefAddressTypes result = refAddressTypesService.update(refAddressTypes);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, refAddressTypes.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /ref-address-types/:id} : Partial updates given fields of an existing refAddressTypes, field will ignore if it is null
     *
     * @param id the id of the refAddressTypes to save.
     * @param refAddressTypes the refAddressTypes to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated refAddressTypes,
     * or with status {@code 400 (Bad Request)} if the refAddressTypes is not valid,
     * or with status {@code 404 (Not Found)} if the refAddressTypes is not found,
     * or with status {@code 500 (Internal Server Error)} if the refAddressTypes couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<RefAddressTypes> partialUpdateRefAddressTypes(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody RefAddressTypes refAddressTypes
    ) throws URISyntaxException {
        log.debug("REST request to partial update RefAddressTypes partially : {}, {}", id, refAddressTypes);
        if (refAddressTypes.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, refAddressTypes.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!refAddressTypesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RefAddressTypes> result = refAddressTypesService.partialUpdate(refAddressTypes);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, refAddressTypes.getId().toString())
        );
    }

    /**
     * {@code GET  /ref-address-types} : get all the refAddressTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of refAddressTypes in body.
     */
    @GetMapping("")
    public List<RefAddressTypes> getAllRefAddressTypes() {
        log.debug("REST request to get all RefAddressTypes");
        return refAddressTypesService.findAll();
    }

    /**
     * {@code GET  /ref-address-types/:id} : get the "id" refAddressTypes.
     *
     * @param id the id of the refAddressTypes to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the refAddressTypes, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<RefAddressTypes> getRefAddressTypes(@PathVariable("id") Long id) {
        log.debug("REST request to get RefAddressTypes : {}", id);
        Optional<RefAddressTypes> refAddressTypes = refAddressTypesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(refAddressTypes);
    }

    /**
     * {@code DELETE  /ref-address-types/:id} : delete the "id" refAddressTypes.
     *
     * @param id the id of the refAddressTypes to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRefAddressTypes(@PathVariable("id") Long id) {
        log.debug("REST request to delete RefAddressTypes : {}", id);
        refAddressTypesService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
