package com.sen.online.web.rest;

import com.sen.online.domain.RefAccountTypes;
import com.sen.online.repository.RefAccountTypesRepository;
import com.sen.online.service.RefAccountTypesService;
import com.sen.online.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.sen.online.domain.RefAccountTypes}.
 */
@RestController
@RequestMapping("/api/ref-account-types")
public class RefAccountTypesResource {

    private final Logger log = LoggerFactory.getLogger(RefAccountTypesResource.class);

    private static final String ENTITY_NAME = "refAccountTypes";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RefAccountTypesService refAccountTypesService;

    private final RefAccountTypesRepository refAccountTypesRepository;

    public RefAccountTypesResource(RefAccountTypesService refAccountTypesService, RefAccountTypesRepository refAccountTypesRepository) {
        this.refAccountTypesService = refAccountTypesService;
        this.refAccountTypesRepository = refAccountTypesRepository;
    }

    /**
     * {@code POST  /ref-account-types} : Create a new refAccountTypes.
     *
     * @param refAccountTypes the refAccountTypes to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new refAccountTypes, or with status {@code 400 (Bad Request)} if the refAccountTypes has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<RefAccountTypes> createRefAccountTypes(@Valid @RequestBody RefAccountTypes refAccountTypes)
        throws URISyntaxException {
        log.debug("REST request to save RefAccountTypes : {}", refAccountTypes);
        if (refAccountTypes.getId() != null) {
            throw new BadRequestAlertException("A new refAccountTypes cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RefAccountTypes result = refAccountTypesService.save(refAccountTypes);
        return ResponseEntity
            .created(new URI("/api/ref-account-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ref-account-types/:id} : Updates an existing refAccountTypes.
     *
     * @param id the id of the refAccountTypes to save.
     * @param refAccountTypes the refAccountTypes to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated refAccountTypes,
     * or with status {@code 400 (Bad Request)} if the refAccountTypes is not valid,
     * or with status {@code 500 (Internal Server Error)} if the refAccountTypes couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<RefAccountTypes> updateRefAccountTypes(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody RefAccountTypes refAccountTypes
    ) throws URISyntaxException {
        log.debug("REST request to update RefAccountTypes : {}, {}", id, refAccountTypes);
        if (refAccountTypes.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, refAccountTypes.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!refAccountTypesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RefAccountTypes result = refAccountTypesService.update(refAccountTypes);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, refAccountTypes.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /ref-account-types/:id} : Partial updates given fields of an existing refAccountTypes, field will ignore if it is null
     *
     * @param id the id of the refAccountTypes to save.
     * @param refAccountTypes the refAccountTypes to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated refAccountTypes,
     * or with status {@code 400 (Bad Request)} if the refAccountTypes is not valid,
     * or with status {@code 404 (Not Found)} if the refAccountTypes is not found,
     * or with status {@code 500 (Internal Server Error)} if the refAccountTypes couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<RefAccountTypes> partialUpdateRefAccountTypes(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody RefAccountTypes refAccountTypes
    ) throws URISyntaxException {
        log.debug("REST request to partial update RefAccountTypes partially : {}, {}", id, refAccountTypes);
        if (refAccountTypes.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, refAccountTypes.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!refAccountTypesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RefAccountTypes> result = refAccountTypesService.partialUpdate(refAccountTypes);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, refAccountTypes.getId().toString())
        );
    }

    /**
     * {@code GET  /ref-account-types} : get all the refAccountTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of refAccountTypes in body.
     */
    @GetMapping("")
    public List<RefAccountTypes> getAllRefAccountTypes() {
        log.debug("REST request to get all RefAccountTypes");
        return refAccountTypesService.findAll();
    }

    /**
     * {@code GET  /ref-account-types/:id} : get the "id" refAccountTypes.
     *
     * @param id the id of the refAccountTypes to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the refAccountTypes, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<RefAccountTypes> getRefAccountTypes(@PathVariable("id") Long id) {
        log.debug("REST request to get RefAccountTypes : {}", id);
        Optional<RefAccountTypes> refAccountTypes = refAccountTypesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(refAccountTypes);
    }

    /**
     * {@code DELETE  /ref-account-types/:id} : delete the "id" refAccountTypes.
     *
     * @param id the id of the refAccountTypes to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRefAccountTypes(@PathVariable("id") Long id) {
        log.debug("REST request to delete RefAccountTypes : {}", id);
        refAccountTypesService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
