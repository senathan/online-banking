package com.sen.online.web.rest;

import com.sen.online.domain.RefCustomerTypes;
import com.sen.online.repository.RefCustomerTypesRepository;
import com.sen.online.service.RefCustomerTypesService;
import com.sen.online.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.sen.online.domain.RefCustomerTypes}.
 */
@RestController
@RequestMapping("/api/ref-customer-types")
public class RefCustomerTypesResource {

    private final Logger log = LoggerFactory.getLogger(RefCustomerTypesResource.class);

    private static final String ENTITY_NAME = "refCustomerTypes";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RefCustomerTypesService refCustomerTypesService;

    private final RefCustomerTypesRepository refCustomerTypesRepository;

    public RefCustomerTypesResource(
        RefCustomerTypesService refCustomerTypesService,
        RefCustomerTypesRepository refCustomerTypesRepository
    ) {
        this.refCustomerTypesService = refCustomerTypesService;
        this.refCustomerTypesRepository = refCustomerTypesRepository;
    }

    /**
     * {@code POST  /ref-customer-types} : Create a new refCustomerTypes.
     *
     * @param refCustomerTypes the refCustomerTypes to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new refCustomerTypes, or with status {@code 400 (Bad Request)} if the refCustomerTypes has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<RefCustomerTypes> createRefCustomerTypes(@Valid @RequestBody RefCustomerTypes refCustomerTypes)
        throws URISyntaxException {
        log.debug("REST request to save RefCustomerTypes : {}", refCustomerTypes);
        if (refCustomerTypes.getId() != null) {
            throw new BadRequestAlertException("A new refCustomerTypes cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RefCustomerTypes result = refCustomerTypesService.save(refCustomerTypes);
        return ResponseEntity
            .created(new URI("/api/ref-customer-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ref-customer-types/:id} : Updates an existing refCustomerTypes.
     *
     * @param id the id of the refCustomerTypes to save.
     * @param refCustomerTypes the refCustomerTypes to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated refCustomerTypes,
     * or with status {@code 400 (Bad Request)} if the refCustomerTypes is not valid,
     * or with status {@code 500 (Internal Server Error)} if the refCustomerTypes couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<RefCustomerTypes> updateRefCustomerTypes(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody RefCustomerTypes refCustomerTypes
    ) throws URISyntaxException {
        log.debug("REST request to update RefCustomerTypes : {}, {}", id, refCustomerTypes);
        if (refCustomerTypes.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, refCustomerTypes.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!refCustomerTypesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RefCustomerTypes result = refCustomerTypesService.update(refCustomerTypes);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, refCustomerTypes.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /ref-customer-types/:id} : Partial updates given fields of an existing refCustomerTypes, field will ignore if it is null
     *
     * @param id the id of the refCustomerTypes to save.
     * @param refCustomerTypes the refCustomerTypes to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated refCustomerTypes,
     * or with status {@code 400 (Bad Request)} if the refCustomerTypes is not valid,
     * or with status {@code 404 (Not Found)} if the refCustomerTypes is not found,
     * or with status {@code 500 (Internal Server Error)} if the refCustomerTypes couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<RefCustomerTypes> partialUpdateRefCustomerTypes(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody RefCustomerTypes refCustomerTypes
    ) throws URISyntaxException {
        log.debug("REST request to partial update RefCustomerTypes partially : {}, {}", id, refCustomerTypes);
        if (refCustomerTypes.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, refCustomerTypes.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!refCustomerTypesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RefCustomerTypes> result = refCustomerTypesService.partialUpdate(refCustomerTypes);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, refCustomerTypes.getId().toString())
        );
    }

    /**
     * {@code GET  /ref-customer-types} : get all the refCustomerTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of refCustomerTypes in body.
     */
    @GetMapping("")
    public List<RefCustomerTypes> getAllRefCustomerTypes() {
        log.debug("REST request to get all RefCustomerTypes");
        return refCustomerTypesService.findAll();
    }

    /**
     * {@code GET  /ref-customer-types/:id} : get the "id" refCustomerTypes.
     *
     * @param id the id of the refCustomerTypes to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the refCustomerTypes, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<RefCustomerTypes> getRefCustomerTypes(@PathVariable("id") Long id) {
        log.debug("REST request to get RefCustomerTypes : {}", id);
        Optional<RefCustomerTypes> refCustomerTypes = refCustomerTypesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(refCustomerTypes);
    }

    /**
     * {@code DELETE  /ref-customer-types/:id} : delete the "id" refCustomerTypes.
     *
     * @param id the id of the refCustomerTypes to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRefCustomerTypes(@PathVariable("id") Long id) {
        log.debug("REST request to delete RefCustomerTypes : {}", id);
        refCustomerTypesService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
