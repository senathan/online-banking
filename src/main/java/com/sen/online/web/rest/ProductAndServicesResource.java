package com.sen.online.web.rest;

import com.sen.online.domain.ProductAndServices;
import com.sen.online.repository.ProductAndServicesRepository;
import com.sen.online.service.ProductAndServicesService;
import com.sen.online.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.sen.online.domain.ProductAndServices}.
 */
@RestController
@RequestMapping("/api/product-and-services")
public class ProductAndServicesResource {

    private final Logger log = LoggerFactory.getLogger(ProductAndServicesResource.class);

    private static final String ENTITY_NAME = "productAndServices";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductAndServicesService productAndServicesService;

    private final ProductAndServicesRepository productAndServicesRepository;

    public ProductAndServicesResource(
        ProductAndServicesService productAndServicesService,
        ProductAndServicesRepository productAndServicesRepository
    ) {
        this.productAndServicesService = productAndServicesService;
        this.productAndServicesRepository = productAndServicesRepository;
    }

    /**
     * {@code POST  /product-and-services} : Create a new productAndServices.
     *
     * @param productAndServices the productAndServices to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productAndServices, or with status {@code 400 (Bad Request)} if the productAndServices has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<ProductAndServices> createProductAndServices(@RequestBody ProductAndServices productAndServices)
        throws URISyntaxException {
        log.debug("REST request to save ProductAndServices : {}", productAndServices);
        if (productAndServices.getId() != null) {
            throw new BadRequestAlertException("A new productAndServices cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductAndServices result = productAndServicesService.save(productAndServices);
        return ResponseEntity
            .created(new URI("/api/product-and-services/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-and-services/:id} : Updates an existing productAndServices.
     *
     * @param id the id of the productAndServices to save.
     * @param productAndServices the productAndServices to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productAndServices,
     * or with status {@code 400 (Bad Request)} if the productAndServices is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productAndServices couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<ProductAndServices> updateProductAndServices(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ProductAndServices productAndServices
    ) throws URISyntaxException {
        log.debug("REST request to update ProductAndServices : {}, {}", id, productAndServices);
        if (productAndServices.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, productAndServices.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!productAndServicesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ProductAndServices result = productAndServicesService.update(productAndServices);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, productAndServices.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /product-and-services/:id} : Partial updates given fields of an existing productAndServices, field will ignore if it is null
     *
     * @param id the id of the productAndServices to save.
     * @param productAndServices the productAndServices to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productAndServices,
     * or with status {@code 400 (Bad Request)} if the productAndServices is not valid,
     * or with status {@code 404 (Not Found)} if the productAndServices is not found,
     * or with status {@code 500 (Internal Server Error)} if the productAndServices couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ProductAndServices> partialUpdateProductAndServices(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ProductAndServices productAndServices
    ) throws URISyntaxException {
        log.debug("REST request to partial update ProductAndServices partially : {}, {}", id, productAndServices);
        if (productAndServices.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, productAndServices.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!productAndServicesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ProductAndServices> result = productAndServicesService.partialUpdate(productAndServices);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, productAndServices.getId().toString())
        );
    }

    /**
     * {@code GET  /product-and-services} : get all the productAndServices.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productAndServices in body.
     */
    @GetMapping("")
    public ResponseEntity<List<ProductAndServices>> getAllProductAndServices(
        @org.springdoc.core.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of ProductAndServices");
        Page<ProductAndServices> page = productAndServicesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /product-and-services/:id} : get the "id" productAndServices.
     *
     * @param id the id of the productAndServices to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productAndServices, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<ProductAndServices> getProductAndServices(@PathVariable("id") Long id) {
        log.debug("REST request to get ProductAndServices : {}", id);
        Optional<ProductAndServices> productAndServices = productAndServicesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(productAndServices);
    }

    /**
     * {@code DELETE  /product-and-services/:id} : delete the "id" productAndServices.
     *
     * @param id the id of the productAndServices to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProductAndServices(@PathVariable("id") Long id) {
        log.debug("REST request to delete ProductAndServices : {}", id);
        productAndServicesService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
