package com.sen.online.web.rest;

import com.sen.online.domain.RefTransactionTypes;
import com.sen.online.repository.RefTransactionTypesRepository;
import com.sen.online.service.RefTransactionTypesService;
import com.sen.online.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.sen.online.domain.RefTransactionTypes}.
 */
@RestController
@RequestMapping("/api/ref-transaction-types")
public class RefTransactionTypesResource {

    private final Logger log = LoggerFactory.getLogger(RefTransactionTypesResource.class);

    private static final String ENTITY_NAME = "refTransactionTypes";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RefTransactionTypesService refTransactionTypesService;

    private final RefTransactionTypesRepository refTransactionTypesRepository;

    public RefTransactionTypesResource(
        RefTransactionTypesService refTransactionTypesService,
        RefTransactionTypesRepository refTransactionTypesRepository
    ) {
        this.refTransactionTypesService = refTransactionTypesService;
        this.refTransactionTypesRepository = refTransactionTypesRepository;
    }

    /**
     * {@code POST  /ref-transaction-types} : Create a new refTransactionTypes.
     *
     * @param refTransactionTypes the refTransactionTypes to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new refTransactionTypes, or with status {@code 400 (Bad Request)} if the refTransactionTypes has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<RefTransactionTypes> createRefTransactionTypes(@Valid @RequestBody RefTransactionTypes refTransactionTypes)
        throws URISyntaxException {
        log.debug("REST request to save RefTransactionTypes : {}", refTransactionTypes);
        if (refTransactionTypes.getId() != null) {
            throw new BadRequestAlertException("A new refTransactionTypes cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RefTransactionTypes result = refTransactionTypesService.save(refTransactionTypes);
        return ResponseEntity
            .created(new URI("/api/ref-transaction-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ref-transaction-types/:id} : Updates an existing refTransactionTypes.
     *
     * @param id the id of the refTransactionTypes to save.
     * @param refTransactionTypes the refTransactionTypes to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated refTransactionTypes,
     * or with status {@code 400 (Bad Request)} if the refTransactionTypes is not valid,
     * or with status {@code 500 (Internal Server Error)} if the refTransactionTypes couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<RefTransactionTypes> updateRefTransactionTypes(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody RefTransactionTypes refTransactionTypes
    ) throws URISyntaxException {
        log.debug("REST request to update RefTransactionTypes : {}, {}", id, refTransactionTypes);
        if (refTransactionTypes.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, refTransactionTypes.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!refTransactionTypesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RefTransactionTypes result = refTransactionTypesService.update(refTransactionTypes);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, refTransactionTypes.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /ref-transaction-types/:id} : Partial updates given fields of an existing refTransactionTypes, field will ignore if it is null
     *
     * @param id the id of the refTransactionTypes to save.
     * @param refTransactionTypes the refTransactionTypes to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated refTransactionTypes,
     * or with status {@code 400 (Bad Request)} if the refTransactionTypes is not valid,
     * or with status {@code 404 (Not Found)} if the refTransactionTypes is not found,
     * or with status {@code 500 (Internal Server Error)} if the refTransactionTypes couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<RefTransactionTypes> partialUpdateRefTransactionTypes(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody RefTransactionTypes refTransactionTypes
    ) throws URISyntaxException {
        log.debug("REST request to partial update RefTransactionTypes partially : {}, {}", id, refTransactionTypes);
        if (refTransactionTypes.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, refTransactionTypes.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!refTransactionTypesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RefTransactionTypes> result = refTransactionTypesService.partialUpdate(refTransactionTypes);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, refTransactionTypes.getId().toString())
        );
    }

    /**
     * {@code GET  /ref-transaction-types} : get all the refTransactionTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of refTransactionTypes in body.
     */
    @GetMapping("")
    public List<RefTransactionTypes> getAllRefTransactionTypes() {
        log.debug("REST request to get all RefTransactionTypes");
        return refTransactionTypesService.findAll();
    }

    /**
     * {@code GET  /ref-transaction-types/:id} : get the "id" refTransactionTypes.
     *
     * @param id the id of the refTransactionTypes to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the refTransactionTypes, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<RefTransactionTypes> getRefTransactionTypes(@PathVariable("id") Long id) {
        log.debug("REST request to get RefTransactionTypes : {}", id);
        Optional<RefTransactionTypes> refTransactionTypes = refTransactionTypesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(refTransactionTypes);
    }

    /**
     * {@code DELETE  /ref-transaction-types/:id} : delete the "id" refTransactionTypes.
     *
     * @param id the id of the refTransactionTypes to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRefTransactionTypes(@PathVariable("id") Long id) {
        log.debug("REST request to delete RefTransactionTypes : {}", id);
        refTransactionTypesService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
