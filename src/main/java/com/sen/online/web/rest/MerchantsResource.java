package com.sen.online.web.rest;

import com.sen.online.domain.Merchants;
import com.sen.online.repository.MerchantsRepository;
import com.sen.online.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.sen.online.domain.Merchants}.
 */
@RestController
@RequestMapping("/api/merchants")
@Transactional
public class MerchantsResource {

    private final Logger log = LoggerFactory.getLogger(MerchantsResource.class);

    private static final String ENTITY_NAME = "merchants";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MerchantsRepository merchantsRepository;

    public MerchantsResource(MerchantsRepository merchantsRepository) {
        this.merchantsRepository = merchantsRepository;
    }

    /**
     * {@code POST  /merchants} : Create a new merchants.
     *
     * @param merchants the merchants to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new merchants, or with status {@code 400 (Bad Request)} if the merchants has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Merchants> createMerchants(@RequestBody Merchants merchants) throws URISyntaxException {
        log.debug("REST request to save Merchants : {}", merchants);
        if (merchants.getId() != null) {
            throw new BadRequestAlertException("A new merchants cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Merchants result = merchantsRepository.save(merchants);
        return ResponseEntity
            .created(new URI("/api/merchants/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /merchants/:id} : Updates an existing merchants.
     *
     * @param id the id of the merchants to save.
     * @param merchants the merchants to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated merchants,
     * or with status {@code 400 (Bad Request)} if the merchants is not valid,
     * or with status {@code 500 (Internal Server Error)} if the merchants couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Merchants> updateMerchants(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Merchants merchants
    ) throws URISyntaxException {
        log.debug("REST request to update Merchants : {}, {}", id, merchants);
        if (merchants.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, merchants.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!merchantsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Merchants result = merchantsRepository.save(merchants);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, merchants.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /merchants/:id} : Partial updates given fields of an existing merchants, field will ignore if it is null
     *
     * @param id the id of the merchants to save.
     * @param merchants the merchants to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated merchants,
     * or with status {@code 400 (Bad Request)} if the merchants is not valid,
     * or with status {@code 404 (Not Found)} if the merchants is not found,
     * or with status {@code 500 (Internal Server Error)} if the merchants couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Merchants> partialUpdateMerchants(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Merchants merchants
    ) throws URISyntaxException {
        log.debug("REST request to partial update Merchants partially : {}, {}", id, merchants);
        if (merchants.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, merchants.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!merchantsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Merchants> result = merchantsRepository.findById(merchants.getId()).map(merchantsRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, merchants.getId().toString())
        );
    }

    /**
     * {@code GET  /merchants} : get all the merchants.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of merchants in body.
     */
    @GetMapping("")
    public List<Merchants> getAllMerchants() {
        log.debug("REST request to get all Merchants");
        return merchantsRepository.findAll();
    }

    /**
     * {@code GET  /merchants/:id} : get the "id" merchants.
     *
     * @param id the id of the merchants to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the merchants, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Merchants> getMerchants(@PathVariable("id") Long id) {
        log.debug("REST request to get Merchants : {}", id);
        Optional<Merchants> merchants = merchantsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(merchants);
    }

    /**
     * {@code DELETE  /merchants/:id} : delete the "id" merchants.
     *
     * @param id the id of the merchants to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMerchants(@PathVariable("id") Long id) {
        log.debug("REST request to delete Merchants : {}", id);
        merchantsRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
