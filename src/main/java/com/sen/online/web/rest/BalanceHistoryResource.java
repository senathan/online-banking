package com.sen.online.web.rest;

import com.sen.online.domain.BalanceHistory;
import com.sen.online.repository.BalanceHistoryRepository;
import com.sen.online.service.BalanceHistoryService;
import com.sen.online.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.sen.online.domain.BalanceHistory}.
 */
@RestController
@RequestMapping("/api/balance-histories")
public class BalanceHistoryResource {

    private final Logger log = LoggerFactory.getLogger(BalanceHistoryResource.class);

    private static final String ENTITY_NAME = "balanceHistory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BalanceHistoryService balanceHistoryService;

    private final BalanceHistoryRepository balanceHistoryRepository;

    public BalanceHistoryResource(BalanceHistoryService balanceHistoryService, BalanceHistoryRepository balanceHistoryRepository) {
        this.balanceHistoryService = balanceHistoryService;
        this.balanceHistoryRepository = balanceHistoryRepository;
    }

    /**
     * {@code POST  /balance-histories} : Create a new balanceHistory.
     *
     * @param balanceHistory the balanceHistory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new balanceHistory, or with status {@code 400 (Bad Request)} if the balanceHistory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<BalanceHistory> createBalanceHistory(@RequestBody BalanceHistory balanceHistory) throws URISyntaxException {
        log.debug("REST request to save BalanceHistory : {}", balanceHistory);
        if (balanceHistory.getId() != null) {
            throw new BadRequestAlertException("A new balanceHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BalanceHistory result = balanceHistoryService.save(balanceHistory);
        return ResponseEntity
            .created(new URI("/api/balance-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /balance-histories/:id} : Updates an existing balanceHistory.
     *
     * @param id the id of the balanceHistory to save.
     * @param balanceHistory the balanceHistory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated balanceHistory,
     * or with status {@code 400 (Bad Request)} if the balanceHistory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the balanceHistory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<BalanceHistory> updateBalanceHistory(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody BalanceHistory balanceHistory
    ) throws URISyntaxException {
        log.debug("REST request to update BalanceHistory : {}, {}", id, balanceHistory);
        if (balanceHistory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, balanceHistory.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!balanceHistoryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        BalanceHistory result = balanceHistoryService.update(balanceHistory);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, balanceHistory.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /balance-histories/:id} : Partial updates given fields of an existing balanceHistory, field will ignore if it is null
     *
     * @param id the id of the balanceHistory to save.
     * @param balanceHistory the balanceHistory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated balanceHistory,
     * or with status {@code 400 (Bad Request)} if the balanceHistory is not valid,
     * or with status {@code 404 (Not Found)} if the balanceHistory is not found,
     * or with status {@code 500 (Internal Server Error)} if the balanceHistory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<BalanceHistory> partialUpdateBalanceHistory(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody BalanceHistory balanceHistory
    ) throws URISyntaxException {
        log.debug("REST request to partial update BalanceHistory partially : {}, {}", id, balanceHistory);
        if (balanceHistory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, balanceHistory.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!balanceHistoryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<BalanceHistory> result = balanceHistoryService.partialUpdate(balanceHistory);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, balanceHistory.getId().toString())
        );
    }

    /**
     * {@code GET  /balance-histories} : get all the balanceHistories.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of balanceHistories in body.
     */
    @GetMapping("")
    public ResponseEntity<List<BalanceHistory>> getAllBalanceHistories(@org.springdoc.core.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of BalanceHistories");
        Page<BalanceHistory> page = balanceHistoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /balance-histories/:id} : get the "id" balanceHistory.
     *
     * @param id the id of the balanceHistory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the balanceHistory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<BalanceHistory> getBalanceHistory(@PathVariable("id") Long id) {
        log.debug("REST request to get BalanceHistory : {}", id);
        Optional<BalanceHistory> balanceHistory = balanceHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(balanceHistory);
    }

    /**
     * {@code DELETE  /balance-histories/:id} : delete the "id" balanceHistory.
     *
     * @param id the id of the balanceHistory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBalanceHistory(@PathVariable("id") Long id) {
        log.debug("REST request to delete BalanceHistory : {}", id);
        balanceHistoryService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
