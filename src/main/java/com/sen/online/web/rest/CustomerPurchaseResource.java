package com.sen.online.web.rest;

import com.sen.online.domain.CustomerPurchase;
import com.sen.online.repository.CustomerPurchaseRepository;
import com.sen.online.service.CustomerPurchaseService;
import com.sen.online.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.sen.online.domain.CustomerPurchase}.
 */
@RestController
@RequestMapping("/api/customer-purchases")
public class CustomerPurchaseResource {

    private final Logger log = LoggerFactory.getLogger(CustomerPurchaseResource.class);

    private static final String ENTITY_NAME = "customerPurchase";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CustomerPurchaseService customerPurchaseService;

    private final CustomerPurchaseRepository customerPurchaseRepository;

    public CustomerPurchaseResource(
        CustomerPurchaseService customerPurchaseService,
        CustomerPurchaseRepository customerPurchaseRepository
    ) {
        this.customerPurchaseService = customerPurchaseService;
        this.customerPurchaseRepository = customerPurchaseRepository;
    }

    /**
     * {@code POST  /customer-purchases} : Create a new customerPurchase.
     *
     * @param customerPurchase the customerPurchase to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new customerPurchase, or with status {@code 400 (Bad Request)} if the customerPurchase has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<CustomerPurchase> createCustomerPurchase(@RequestBody CustomerPurchase customerPurchase)
        throws URISyntaxException {
        log.debug("REST request to save CustomerPurchase : {}", customerPurchase);
        if (customerPurchase.getId() != null) {
            throw new BadRequestAlertException("A new customerPurchase cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomerPurchase result = customerPurchaseService.save(customerPurchase);
        return ResponseEntity
            .created(new URI("/api/customer-purchases/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /customer-purchases/:id} : Updates an existing customerPurchase.
     *
     * @param id the id of the customerPurchase to save.
     * @param customerPurchase the customerPurchase to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerPurchase,
     * or with status {@code 400 (Bad Request)} if the customerPurchase is not valid,
     * or with status {@code 500 (Internal Server Error)} if the customerPurchase couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<CustomerPurchase> updateCustomerPurchase(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CustomerPurchase customerPurchase
    ) throws URISyntaxException {
        log.debug("REST request to update CustomerPurchase : {}, {}", id, customerPurchase);
        if (customerPurchase.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customerPurchase.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!customerPurchaseRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CustomerPurchase result = customerPurchaseService.update(customerPurchase);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customerPurchase.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /customer-purchases/:id} : Partial updates given fields of an existing customerPurchase, field will ignore if it is null
     *
     * @param id the id of the customerPurchase to save.
     * @param customerPurchase the customerPurchase to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerPurchase,
     * or with status {@code 400 (Bad Request)} if the customerPurchase is not valid,
     * or with status {@code 404 (Not Found)} if the customerPurchase is not found,
     * or with status {@code 500 (Internal Server Error)} if the customerPurchase couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CustomerPurchase> partialUpdateCustomerPurchase(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CustomerPurchase customerPurchase
    ) throws URISyntaxException {
        log.debug("REST request to partial update CustomerPurchase partially : {}, {}", id, customerPurchase);
        if (customerPurchase.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customerPurchase.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!customerPurchaseRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CustomerPurchase> result = customerPurchaseService.partialUpdate(customerPurchase);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customerPurchase.getId().toString())
        );
    }

    /**
     * {@code GET  /customer-purchases} : get all the customerPurchases.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of customerPurchases in body.
     */
    @GetMapping("")
    public ResponseEntity<List<CustomerPurchase>> getAllCustomerPurchases(
        @org.springdoc.core.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of CustomerPurchases");
        Page<CustomerPurchase> page = customerPurchaseService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /customer-purchases/:id} : get the "id" customerPurchase.
     *
     * @param id the id of the customerPurchase to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the customerPurchase, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<CustomerPurchase> getCustomerPurchase(@PathVariable("id") Long id) {
        log.debug("REST request to get CustomerPurchase : {}", id);
        Optional<CustomerPurchase> customerPurchase = customerPurchaseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(customerPurchase);
    }

    /**
     * {@code DELETE  /customer-purchases/:id} : delete the "id" customerPurchase.
     *
     * @param id the id of the customerPurchase to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCustomerPurchase(@PathVariable("id") Long id) {
        log.debug("REST request to delete CustomerPurchase : {}", id);
        customerPurchaseService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
