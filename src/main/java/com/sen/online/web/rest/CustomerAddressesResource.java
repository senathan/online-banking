package com.sen.online.web.rest;

import com.sen.online.domain.CustomerAddresses;
import com.sen.online.repository.CustomerAddressesRepository;
import com.sen.online.service.CustomerAddressesService;
import com.sen.online.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.sen.online.domain.CustomerAddresses}.
 */
@RestController
@RequestMapping("/api/customer-addresses")
public class CustomerAddressesResource {

    private final Logger log = LoggerFactory.getLogger(CustomerAddressesResource.class);

    private static final String ENTITY_NAME = "customerAddresses";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CustomerAddressesService customerAddressesService;

    private final CustomerAddressesRepository customerAddressesRepository;

    public CustomerAddressesResource(
        CustomerAddressesService customerAddressesService,
        CustomerAddressesRepository customerAddressesRepository
    ) {
        this.customerAddressesService = customerAddressesService;
        this.customerAddressesRepository = customerAddressesRepository;
    }

    /**
     * {@code POST  /customer-addresses} : Create a new customerAddresses.
     *
     * @param customerAddresses the customerAddresses to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new customerAddresses, or with status {@code 400 (Bad Request)} if the customerAddresses has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<CustomerAddresses> createCustomerAddresses(@RequestBody CustomerAddresses customerAddresses)
        throws URISyntaxException {
        log.debug("REST request to save CustomerAddresses : {}", customerAddresses);
        if (customerAddresses.getId() != null) {
            throw new BadRequestAlertException("A new customerAddresses cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomerAddresses result = customerAddressesService.save(customerAddresses);
        return ResponseEntity
            .created(new URI("/api/customer-addresses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /customer-addresses/:id} : Updates an existing customerAddresses.
     *
     * @param id the id of the customerAddresses to save.
     * @param customerAddresses the customerAddresses to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerAddresses,
     * or with status {@code 400 (Bad Request)} if the customerAddresses is not valid,
     * or with status {@code 500 (Internal Server Error)} if the customerAddresses couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<CustomerAddresses> updateCustomerAddresses(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CustomerAddresses customerAddresses
    ) throws URISyntaxException {
        log.debug("REST request to update CustomerAddresses : {}, {}", id, customerAddresses);
        if (customerAddresses.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customerAddresses.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!customerAddressesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CustomerAddresses result = customerAddressesService.update(customerAddresses);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customerAddresses.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /customer-addresses/:id} : Partial updates given fields of an existing customerAddresses, field will ignore if it is null
     *
     * @param id the id of the customerAddresses to save.
     * @param customerAddresses the customerAddresses to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerAddresses,
     * or with status {@code 400 (Bad Request)} if the customerAddresses is not valid,
     * or with status {@code 404 (Not Found)} if the customerAddresses is not found,
     * or with status {@code 500 (Internal Server Error)} if the customerAddresses couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CustomerAddresses> partialUpdateCustomerAddresses(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CustomerAddresses customerAddresses
    ) throws URISyntaxException {
        log.debug("REST request to partial update CustomerAddresses partially : {}, {}", id, customerAddresses);
        if (customerAddresses.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customerAddresses.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!customerAddressesRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CustomerAddresses> result = customerAddressesService.partialUpdate(customerAddresses);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customerAddresses.getId().toString())
        );
    }

    /**
     * {@code GET  /customer-addresses} : get all the customerAddresses.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of customerAddresses in body.
     */
    @GetMapping("")
    public ResponseEntity<List<CustomerAddresses>> getAllCustomerAddresses(
        @org.springdoc.core.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of CustomerAddresses");
        Page<CustomerAddresses> page = customerAddressesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /customer-addresses/:id} : get the "id" customerAddresses.
     *
     * @param id the id of the customerAddresses to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the customerAddresses, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<CustomerAddresses> getCustomerAddresses(@PathVariable("id") Long id) {
        log.debug("REST request to get CustomerAddresses : {}", id);
        Optional<CustomerAddresses> customerAddresses = customerAddressesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(customerAddresses);
    }

    /**
     * {@code DELETE  /customer-addresses/:id} : delete the "id" customerAddresses.
     *
     * @param id the id of the customerAddresses to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCustomerAddresses(@PathVariable("id") Long id) {
        log.debug("REST request to delete CustomerAddresses : {}", id);
        customerAddressesService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
