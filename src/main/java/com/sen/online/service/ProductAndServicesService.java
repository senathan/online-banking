package com.sen.online.service;

import com.sen.online.domain.ProductAndServices;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.sen.online.domain.ProductAndServices}.
 */
public interface ProductAndServicesService {
    /**
     * Save a productAndServices.
     *
     * @param productAndServices the entity to save.
     * @return the persisted entity.
     */
    ProductAndServices save(ProductAndServices productAndServices);

    /**
     * Updates a productAndServices.
     *
     * @param productAndServices the entity to update.
     * @return the persisted entity.
     */
    ProductAndServices update(ProductAndServices productAndServices);

    /**
     * Partially updates a productAndServices.
     *
     * @param productAndServices the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ProductAndServices> partialUpdate(ProductAndServices productAndServices);

    /**
     * Get all the productAndServices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProductAndServices> findAll(Pageable pageable);

    /**
     * Get the "id" productAndServices.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProductAndServices> findOne(Long id);

    /**
     * Delete the "id" productAndServices.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
