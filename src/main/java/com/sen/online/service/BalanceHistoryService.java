package com.sen.online.service;

import com.sen.online.domain.BalanceHistory;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.sen.online.domain.BalanceHistory}.
 */
public interface BalanceHistoryService {
    /**
     * Save a balanceHistory.
     *
     * @param balanceHistory the entity to save.
     * @return the persisted entity.
     */
    BalanceHistory save(BalanceHistory balanceHistory);

    /**
     * Updates a balanceHistory.
     *
     * @param balanceHistory the entity to update.
     * @return the persisted entity.
     */
    BalanceHistory update(BalanceHistory balanceHistory);

    /**
     * Partially updates a balanceHistory.
     *
     * @param balanceHistory the entity to update partially.
     * @return the persisted entity.
     */
    Optional<BalanceHistory> partialUpdate(BalanceHistory balanceHistory);

    /**
     * Get all the balanceHistories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BalanceHistory> findAll(Pageable pageable);

    /**
     * Get the "id" balanceHistory.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BalanceHistory> findOne(Long id);

    /**
     * Delete the "id" balanceHistory.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
