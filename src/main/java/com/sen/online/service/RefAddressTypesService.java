package com.sen.online.service;

import com.sen.online.domain.RefAddressTypes;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.sen.online.domain.RefAddressTypes}.
 */
public interface RefAddressTypesService {
    /**
     * Save a refAddressTypes.
     *
     * @param refAddressTypes the entity to save.
     * @return the persisted entity.
     */
    RefAddressTypes save(RefAddressTypes refAddressTypes);

    /**
     * Updates a refAddressTypes.
     *
     * @param refAddressTypes the entity to update.
     * @return the persisted entity.
     */
    RefAddressTypes update(RefAddressTypes refAddressTypes);

    /**
     * Partially updates a refAddressTypes.
     *
     * @param refAddressTypes the entity to update partially.
     * @return the persisted entity.
     */
    Optional<RefAddressTypes> partialUpdate(RefAddressTypes refAddressTypes);

    /**
     * Get all the refAddressTypes.
     *
     * @return the list of entities.
     */
    List<RefAddressTypes> findAll();

    /**
     * Get the "id" refAddressTypes.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RefAddressTypes> findOne(Long id);

    /**
     * Delete the "id" refAddressTypes.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
