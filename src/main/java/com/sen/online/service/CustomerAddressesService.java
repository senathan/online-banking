package com.sen.online.service;

import com.sen.online.domain.CustomerAddresses;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.sen.online.domain.CustomerAddresses}.
 */
public interface CustomerAddressesService {
    /**
     * Save a customerAddresses.
     *
     * @param customerAddresses the entity to save.
     * @return the persisted entity.
     */
    CustomerAddresses save(CustomerAddresses customerAddresses);

    /**
     * Updates a customerAddresses.
     *
     * @param customerAddresses the entity to update.
     * @return the persisted entity.
     */
    CustomerAddresses update(CustomerAddresses customerAddresses);

    /**
     * Partially updates a customerAddresses.
     *
     * @param customerAddresses the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CustomerAddresses> partialUpdate(CustomerAddresses customerAddresses);

    /**
     * Get all the customerAddresses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CustomerAddresses> findAll(Pageable pageable);

    /**
     * Get the "id" customerAddresses.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CustomerAddresses> findOne(Long id);

    /**
     * Delete the "id" customerAddresses.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
