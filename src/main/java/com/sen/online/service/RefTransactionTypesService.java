package com.sen.online.service;

import com.sen.online.domain.RefTransactionTypes;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.sen.online.domain.RefTransactionTypes}.
 */
public interface RefTransactionTypesService {
    /**
     * Save a refTransactionTypes.
     *
     * @param refTransactionTypes the entity to save.
     * @return the persisted entity.
     */
    RefTransactionTypes save(RefTransactionTypes refTransactionTypes);

    /**
     * Updates a refTransactionTypes.
     *
     * @param refTransactionTypes the entity to update.
     * @return the persisted entity.
     */
    RefTransactionTypes update(RefTransactionTypes refTransactionTypes);

    /**
     * Partially updates a refTransactionTypes.
     *
     * @param refTransactionTypes the entity to update partially.
     * @return the persisted entity.
     */
    Optional<RefTransactionTypes> partialUpdate(RefTransactionTypes refTransactionTypes);

    /**
     * Get all the refTransactionTypes.
     *
     * @return the list of entities.
     */
    List<RefTransactionTypes> findAll();

    /**
     * Get the "id" refTransactionTypes.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RefTransactionTypes> findOne(Long id);

    /**
     * Delete the "id" refTransactionTypes.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
