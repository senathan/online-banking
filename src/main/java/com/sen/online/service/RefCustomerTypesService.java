package com.sen.online.service;

import com.sen.online.domain.RefCustomerTypes;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.sen.online.domain.RefCustomerTypes}.
 */
public interface RefCustomerTypesService {
    /**
     * Save a refCustomerTypes.
     *
     * @param refCustomerTypes the entity to save.
     * @return the persisted entity.
     */
    RefCustomerTypes save(RefCustomerTypes refCustomerTypes);

    /**
     * Updates a refCustomerTypes.
     *
     * @param refCustomerTypes the entity to update.
     * @return the persisted entity.
     */
    RefCustomerTypes update(RefCustomerTypes refCustomerTypes);

    /**
     * Partially updates a refCustomerTypes.
     *
     * @param refCustomerTypes the entity to update partially.
     * @return the persisted entity.
     */
    Optional<RefCustomerTypes> partialUpdate(RefCustomerTypes refCustomerTypes);

    /**
     * Get all the refCustomerTypes.
     *
     * @return the list of entities.
     */
    List<RefCustomerTypes> findAll();

    /**
     * Get the "id" refCustomerTypes.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RefCustomerTypes> findOne(Long id);

    /**
     * Delete the "id" refCustomerTypes.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
