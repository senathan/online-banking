package com.sen.online.service;

import com.sen.online.domain.CustomerPurchase;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.sen.online.domain.CustomerPurchase}.
 */
public interface CustomerPurchaseService {
    /**
     * Save a customerPurchase.
     *
     * @param customerPurchase the entity to save.
     * @return the persisted entity.
     */
    CustomerPurchase save(CustomerPurchase customerPurchase);

    /**
     * Updates a customerPurchase.
     *
     * @param customerPurchase the entity to update.
     * @return the persisted entity.
     */
    CustomerPurchase update(CustomerPurchase customerPurchase);

    /**
     * Partially updates a customerPurchase.
     *
     * @param customerPurchase the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CustomerPurchase> partialUpdate(CustomerPurchase customerPurchase);

    /**
     * Get all the customerPurchases.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CustomerPurchase> findAll(Pageable pageable);

    /**
     * Get the "id" customerPurchase.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CustomerPurchase> findOne(Long id);

    /**
     * Delete the "id" customerPurchase.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
