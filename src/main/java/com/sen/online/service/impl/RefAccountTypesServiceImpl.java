package com.sen.online.service.impl;

import com.sen.online.domain.RefAccountTypes;
import com.sen.online.repository.RefAccountTypesRepository;
import com.sen.online.service.RefAccountTypesService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.sen.online.domain.RefAccountTypes}.
 */
@Service
@Transactional
public class RefAccountTypesServiceImpl implements RefAccountTypesService {

    private final Logger log = LoggerFactory.getLogger(RefAccountTypesServiceImpl.class);

    private final RefAccountTypesRepository refAccountTypesRepository;

    public RefAccountTypesServiceImpl(RefAccountTypesRepository refAccountTypesRepository) {
        this.refAccountTypesRepository = refAccountTypesRepository;
    }

    @Override
    public RefAccountTypes save(RefAccountTypes refAccountTypes) {
        log.debug("Request to save RefAccountTypes : {}", refAccountTypes);
        return refAccountTypesRepository.save(refAccountTypes);
    }

    @Override
    public RefAccountTypes update(RefAccountTypes refAccountTypes) {
        log.debug("Request to update RefAccountTypes : {}", refAccountTypes);
        return refAccountTypesRepository.save(refAccountTypes);
    }

    @Override
    public Optional<RefAccountTypes> partialUpdate(RefAccountTypes refAccountTypes) {
        log.debug("Request to partially update RefAccountTypes : {}", refAccountTypes);

        return refAccountTypesRepository
            .findById(refAccountTypes.getId())
            .map(existingRefAccountTypes -> {
                if (refAccountTypes.getAccountTypeCode() != null) {
                    existingRefAccountTypes.setAccountTypeCode(refAccountTypes.getAccountTypeCode());
                }

                return existingRefAccountTypes;
            })
            .map(refAccountTypesRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RefAccountTypes> findAll() {
        log.debug("Request to get all RefAccountTypes");
        return refAccountTypesRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RefAccountTypes> findOne(Long id) {
        log.debug("Request to get RefAccountTypes : {}", id);
        return refAccountTypesRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RefAccountTypes : {}", id);
        refAccountTypesRepository.deleteById(id);
    }
}
