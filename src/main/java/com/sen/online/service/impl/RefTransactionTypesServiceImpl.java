package com.sen.online.service.impl;

import com.sen.online.domain.RefTransactionTypes;
import com.sen.online.repository.RefTransactionTypesRepository;
import com.sen.online.service.RefTransactionTypesService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.sen.online.domain.RefTransactionTypes}.
 */
@Service
@Transactional
public class RefTransactionTypesServiceImpl implements RefTransactionTypesService {

    private final Logger log = LoggerFactory.getLogger(RefTransactionTypesServiceImpl.class);

    private final RefTransactionTypesRepository refTransactionTypesRepository;

    public RefTransactionTypesServiceImpl(RefTransactionTypesRepository refTransactionTypesRepository) {
        this.refTransactionTypesRepository = refTransactionTypesRepository;
    }

    @Override
    public RefTransactionTypes save(RefTransactionTypes refTransactionTypes) {
        log.debug("Request to save RefTransactionTypes : {}", refTransactionTypes);
        return refTransactionTypesRepository.save(refTransactionTypes);
    }

    @Override
    public RefTransactionTypes update(RefTransactionTypes refTransactionTypes) {
        log.debug("Request to update RefTransactionTypes : {}", refTransactionTypes);
        return refTransactionTypesRepository.save(refTransactionTypes);
    }

    @Override
    public Optional<RefTransactionTypes> partialUpdate(RefTransactionTypes refTransactionTypes) {
        log.debug("Request to partially update RefTransactionTypes : {}", refTransactionTypes);

        return refTransactionTypesRepository
            .findById(refTransactionTypes.getId())
            .map(existingRefTransactionTypes -> {
                if (refTransactionTypes.getTransactionTypeCode() != null) {
                    existingRefTransactionTypes.setTransactionTypeCode(refTransactionTypes.getTransactionTypeCode());
                }

                return existingRefTransactionTypes;
            })
            .map(refTransactionTypesRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RefTransactionTypes> findAll() {
        log.debug("Request to get all RefTransactionTypes");
        return refTransactionTypesRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RefTransactionTypes> findOne(Long id) {
        log.debug("Request to get RefTransactionTypes : {}", id);
        return refTransactionTypesRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RefTransactionTypes : {}", id);
        refTransactionTypesRepository.deleteById(id);
    }
}
