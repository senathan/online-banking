package com.sen.online.service.impl;

import com.sen.online.domain.RefCustomerTypes;
import com.sen.online.repository.RefCustomerTypesRepository;
import com.sen.online.service.RefCustomerTypesService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.sen.online.domain.RefCustomerTypes}.
 */
@Service
@Transactional
public class RefCustomerTypesServiceImpl implements RefCustomerTypesService {

    private final Logger log = LoggerFactory.getLogger(RefCustomerTypesServiceImpl.class);

    private final RefCustomerTypesRepository refCustomerTypesRepository;

    public RefCustomerTypesServiceImpl(RefCustomerTypesRepository refCustomerTypesRepository) {
        this.refCustomerTypesRepository = refCustomerTypesRepository;
    }

    @Override
    public RefCustomerTypes save(RefCustomerTypes refCustomerTypes) {
        log.debug("Request to save RefCustomerTypes : {}", refCustomerTypes);
        return refCustomerTypesRepository.save(refCustomerTypes);
    }

    @Override
    public RefCustomerTypes update(RefCustomerTypes refCustomerTypes) {
        log.debug("Request to update RefCustomerTypes : {}", refCustomerTypes);
        return refCustomerTypesRepository.save(refCustomerTypes);
    }

    @Override
    public Optional<RefCustomerTypes> partialUpdate(RefCustomerTypes refCustomerTypes) {
        log.debug("Request to partially update RefCustomerTypes : {}", refCustomerTypes);

        return refCustomerTypesRepository
            .findById(refCustomerTypes.getId())
            .map(existingRefCustomerTypes -> {
                if (refCustomerTypes.getCustomerTypeCode() != null) {
                    existingRefCustomerTypes.setCustomerTypeCode(refCustomerTypes.getCustomerTypeCode());
                }

                return existingRefCustomerTypes;
            })
            .map(refCustomerTypesRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RefCustomerTypes> findAll() {
        log.debug("Request to get all RefCustomerTypes");
        return refCustomerTypesRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RefCustomerTypes> findOne(Long id) {
        log.debug("Request to get RefCustomerTypes : {}", id);
        return refCustomerTypesRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RefCustomerTypes : {}", id);
        refCustomerTypesRepository.deleteById(id);
    }
}
