package com.sen.online.service.impl;

import com.sen.online.domain.Addresses;
import com.sen.online.repository.AddressesRepository;
import com.sen.online.service.AddressesService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.sen.online.domain.Addresses}.
 */
@Service
@Transactional
public class AddressesServiceImpl implements AddressesService {

    private final Logger log = LoggerFactory.getLogger(AddressesServiceImpl.class);

    private final AddressesRepository addressesRepository;

    public AddressesServiceImpl(AddressesRepository addressesRepository) {
        this.addressesRepository = addressesRepository;
    }

    @Override
    public Addresses save(Addresses addresses) {
        log.debug("Request to save Addresses : {}", addresses);
        return addressesRepository.save(addresses);
    }

    @Override
    public Addresses update(Addresses addresses) {
        log.debug("Request to update Addresses : {}", addresses);
        return addressesRepository.save(addresses);
    }

    @Override
    public Optional<Addresses> partialUpdate(Addresses addresses) {
        log.debug("Request to partially update Addresses : {}", addresses);

        return addressesRepository
            .findById(addresses.getId())
            .map(existingAddresses -> {
                if (addresses.getAddressLine1() != null) {
                    existingAddresses.setAddressLine1(addresses.getAddressLine1());
                }
                if (addresses.getAddressLine2() != null) {
                    existingAddresses.setAddressLine2(addresses.getAddressLine2());
                }
                if (addresses.getPostcode() != null) {
                    existingAddresses.setPostcode(addresses.getPostcode());
                }

                return existingAddresses;
            })
            .map(addressesRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Addresses> findAll(Pageable pageable) {
        log.debug("Request to get all Addresses");
        return addressesRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Addresses> findOne(Long id) {
        log.debug("Request to get Addresses : {}", id);
        return addressesRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Addresses : {}", id);
        addressesRepository.deleteById(id);
    }
}
