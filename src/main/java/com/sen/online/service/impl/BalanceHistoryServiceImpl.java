package com.sen.online.service.impl;

import com.sen.online.domain.BalanceHistory;
import com.sen.online.repository.BalanceHistoryRepository;
import com.sen.online.service.BalanceHistoryService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.sen.online.domain.BalanceHistory}.
 */
@Service
@Transactional
public class BalanceHistoryServiceImpl implements BalanceHistoryService {

    private final Logger log = LoggerFactory.getLogger(BalanceHistoryServiceImpl.class);

    private final BalanceHistoryRepository balanceHistoryRepository;

    public BalanceHistoryServiceImpl(BalanceHistoryRepository balanceHistoryRepository) {
        this.balanceHistoryRepository = balanceHistoryRepository;
    }

    @Override
    public BalanceHistory save(BalanceHistory balanceHistory) {
        log.debug("Request to save BalanceHistory : {}", balanceHistory);
        return balanceHistoryRepository.save(balanceHistory);
    }

    @Override
    public BalanceHistory update(BalanceHistory balanceHistory) {
        log.debug("Request to update BalanceHistory : {}", balanceHistory);
        return balanceHistoryRepository.save(balanceHistory);
    }

    @Override
    public Optional<BalanceHistory> partialUpdate(BalanceHistory balanceHistory) {
        log.debug("Request to partially update BalanceHistory : {}", balanceHistory);

        return balanceHistoryRepository
            .findById(balanceHistory.getId())
            .map(existingBalanceHistory -> {
                if (balanceHistory.getBalanceDate() != null) {
                    existingBalanceHistory.setBalanceDate(balanceHistory.getBalanceDate());
                }

                return existingBalanceHistory;
            })
            .map(balanceHistoryRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BalanceHistory> findAll(Pageable pageable) {
        log.debug("Request to get all BalanceHistories");
        return balanceHistoryRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<BalanceHistory> findOne(Long id) {
        log.debug("Request to get BalanceHistory : {}", id);
        return balanceHistoryRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete BalanceHistory : {}", id);
        balanceHistoryRepository.deleteById(id);
    }
}
