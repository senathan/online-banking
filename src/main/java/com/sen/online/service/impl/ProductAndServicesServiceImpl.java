package com.sen.online.service.impl;

import com.sen.online.domain.ProductAndServices;
import com.sen.online.repository.ProductAndServicesRepository;
import com.sen.online.service.ProductAndServicesService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.sen.online.domain.ProductAndServices}.
 */
@Service
@Transactional
public class ProductAndServicesServiceImpl implements ProductAndServicesService {

    private final Logger log = LoggerFactory.getLogger(ProductAndServicesServiceImpl.class);

    private final ProductAndServicesRepository productAndServicesRepository;

    public ProductAndServicesServiceImpl(ProductAndServicesRepository productAndServicesRepository) {
        this.productAndServicesRepository = productAndServicesRepository;
    }

    @Override
    public ProductAndServices save(ProductAndServices productAndServices) {
        log.debug("Request to save ProductAndServices : {}", productAndServices);
        return productAndServicesRepository.save(productAndServices);
    }

    @Override
    public ProductAndServices update(ProductAndServices productAndServices) {
        log.debug("Request to update ProductAndServices : {}", productAndServices);
        // no save call needed as we have no fields that can be updated
        return productAndServices;
    }

    @Override
    public Optional<ProductAndServices> partialUpdate(ProductAndServices productAndServices) {
        log.debug("Request to partially update ProductAndServices : {}", productAndServices);

        return productAndServicesRepository.findById(productAndServices.getId())// .map(productAndServicesRepository::save)
        ;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ProductAndServices> findAll(Pageable pageable) {
        log.debug("Request to get all ProductAndServices");
        return productAndServicesRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ProductAndServices> findOne(Long id) {
        log.debug("Request to get ProductAndServices : {}", id);
        return productAndServicesRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProductAndServices : {}", id);
        productAndServicesRepository.deleteById(id);
    }
}
