package com.sen.online.service.impl;

import com.sen.online.domain.CustomerPurchase;
import com.sen.online.repository.CustomerPurchaseRepository;
import com.sen.online.service.CustomerPurchaseService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.sen.online.domain.CustomerPurchase}.
 */
@Service
@Transactional
public class CustomerPurchaseServiceImpl implements CustomerPurchaseService {

    private final Logger log = LoggerFactory.getLogger(CustomerPurchaseServiceImpl.class);

    private final CustomerPurchaseRepository customerPurchaseRepository;

    public CustomerPurchaseServiceImpl(CustomerPurchaseRepository customerPurchaseRepository) {
        this.customerPurchaseRepository = customerPurchaseRepository;
    }

    @Override
    public CustomerPurchase save(CustomerPurchase customerPurchase) {
        log.debug("Request to save CustomerPurchase : {}", customerPurchase);
        return customerPurchaseRepository.save(customerPurchase);
    }

    @Override
    public CustomerPurchase update(CustomerPurchase customerPurchase) {
        log.debug("Request to update CustomerPurchase : {}", customerPurchase);
        return customerPurchaseRepository.save(customerPurchase);
    }

    @Override
    public Optional<CustomerPurchase> partialUpdate(CustomerPurchase customerPurchase) {
        log.debug("Request to partially update CustomerPurchase : {}", customerPurchase);

        return customerPurchaseRepository.findById(customerPurchase.getId()).map(customerPurchaseRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CustomerPurchase> findAll(Pageable pageable) {
        log.debug("Request to get all CustomerPurchases");
        return customerPurchaseRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CustomerPurchase> findOne(Long id) {
        log.debug("Request to get CustomerPurchase : {}", id);
        return customerPurchaseRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CustomerPurchase : {}", id);
        customerPurchaseRepository.deleteById(id);
    }
}
