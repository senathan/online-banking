package com.sen.online.service.impl;

import com.sen.online.domain.CustomerAddresses;
import com.sen.online.repository.CustomerAddressesRepository;
import com.sen.online.service.CustomerAddressesService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.sen.online.domain.CustomerAddresses}.
 */
@Service
@Transactional
public class CustomerAddressesServiceImpl implements CustomerAddressesService {

    private final Logger log = LoggerFactory.getLogger(CustomerAddressesServiceImpl.class);

    private final CustomerAddressesRepository customerAddressesRepository;

    public CustomerAddressesServiceImpl(CustomerAddressesRepository customerAddressesRepository) {
        this.customerAddressesRepository = customerAddressesRepository;
    }

    @Override
    public CustomerAddresses save(CustomerAddresses customerAddresses) {
        log.debug("Request to save CustomerAddresses : {}", customerAddresses);
        return customerAddressesRepository.save(customerAddresses);
    }

    @Override
    public CustomerAddresses update(CustomerAddresses customerAddresses) {
        log.debug("Request to update CustomerAddresses : {}", customerAddresses);
        return customerAddressesRepository.save(customerAddresses);
    }

    @Override
    public Optional<CustomerAddresses> partialUpdate(CustomerAddresses customerAddresses) {
        log.debug("Request to partially update CustomerAddresses : {}", customerAddresses);

        return customerAddressesRepository.findById(customerAddresses.getId()).map(customerAddressesRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CustomerAddresses> findAll(Pageable pageable) {
        log.debug("Request to get all CustomerAddresses");
        return customerAddressesRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CustomerAddresses> findOne(Long id) {
        log.debug("Request to get CustomerAddresses : {}", id);
        return customerAddressesRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CustomerAddresses : {}", id);
        customerAddressesRepository.deleteById(id);
    }
}
