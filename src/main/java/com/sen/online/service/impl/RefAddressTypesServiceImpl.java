package com.sen.online.service.impl;

import com.sen.online.domain.RefAddressTypes;
import com.sen.online.repository.RefAddressTypesRepository;
import com.sen.online.service.RefAddressTypesService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.sen.online.domain.RefAddressTypes}.
 */
@Service
@Transactional
public class RefAddressTypesServiceImpl implements RefAddressTypesService {

    private final Logger log = LoggerFactory.getLogger(RefAddressTypesServiceImpl.class);

    private final RefAddressTypesRepository refAddressTypesRepository;

    public RefAddressTypesServiceImpl(RefAddressTypesRepository refAddressTypesRepository) {
        this.refAddressTypesRepository = refAddressTypesRepository;
    }

    @Override
    public RefAddressTypes save(RefAddressTypes refAddressTypes) {
        log.debug("Request to save RefAddressTypes : {}", refAddressTypes);
        return refAddressTypesRepository.save(refAddressTypes);
    }

    @Override
    public RefAddressTypes update(RefAddressTypes refAddressTypes) {
        log.debug("Request to update RefAddressTypes : {}", refAddressTypes);
        return refAddressTypesRepository.save(refAddressTypes);
    }

    @Override
    public Optional<RefAddressTypes> partialUpdate(RefAddressTypes refAddressTypes) {
        log.debug("Request to partially update RefAddressTypes : {}", refAddressTypes);

        return refAddressTypesRepository
            .findById(refAddressTypes.getId())
            .map(existingRefAddressTypes -> {
                if (refAddressTypes.getAddressTypeCode() != null) {
                    existingRefAddressTypes.setAddressTypeCode(refAddressTypes.getAddressTypeCode());
                }

                return existingRefAddressTypes;
            })
            .map(refAddressTypesRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RefAddressTypes> findAll() {
        log.debug("Request to get all RefAddressTypes");
        return refAddressTypesRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RefAddressTypes> findOne(Long id) {
        log.debug("Request to get RefAddressTypes : {}", id);
        return refAddressTypesRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RefAddressTypes : {}", id);
        refAddressTypesRepository.deleteById(id);
    }
}
