package com.sen.online.service;

import com.sen.online.domain.RefAccountTypes;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.sen.online.domain.RefAccountTypes}.
 */
public interface RefAccountTypesService {
    /**
     * Save a refAccountTypes.
     *
     * @param refAccountTypes the entity to save.
     * @return the persisted entity.
     */
    RefAccountTypes save(RefAccountTypes refAccountTypes);

    /**
     * Updates a refAccountTypes.
     *
     * @param refAccountTypes the entity to update.
     * @return the persisted entity.
     */
    RefAccountTypes update(RefAccountTypes refAccountTypes);

    /**
     * Partially updates a refAccountTypes.
     *
     * @param refAccountTypes the entity to update partially.
     * @return the persisted entity.
     */
    Optional<RefAccountTypes> partialUpdate(RefAccountTypes refAccountTypes);

    /**
     * Get all the refAccountTypes.
     *
     * @return the list of entities.
     */
    List<RefAccountTypes> findAll();

    /**
     * Get the "id" refAccountTypes.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RefAccountTypes> findOne(Long id);

    /**
     * Delete the "id" refAccountTypes.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
