import { IAccounts } from 'app/shared/model/accounts.model';
import { ICustomerAddresses } from 'app/shared/model/customer-addresses.model';
import { ICustomerPurchase } from 'app/shared/model/customer-purchase.model';
import { IRefCustomerTypes } from 'app/shared/model/ref-customer-types.model';

export interface ICustomers {
  id?: number;
  customerName?: string | null;
  accounts?: IAccounts[] | null;
  customerAddresses?: ICustomerAddresses[] | null;
  customerPurchases?: ICustomerPurchase[] | null;
  customer?: IRefCustomerTypes | null;
}

export const defaultValue: Readonly<ICustomers> = {};
