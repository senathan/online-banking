import { IRefAccountTypes } from 'app/shared/model/ref-account-types.model';
import { ICustomers } from 'app/shared/model/customers.model';
import { IBalanceHistory } from 'app/shared/model/balance-history.model';
import { ITransactions } from 'app/shared/model/transactions.model';

export interface IAccounts {
  id?: number;
  accountDesc?: string | null;
  account?: IRefAccountTypes | null;
  customers?: ICustomers | null;
  balanceHistories?: IBalanceHistory[] | null;
  transactions?: ITransactions[] | null;
}

export const defaultValue: Readonly<IAccounts> = {};
