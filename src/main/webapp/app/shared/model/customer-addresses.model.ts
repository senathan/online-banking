import { IAddresses } from 'app/shared/model/addresses.model';
import { ICustomers } from 'app/shared/model/customers.model';

export interface ICustomerAddresses {
  id?: number;
  addresses?: IAddresses | null;
  customers?: ICustomers | null;
}

export const defaultValue: Readonly<ICustomerAddresses> = {};
