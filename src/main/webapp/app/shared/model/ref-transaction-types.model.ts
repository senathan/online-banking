import { ITransactions } from 'app/shared/model/transactions.model';

export interface IRefTransactionTypes {
  id?: number;
  transactionTypeCode?: string;
  transactions?: ITransactions[] | null;
}

export const defaultValue: Readonly<IRefTransactionTypes> = {};
