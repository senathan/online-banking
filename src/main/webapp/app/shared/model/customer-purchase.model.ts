import { ICustomers } from 'app/shared/model/customers.model';
import { IProductAndServices } from 'app/shared/model/product-and-services.model';
import { ITransactions } from 'app/shared/model/transactions.model';

export interface ICustomerPurchase {
  id?: number;
  customers?: ICustomers | null;
  productAndServices?: IProductAndServices | null;
  transactions?: ITransactions[] | null;
}

export const defaultValue: Readonly<ICustomerPurchase> = {};
