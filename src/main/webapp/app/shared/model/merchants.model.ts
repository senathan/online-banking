import { IProductAndServices } from 'app/shared/model/product-and-services.model';

export interface IMerchants {
  id?: number;
  merchant?: IProductAndServices | null;
}

export const defaultValue: Readonly<IMerchants> = {};
