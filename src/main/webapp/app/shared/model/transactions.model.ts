import { IAccounts } from 'app/shared/model/accounts.model';
import { ICustomerPurchase } from 'app/shared/model/customer-purchase.model';
import { IRefTransactionTypes } from 'app/shared/model/ref-transaction-types.model';

export interface ITransactions {
  id?: number;
  transactionUniqueCode?: string | null;
  accounts?: IAccounts | null;
  customerPurchase?: ICustomerPurchase | null;
  transaction?: IRefTransactionTypes | null;
}

export const defaultValue: Readonly<ITransactions> = {};
