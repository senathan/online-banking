import { IAccounts } from 'app/shared/model/accounts.model';

export interface IRefAccountTypes {
  id?: number;
  accountTypeCode?: string;
  accounts?: IAccounts[] | null;
}

export const defaultValue: Readonly<IRefAccountTypes> = {};
