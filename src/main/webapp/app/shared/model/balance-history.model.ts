import { IAccounts } from 'app/shared/model/accounts.model';

export interface IBalanceHistory {
  id?: number;
  balanceDate?: string | null;
  accounts?: IAccounts | null;
}

export const defaultValue: Readonly<IBalanceHistory> = {};
