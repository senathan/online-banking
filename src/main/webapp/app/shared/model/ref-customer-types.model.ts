import { ICustomers } from 'app/shared/model/customers.model';

export interface IRefCustomerTypes {
  id?: number;
  customerTypeCode?: string;
  customers?: ICustomers[] | null;
}

export const defaultValue: Readonly<IRefCustomerTypes> = {};
