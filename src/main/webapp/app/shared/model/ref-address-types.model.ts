import { IAddresses } from 'app/shared/model/addresses.model';

export interface IRefAddressTypes {
  id?: number;
  addressTypeCode?: string;
  addresses?: IAddresses[] | null;
}

export const defaultValue: Readonly<IRefAddressTypes> = {};
