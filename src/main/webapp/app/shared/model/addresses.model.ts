import { IRefAddressTypes } from 'app/shared/model/ref-address-types.model';
import { ICustomerAddresses } from 'app/shared/model/customer-addresses.model';

export interface IAddresses {
  id?: number;
  addressLine1?: string | null;
  addressLine2?: string | null;
  postcode?: string | null;
  address?: IRefAddressTypes | null;
  customerAddresses?: ICustomerAddresses[] | null;
}

export const defaultValue: Readonly<IAddresses> = {};
