import { ICustomerPurchase } from 'app/shared/model/customer-purchase.model';

export interface IProductAndServices {
  id?: number;
  customerPurchases?: ICustomerPurchase[] | null;
}

export const defaultValue: Readonly<IProductAndServices> = {};
