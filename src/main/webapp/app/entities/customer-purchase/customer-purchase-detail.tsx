import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './customer-purchase.reducer';

export const CustomerPurchaseDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const customerPurchaseEntity = useAppSelector(state => state.customerPurchase.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="customerPurchaseDetailsHeading">Customer Purchase</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{customerPurchaseEntity.id}</dd>
          <dt>Customers</dt>
          <dd>{customerPurchaseEntity.customers ? customerPurchaseEntity.customers.id : ''}</dd>
          <dt>Product And Services</dt>
          <dd>{customerPurchaseEntity.productAndServices ? customerPurchaseEntity.productAndServices.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/customer-purchase" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/customer-purchase/${customerPurchaseEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default CustomerPurchaseDetail;
