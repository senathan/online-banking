import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { ICustomers } from 'app/shared/model/customers.model';
import { getEntities as getCustomers } from 'app/entities/customers/customers.reducer';
import { IProductAndServices } from 'app/shared/model/product-and-services.model';
import { getEntities as getProductAndServices } from 'app/entities/product-and-services/product-and-services.reducer';
import { ICustomerPurchase } from 'app/shared/model/customer-purchase.model';
import { getEntity, updateEntity, createEntity, reset } from './customer-purchase.reducer';

export const CustomerPurchaseUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const customers = useAppSelector(state => state.customers.entities);
  const productAndServices = useAppSelector(state => state.productAndServices.entities);
  const customerPurchaseEntity = useAppSelector(state => state.customerPurchase.entity);
  const loading = useAppSelector(state => state.customerPurchase.loading);
  const updating = useAppSelector(state => state.customerPurchase.updating);
  const updateSuccess = useAppSelector(state => state.customerPurchase.updateSuccess);

  const handleClose = () => {
    navigate('/customer-purchase' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getCustomers({}));
    dispatch(getProductAndServices({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  // eslint-disable-next-line complexity
  const saveEntity = values => {
    if (values.id !== undefined && typeof values.id !== 'number') {
      values.id = Number(values.id);
    }

    const entity = {
      ...customerPurchaseEntity,
      ...values,
      customers: customers.find(it => it.id.toString() === values.customers.toString()),
      productAndServices: productAndServices.find(it => it.id.toString() === values.productAndServices.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...customerPurchaseEntity,
          customers: customerPurchaseEntity?.customers?.id,
          productAndServices: customerPurchaseEntity?.productAndServices?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="onlineBankingApp.customerPurchase.home.createOrEditLabel" data-cy="CustomerPurchaseCreateUpdateHeading">
            Create or edit a Customer Purchase
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField name="id" required readOnly id="customer-purchase-id" label="ID" validate={{ required: true }} />
              ) : null}
              <ValidatedField id="customer-purchase-customers" name="customers" data-cy="customers" label="Customers" type="select">
                <option value="" key="0" />
                {customers
                  ? customers.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="customer-purchase-productAndServices"
                name="productAndServices"
                data-cy="productAndServices"
                label="Product And Services"
                type="select"
              >
                <option value="" key="0" />
                {productAndServices
                  ? productAndServices.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/customer-purchase" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default CustomerPurchaseUpdate;
