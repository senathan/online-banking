import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import CustomerPurchase from './customer-purchase';
import CustomerPurchaseDetail from './customer-purchase-detail';
import CustomerPurchaseUpdate from './customer-purchase-update';
import CustomerPurchaseDeleteDialog from './customer-purchase-delete-dialog';

const CustomerPurchaseRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<CustomerPurchase />} />
    <Route path="new" element={<CustomerPurchaseUpdate />} />
    <Route path=":id">
      <Route index element={<CustomerPurchaseDetail />} />
      <Route path="edit" element={<CustomerPurchaseUpdate />} />
      <Route path="delete" element={<CustomerPurchaseDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default CustomerPurchaseRoutes;
