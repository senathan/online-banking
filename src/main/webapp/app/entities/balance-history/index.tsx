import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import BalanceHistory from './balance-history';
import BalanceHistoryDetail from './balance-history-detail';
import BalanceHistoryUpdate from './balance-history-update';
import BalanceHistoryDeleteDialog from './balance-history-delete-dialog';

const BalanceHistoryRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<BalanceHistory />} />
    <Route path="new" element={<BalanceHistoryUpdate />} />
    <Route path=":id">
      <Route index element={<BalanceHistoryDetail />} />
      <Route path="edit" element={<BalanceHistoryUpdate />} />
      <Route path="delete" element={<BalanceHistoryDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default BalanceHistoryRoutes;
