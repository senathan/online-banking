import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './balance-history.reducer';

export const BalanceHistoryDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const balanceHistoryEntity = useAppSelector(state => state.balanceHistory.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="balanceHistoryDetailsHeading">Balance History</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{balanceHistoryEntity.id}</dd>
          <dt>
            <span id="balanceDate">Balance Date</span>
          </dt>
          <dd>{balanceHistoryEntity.balanceDate}</dd>
          <dt>Accounts</dt>
          <dd>{balanceHistoryEntity.accounts ? balanceHistoryEntity.accounts.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/balance-history" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/balance-history/${balanceHistoryEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default BalanceHistoryDetail;
