import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import RefAccountTypes from './ref-account-types';
import RefAccountTypesDetail from './ref-account-types-detail';
import RefAccountTypesUpdate from './ref-account-types-update';
import RefAccountTypesDeleteDialog from './ref-account-types-delete-dialog';

const RefAccountTypesRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<RefAccountTypes />} />
    <Route path="new" element={<RefAccountTypesUpdate />} />
    <Route path=":id">
      <Route index element={<RefAccountTypesDetail />} />
      <Route path="edit" element={<RefAccountTypesUpdate />} />
      <Route path="delete" element={<RefAccountTypesDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default RefAccountTypesRoutes;
