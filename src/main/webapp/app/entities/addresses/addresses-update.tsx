import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IRefAddressTypes } from 'app/shared/model/ref-address-types.model';
import { getEntities as getRefAddressTypes } from 'app/entities/ref-address-types/ref-address-types.reducer';
import { IAddresses } from 'app/shared/model/addresses.model';
import { getEntity, updateEntity, createEntity, reset } from './addresses.reducer';

export const AddressesUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const refAddressTypes = useAppSelector(state => state.refAddressTypes.entities);
  const addressesEntity = useAppSelector(state => state.addresses.entity);
  const loading = useAppSelector(state => state.addresses.loading);
  const updating = useAppSelector(state => state.addresses.updating);
  const updateSuccess = useAppSelector(state => state.addresses.updateSuccess);

  const handleClose = () => {
    navigate('/addresses' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getRefAddressTypes({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  // eslint-disable-next-line complexity
  const saveEntity = values => {
    if (values.id !== undefined && typeof values.id !== 'number') {
      values.id = Number(values.id);
    }

    const entity = {
      ...addressesEntity,
      ...values,
      address: refAddressTypes.find(it => it.id.toString() === values.address.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...addressesEntity,
          address: addressesEntity?.address?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="onlineBankingApp.addresses.home.createOrEditLabel" data-cy="AddressesCreateUpdateHeading">
            Create or edit a Addresses
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="addresses-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedField label="Address Line 1" id="addresses-addressLine1" name="addressLine1" data-cy="addressLine1" type="text" />
              <ValidatedField label="Address Line 2" id="addresses-addressLine2" name="addressLine2" data-cy="addressLine2" type="text" />
              <ValidatedField label="Postcode" id="addresses-postcode" name="postcode" data-cy="postcode" type="text" />
              <ValidatedField id="addresses-address" name="address" data-cy="address" label="Address" type="select">
                <option value="" key="0" />
                {refAddressTypes
                  ? refAddressTypes.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/addresses" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default AddressesUpdate;
