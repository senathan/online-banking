import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IRefCustomerTypes } from 'app/shared/model/ref-customer-types.model';
import { getEntities as getRefCustomerTypes } from 'app/entities/ref-customer-types/ref-customer-types.reducer';
import { ICustomers } from 'app/shared/model/customers.model';
import { getEntity, updateEntity, createEntity, reset } from './customers.reducer';

export const CustomersUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const refCustomerTypes = useAppSelector(state => state.refCustomerTypes.entities);
  const customersEntity = useAppSelector(state => state.customers.entity);
  const loading = useAppSelector(state => state.customers.loading);
  const updating = useAppSelector(state => state.customers.updating);
  const updateSuccess = useAppSelector(state => state.customers.updateSuccess);

  const handleClose = () => {
    navigate('/customers' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getRefCustomerTypes({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  // eslint-disable-next-line complexity
  const saveEntity = values => {
    if (values.id !== undefined && typeof values.id !== 'number') {
      values.id = Number(values.id);
    }

    const entity = {
      ...customersEntity,
      ...values,
      customer: refCustomerTypes.find(it => it.id.toString() === values.customer.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...customersEntity,
          customer: customersEntity?.customer?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="onlineBankingApp.customers.home.createOrEditLabel" data-cy="CustomersCreateUpdateHeading">
            Create or edit a Customers
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="customers-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedField label="Customer Name" id="customers-customerName" name="customerName" data-cy="customerName" type="text" />
              <ValidatedField id="customers-customer" name="customer" data-cy="customer" label="Customer" type="select">
                <option value="" key="0" />
                {refCustomerTypes
                  ? refCustomerTypes.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/customers" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default CustomersUpdate;
