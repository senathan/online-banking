import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import RefTransactionTypes from './ref-transaction-types';
import RefTransactionTypesDetail from './ref-transaction-types-detail';
import RefTransactionTypesUpdate from './ref-transaction-types-update';
import RefTransactionTypesDeleteDialog from './ref-transaction-types-delete-dialog';

const RefTransactionTypesRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<RefTransactionTypes />} />
    <Route path="new" element={<RefTransactionTypesUpdate />} />
    <Route path=":id">
      <Route index element={<RefTransactionTypesDetail />} />
      <Route path="edit" element={<RefTransactionTypesUpdate />} />
      <Route path="delete" element={<RefTransactionTypesDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default RefTransactionTypesRoutes;
