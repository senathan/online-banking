import React from 'react';

import MenuItem from 'app/shared/layout/menus/menu-item';

const EntitiesMenu = () => {
  return (
    <>
      {/* prettier-ignore */}
      <MenuItem icon="asterisk" to="/ref-customer-types">
        Ref Customer Types
      </MenuItem>
      <MenuItem icon="asterisk" to="/ref-address-types">
        Ref Address Types
      </MenuItem>
      <MenuItem icon="asterisk" to="/ref-account-types">
        Ref Account Types
      </MenuItem>
      <MenuItem icon="asterisk" to="/ref-transaction-types">
        Ref Transaction Types
      </MenuItem>
      <MenuItem icon="asterisk" to="/transactions">
        Transactions
      </MenuItem>
      <MenuItem icon="asterisk" to="/customers">
        Customers
      </MenuItem>
      <MenuItem icon="asterisk" to="/addresses">
        Addresses
      </MenuItem>
      <MenuItem icon="asterisk" to="/accounts">
        Accounts
      </MenuItem>
      <MenuItem icon="asterisk" to="/customer-addresses">
        Customer Addresses
      </MenuItem>
      <MenuItem icon="asterisk" to="/customer-purchase">
        Customer Purchase
      </MenuItem>
      <MenuItem icon="asterisk" to="/merchants">
        Merchants
      </MenuItem>
      <MenuItem icon="asterisk" to="/balance-history">
        Balance History
      </MenuItem>
      <MenuItem icon="asterisk" to="/product-and-services">
        Product And Services
      </MenuItem>
      {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
    </>
  );
};

export default EntitiesMenu;
