import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './ref-customer-types.reducer';

export const RefCustomerTypesDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const refCustomerTypesEntity = useAppSelector(state => state.refCustomerTypes.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="refCustomerTypesDetailsHeading">Ref Customer Types</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{refCustomerTypesEntity.id}</dd>
          <dt>
            <span id="customerTypeCode">Customer Type Code</span>
          </dt>
          <dd>{refCustomerTypesEntity.customerTypeCode}</dd>
        </dl>
        <Button tag={Link} to="/ref-customer-types" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/ref-customer-types/${refCustomerTypesEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default RefCustomerTypesDetail;
