import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import RefCustomerTypes from './ref-customer-types';
import RefCustomerTypesDetail from './ref-customer-types-detail';
import RefCustomerTypesUpdate from './ref-customer-types-update';
import RefCustomerTypesDeleteDialog from './ref-customer-types-delete-dialog';

const RefCustomerTypesRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<RefCustomerTypes />} />
    <Route path="new" element={<RefCustomerTypesUpdate />} />
    <Route path=":id">
      <Route index element={<RefCustomerTypesDetail />} />
      <Route path="edit" element={<RefCustomerTypesUpdate />} />
      <Route path="delete" element={<RefCustomerTypesDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default RefCustomerTypesRoutes;
