import refCustomerTypes from 'app/entities/ref-customer-types/ref-customer-types.reducer';
import refAddressTypes from 'app/entities/ref-address-types/ref-address-types.reducer';
import refAccountTypes from 'app/entities/ref-account-types/ref-account-types.reducer';
import refTransactionTypes from 'app/entities/ref-transaction-types/ref-transaction-types.reducer';
import transactions from 'app/entities/transactions/transactions.reducer';
import customers from 'app/entities/customers/customers.reducer';
import addresses from 'app/entities/addresses/addresses.reducer';
import accounts from 'app/entities/accounts/accounts.reducer';
import customerAddresses from 'app/entities/customer-addresses/customer-addresses.reducer';
import customerPurchase from 'app/entities/customer-purchase/customer-purchase.reducer';
import merchants from 'app/entities/merchants/merchants.reducer';
import balanceHistory from 'app/entities/balance-history/balance-history.reducer';
import productAndServices from 'app/entities/product-and-services/product-and-services.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const entitiesReducers = {
  refCustomerTypes,
  refAddressTypes,
  refAccountTypes,
  refTransactionTypes,
  transactions,
  customers,
  addresses,
  accounts,
  customerAddresses,
  customerPurchase,
  merchants,
  balanceHistory,
  productAndServices,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
};

export default entitiesReducers;
