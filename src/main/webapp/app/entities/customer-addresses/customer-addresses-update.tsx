import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IAddresses } from 'app/shared/model/addresses.model';
import { getEntities as getAddresses } from 'app/entities/addresses/addresses.reducer';
import { ICustomers } from 'app/shared/model/customers.model';
import { getEntities as getCustomers } from 'app/entities/customers/customers.reducer';
import { ICustomerAddresses } from 'app/shared/model/customer-addresses.model';
import { getEntity, updateEntity, createEntity, reset } from './customer-addresses.reducer';

export const CustomerAddressesUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const addresses = useAppSelector(state => state.addresses.entities);
  const customers = useAppSelector(state => state.customers.entities);
  const customerAddressesEntity = useAppSelector(state => state.customerAddresses.entity);
  const loading = useAppSelector(state => state.customerAddresses.loading);
  const updating = useAppSelector(state => state.customerAddresses.updating);
  const updateSuccess = useAppSelector(state => state.customerAddresses.updateSuccess);

  const handleClose = () => {
    navigate('/customer-addresses' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getAddresses({}));
    dispatch(getCustomers({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  // eslint-disable-next-line complexity
  const saveEntity = values => {
    if (values.id !== undefined && typeof values.id !== 'number') {
      values.id = Number(values.id);
    }

    const entity = {
      ...customerAddressesEntity,
      ...values,
      addresses: addresses.find(it => it.id.toString() === values.addresses.toString()),
      customers: customers.find(it => it.id.toString() === values.customers.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...customerAddressesEntity,
          addresses: customerAddressesEntity?.addresses?.id,
          customers: customerAddressesEntity?.customers?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="onlineBankingApp.customerAddresses.home.createOrEditLabel" data-cy="CustomerAddressesCreateUpdateHeading">
            Create or edit a Customer Addresses
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField name="id" required readOnly id="customer-addresses-id" label="ID" validate={{ required: true }} />
              ) : null}
              <ValidatedField id="customer-addresses-addresses" name="addresses" data-cy="addresses" label="Addresses" type="select">
                <option value="" key="0" />
                {addresses
                  ? addresses.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField id="customer-addresses-customers" name="customers" data-cy="customers" label="Customers" type="select">
                <option value="" key="0" />
                {customers
                  ? customers.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/customer-addresses" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default CustomerAddressesUpdate;
