import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './customer-addresses.reducer';

export const CustomerAddressesDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const customerAddressesEntity = useAppSelector(state => state.customerAddresses.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="customerAddressesDetailsHeading">Customer Addresses</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{customerAddressesEntity.id}</dd>
          <dt>Addresses</dt>
          <dd>{customerAddressesEntity.addresses ? customerAddressesEntity.addresses.id : ''}</dd>
          <dt>Customers</dt>
          <dd>{customerAddressesEntity.customers ? customerAddressesEntity.customers.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/customer-addresses" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/customer-addresses/${customerAddressesEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default CustomerAddressesDetail;
