import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import CustomerAddresses from './customer-addresses';
import CustomerAddressesDetail from './customer-addresses-detail';
import CustomerAddressesUpdate from './customer-addresses-update';
import CustomerAddressesDeleteDialog from './customer-addresses-delete-dialog';

const CustomerAddressesRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<CustomerAddresses />} />
    <Route path="new" element={<CustomerAddressesUpdate />} />
    <Route path=":id">
      <Route index element={<CustomerAddressesDetail />} />
      <Route path="edit" element={<CustomerAddressesUpdate />} />
      <Route path="delete" element={<CustomerAddressesDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default CustomerAddressesRoutes;
