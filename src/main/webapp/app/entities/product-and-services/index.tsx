import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import ProductAndServices from './product-and-services';
import ProductAndServicesDetail from './product-and-services-detail';
import ProductAndServicesUpdate from './product-and-services-update';
import ProductAndServicesDeleteDialog from './product-and-services-delete-dialog';

const ProductAndServicesRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<ProductAndServices />} />
    <Route path="new" element={<ProductAndServicesUpdate />} />
    <Route path=":id">
      <Route index element={<ProductAndServicesDetail />} />
      <Route path="edit" element={<ProductAndServicesUpdate />} />
      <Route path="delete" element={<ProductAndServicesDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default ProductAndServicesRoutes;
