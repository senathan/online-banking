import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IProductAndServices } from 'app/shared/model/product-and-services.model';
import { getEntities as getProductAndServices } from 'app/entities/product-and-services/product-and-services.reducer';
import { IMerchants } from 'app/shared/model/merchants.model';
import { getEntity, updateEntity, createEntity, reset } from './merchants.reducer';

export const MerchantsUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const productAndServices = useAppSelector(state => state.productAndServices.entities);
  const merchantsEntity = useAppSelector(state => state.merchants.entity);
  const loading = useAppSelector(state => state.merchants.loading);
  const updating = useAppSelector(state => state.merchants.updating);
  const updateSuccess = useAppSelector(state => state.merchants.updateSuccess);

  const handleClose = () => {
    navigate('/merchants');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getProductAndServices({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  // eslint-disable-next-line complexity
  const saveEntity = values => {
    if (values.id !== undefined && typeof values.id !== 'number') {
      values.id = Number(values.id);
    }

    const entity = {
      ...merchantsEntity,
      ...values,
      merchant: productAndServices.find(it => it.id.toString() === values.merchant.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...merchantsEntity,
          merchant: merchantsEntity?.merchant?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="onlineBankingApp.merchants.home.createOrEditLabel" data-cy="MerchantsCreateUpdateHeading">
            Create or edit a Merchants
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="merchants-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedField id="merchants-merchant" name="merchant" data-cy="merchant" label="Merchant" type="select">
                <option value="" key="0" />
                {productAndServices
                  ? productAndServices.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/merchants" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default MerchantsUpdate;
