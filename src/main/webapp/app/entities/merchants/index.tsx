import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Merchants from './merchants';
import MerchantsDetail from './merchants-detail';
import MerchantsUpdate from './merchants-update';
import MerchantsDeleteDialog from './merchants-delete-dialog';

const MerchantsRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Merchants />} />
    <Route path="new" element={<MerchantsUpdate />} />
    <Route path=":id">
      <Route index element={<MerchantsDetail />} />
      <Route path="edit" element={<MerchantsUpdate />} />
      <Route path="delete" element={<MerchantsDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default MerchantsRoutes;
