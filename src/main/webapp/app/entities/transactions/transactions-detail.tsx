import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './transactions.reducer';

export const TransactionsDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const transactionsEntity = useAppSelector(state => state.transactions.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="transactionsDetailsHeading">Transactions</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{transactionsEntity.id}</dd>
          <dt>
            <span id="transactionUniqueCode">Transaction Unique Code</span>
          </dt>
          <dd>{transactionsEntity.transactionUniqueCode}</dd>
          <dt>Accounts</dt>
          <dd>{transactionsEntity.accounts ? transactionsEntity.accounts.id : ''}</dd>
          <dt>Customer Purchase</dt>
          <dd>{transactionsEntity.customerPurchase ? transactionsEntity.customerPurchase.id : ''}</dd>
          <dt>Transaction</dt>
          <dd>{transactionsEntity.transaction ? transactionsEntity.transaction.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/transactions" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/transactions/${transactionsEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

export default TransactionsDetail;
