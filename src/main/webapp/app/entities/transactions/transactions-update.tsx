import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IAccounts } from 'app/shared/model/accounts.model';
import { getEntities as getAccounts } from 'app/entities/accounts/accounts.reducer';
import { ICustomerPurchase } from 'app/shared/model/customer-purchase.model';
import { getEntities as getCustomerPurchases } from 'app/entities/customer-purchase/customer-purchase.reducer';
import { IRefTransactionTypes } from 'app/shared/model/ref-transaction-types.model';
import { getEntities as getRefTransactionTypes } from 'app/entities/ref-transaction-types/ref-transaction-types.reducer';
import { ITransactions } from 'app/shared/model/transactions.model';
import { getEntity, updateEntity, createEntity, reset } from './transactions.reducer';

export const TransactionsUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const accounts = useAppSelector(state => state.accounts.entities);
  const customerPurchases = useAppSelector(state => state.customerPurchase.entities);
  const refTransactionTypes = useAppSelector(state => state.refTransactionTypes.entities);
  const transactionsEntity = useAppSelector(state => state.transactions.entity);
  const loading = useAppSelector(state => state.transactions.loading);
  const updating = useAppSelector(state => state.transactions.updating);
  const updateSuccess = useAppSelector(state => state.transactions.updateSuccess);

  const handleClose = () => {
    navigate('/transactions');
  };

  useEffect(() => {
    if (!isNew) {
      dispatch(getEntity(id));
    }

    dispatch(getAccounts({}));
    dispatch(getCustomerPurchases({}));
    dispatch(getRefTransactionTypes({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  // eslint-disable-next-line complexity
  const saveEntity = values => {
    if (values.id !== undefined && typeof values.id !== 'number') {
      values.id = Number(values.id);
    }

    const entity = {
      ...transactionsEntity,
      ...values,
      accounts: accounts.find(it => it.id.toString() === values.accounts.toString()),
      customerPurchase: customerPurchases.find(it => it.id.toString() === values.customerPurchase.toString()),
      transaction: refTransactionTypes.find(it => it.id.toString() === values.transaction.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...transactionsEntity,
          accounts: transactionsEntity?.accounts?.id,
          customerPurchase: transactionsEntity?.customerPurchase?.id,
          transaction: transactionsEntity?.transaction?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="onlineBankingApp.transactions.home.createOrEditLabel" data-cy="TransactionsCreateUpdateHeading">
            Create or edit a Transactions
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? <ValidatedField name="id" required readOnly id="transactions-id" label="ID" validate={{ required: true }} /> : null}
              <ValidatedField
                label="Transaction Unique Code"
                id="transactions-transactionUniqueCode"
                name="transactionUniqueCode"
                data-cy="transactionUniqueCode"
                type="text"
              />
              <ValidatedField id="transactions-accounts" name="accounts" data-cy="accounts" label="Accounts" type="select">
                <option value="" key="0" />
                {accounts
                  ? accounts.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="transactions-customerPurchase"
                name="customerPurchase"
                data-cy="customerPurchase"
                label="Customer Purchase"
                type="select"
              >
                <option value="" key="0" />
                {customerPurchases
                  ? customerPurchases.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField id="transactions-transaction" name="transaction" data-cy="transaction" label="Transaction" type="select">
                <option value="" key="0" />
                {refTransactionTypes
                  ? refTransactionTypes.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/transactions" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default TransactionsUpdate;
