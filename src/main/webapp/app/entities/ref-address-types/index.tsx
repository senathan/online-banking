import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import RefAddressTypes from './ref-address-types';
import RefAddressTypesDetail from './ref-address-types-detail';
import RefAddressTypesUpdate from './ref-address-types-update';
import RefAddressTypesDeleteDialog from './ref-address-types-delete-dialog';

const RefAddressTypesRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<RefAddressTypes />} />
    <Route path="new" element={<RefAddressTypesUpdate />} />
    <Route path=":id">
      <Route index element={<RefAddressTypesDetail />} />
      <Route path="edit" element={<RefAddressTypesUpdate />} />
      <Route path="delete" element={<RefAddressTypesDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default RefAddressTypesRoutes;
