import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import RefCustomerTypes from './ref-customer-types';
import RefAddressTypes from './ref-address-types';
import RefAccountTypes from './ref-account-types';
import RefTransactionTypes from './ref-transaction-types';
import Transactions from './transactions';
import Customers from './customers';
import Addresses from './addresses';
import Accounts from './accounts';
import CustomerAddresses from './customer-addresses';
import CustomerPurchase from './customer-purchase';
import Merchants from './merchants';
import BalanceHistory from './balance-history';
import ProductAndServices from './product-and-services';
/* jhipster-needle-add-route-import - JHipster will add routes here */

export default () => {
  return (
    <div>
      <ErrorBoundaryRoutes>
        {/* prettier-ignore */}
        <Route path="ref-customer-types/*" element={<RefCustomerTypes />} />
        <Route path="ref-address-types/*" element={<RefAddressTypes />} />
        <Route path="ref-account-types/*" element={<RefAccountTypes />} />
        <Route path="ref-transaction-types/*" element={<RefTransactionTypes />} />
        <Route path="transactions/*" element={<Transactions />} />
        <Route path="customers/*" element={<Customers />} />
        <Route path="addresses/*" element={<Addresses />} />
        <Route path="accounts/*" element={<Accounts />} />
        <Route path="customer-addresses/*" element={<CustomerAddresses />} />
        <Route path="customer-purchase/*" element={<CustomerPurchase />} />
        <Route path="merchants/*" element={<Merchants />} />
        <Route path="balance-history/*" element={<BalanceHistory />} />
        <Route path="product-and-services/*" element={<ProductAndServices />} />
        {/* jhipster-needle-add-route-path - JHipster will add routes here */}
      </ErrorBoundaryRoutes>
    </div>
  );
};
