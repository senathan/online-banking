package com.sen.online.domain;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class CustomerAddressesTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static CustomerAddresses getCustomerAddressesSample1() {
        return new CustomerAddresses().id(1L);
    }

    public static CustomerAddresses getCustomerAddressesSample2() {
        return new CustomerAddresses().id(2L);
    }

    public static CustomerAddresses getCustomerAddressesRandomSampleGenerator() {
        return new CustomerAddresses().id(longCount.incrementAndGet());
    }
}
