package com.sen.online.domain;

import static com.sen.online.domain.AccountsTestSamples.*;
import static com.sen.online.domain.BalanceHistoryTestSamples.*;
import static com.sen.online.domain.CustomersTestSamples.*;
import static com.sen.online.domain.RefAccountTypesTestSamples.*;
import static com.sen.online.domain.TransactionsTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class AccountsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Accounts.class);
        Accounts accounts1 = getAccountsSample1();
        Accounts accounts2 = new Accounts();
        assertThat(accounts1).isNotEqualTo(accounts2);

        accounts2.setId(accounts1.getId());
        assertThat(accounts1).isEqualTo(accounts2);

        accounts2 = getAccountsSample2();
        assertThat(accounts1).isNotEqualTo(accounts2);
    }

    @Test
    void accountTest() throws Exception {
        Accounts accounts = getAccountsRandomSampleGenerator();
        RefAccountTypes refAccountTypesBack = getRefAccountTypesRandomSampleGenerator();

        accounts.setAccount(refAccountTypesBack);
        assertThat(accounts.getAccount()).isEqualTo(refAccountTypesBack);

        accounts.account(null);
        assertThat(accounts.getAccount()).isNull();
    }

    @Test
    void customersTest() throws Exception {
        Accounts accounts = getAccountsRandomSampleGenerator();
        Customers customersBack = getCustomersRandomSampleGenerator();

        accounts.setCustomers(customersBack);
        assertThat(accounts.getCustomers()).isEqualTo(customersBack);

        accounts.customers(null);
        assertThat(accounts.getCustomers()).isNull();
    }

    @Test
    void balanceHistoryTest() throws Exception {
        Accounts accounts = getAccountsRandomSampleGenerator();
        BalanceHistory balanceHistoryBack = getBalanceHistoryRandomSampleGenerator();

        accounts.addBalanceHistory(balanceHistoryBack);
        assertThat(accounts.getBalanceHistories()).containsOnly(balanceHistoryBack);
        assertThat(balanceHistoryBack.getAccounts()).isEqualTo(accounts);

        accounts.removeBalanceHistory(balanceHistoryBack);
        assertThat(accounts.getBalanceHistories()).doesNotContain(balanceHistoryBack);
        assertThat(balanceHistoryBack.getAccounts()).isNull();

        accounts.balanceHistories(new HashSet<>(Set.of(balanceHistoryBack)));
        assertThat(accounts.getBalanceHistories()).containsOnly(balanceHistoryBack);
        assertThat(balanceHistoryBack.getAccounts()).isEqualTo(accounts);

        accounts.setBalanceHistories(new HashSet<>());
        assertThat(accounts.getBalanceHistories()).doesNotContain(balanceHistoryBack);
        assertThat(balanceHistoryBack.getAccounts()).isNull();
    }

    @Test
    void transactionsTest() throws Exception {
        Accounts accounts = getAccountsRandomSampleGenerator();
        Transactions transactionsBack = getTransactionsRandomSampleGenerator();

        accounts.addTransactions(transactionsBack);
        assertThat(accounts.getTransactions()).containsOnly(transactionsBack);
        assertThat(transactionsBack.getAccounts()).isEqualTo(accounts);

        accounts.removeTransactions(transactionsBack);
        assertThat(accounts.getTransactions()).doesNotContain(transactionsBack);
        assertThat(transactionsBack.getAccounts()).isNull();

        accounts.transactions(new HashSet<>(Set.of(transactionsBack)));
        assertThat(accounts.getTransactions()).containsOnly(transactionsBack);
        assertThat(transactionsBack.getAccounts()).isEqualTo(accounts);

        accounts.setTransactions(new HashSet<>());
        assertThat(accounts.getTransactions()).doesNotContain(transactionsBack);
        assertThat(transactionsBack.getAccounts()).isNull();
    }
}
