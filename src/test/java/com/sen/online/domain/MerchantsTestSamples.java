package com.sen.online.domain;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class MerchantsTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Merchants getMerchantsSample1() {
        return new Merchants().id(1L);
    }

    public static Merchants getMerchantsSample2() {
        return new Merchants().id(2L);
    }

    public static Merchants getMerchantsRandomSampleGenerator() {
        return new Merchants().id(longCount.incrementAndGet());
    }
}
