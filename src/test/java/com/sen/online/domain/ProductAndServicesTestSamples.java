package com.sen.online.domain;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class ProductAndServicesTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static ProductAndServices getProductAndServicesSample1() {
        return new ProductAndServices().id(1L);
    }

    public static ProductAndServices getProductAndServicesSample2() {
        return new ProductAndServices().id(2L);
    }

    public static ProductAndServices getProductAndServicesRandomSampleGenerator() {
        return new ProductAndServices().id(longCount.incrementAndGet());
    }
}
