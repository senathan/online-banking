package com.sen.online.domain;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class CustomerPurchaseTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static CustomerPurchase getCustomerPurchaseSample1() {
        return new CustomerPurchase().id(1L);
    }

    public static CustomerPurchase getCustomerPurchaseSample2() {
        return new CustomerPurchase().id(2L);
    }

    public static CustomerPurchase getCustomerPurchaseRandomSampleGenerator() {
        return new CustomerPurchase().id(longCount.incrementAndGet());
    }
}
