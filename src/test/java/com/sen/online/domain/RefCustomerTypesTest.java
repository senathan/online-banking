package com.sen.online.domain;

import static com.sen.online.domain.CustomersTestSamples.*;
import static com.sen.online.domain.RefCustomerTypesTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class RefCustomerTypesTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RefCustomerTypes.class);
        RefCustomerTypes refCustomerTypes1 = getRefCustomerTypesSample1();
        RefCustomerTypes refCustomerTypes2 = new RefCustomerTypes();
        assertThat(refCustomerTypes1).isNotEqualTo(refCustomerTypes2);

        refCustomerTypes2.setId(refCustomerTypes1.getId());
        assertThat(refCustomerTypes1).isEqualTo(refCustomerTypes2);

        refCustomerTypes2 = getRefCustomerTypesSample2();
        assertThat(refCustomerTypes1).isNotEqualTo(refCustomerTypes2);
    }

    @Test
    void customersTest() throws Exception {
        RefCustomerTypes refCustomerTypes = getRefCustomerTypesRandomSampleGenerator();
        Customers customersBack = getCustomersRandomSampleGenerator();

        refCustomerTypes.addCustomers(customersBack);
        assertThat(refCustomerTypes.getCustomers()).containsOnly(customersBack);
        assertThat(customersBack.getCustomer()).isEqualTo(refCustomerTypes);

        refCustomerTypes.removeCustomers(customersBack);
        assertThat(refCustomerTypes.getCustomers()).doesNotContain(customersBack);
        assertThat(customersBack.getCustomer()).isNull();

        refCustomerTypes.customers(new HashSet<>(Set.of(customersBack)));
        assertThat(refCustomerTypes.getCustomers()).containsOnly(customersBack);
        assertThat(customersBack.getCustomer()).isEqualTo(refCustomerTypes);

        refCustomerTypes.setCustomers(new HashSet<>());
        assertThat(refCustomerTypes.getCustomers()).doesNotContain(customersBack);
        assertThat(customersBack.getCustomer()).isNull();
    }
}
