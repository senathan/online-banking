package com.sen.online.domain;

import static com.sen.online.domain.AddressesTestSamples.*;
import static com.sen.online.domain.CustomerAddressesTestSamples.*;
import static com.sen.online.domain.CustomersTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CustomerAddressesTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomerAddresses.class);
        CustomerAddresses customerAddresses1 = getCustomerAddressesSample1();
        CustomerAddresses customerAddresses2 = new CustomerAddresses();
        assertThat(customerAddresses1).isNotEqualTo(customerAddresses2);

        customerAddresses2.setId(customerAddresses1.getId());
        assertThat(customerAddresses1).isEqualTo(customerAddresses2);

        customerAddresses2 = getCustomerAddressesSample2();
        assertThat(customerAddresses1).isNotEqualTo(customerAddresses2);
    }

    @Test
    void addressesTest() throws Exception {
        CustomerAddresses customerAddresses = getCustomerAddressesRandomSampleGenerator();
        Addresses addressesBack = getAddressesRandomSampleGenerator();

        customerAddresses.setAddresses(addressesBack);
        assertThat(customerAddresses.getAddresses()).isEqualTo(addressesBack);

        customerAddresses.addresses(null);
        assertThat(customerAddresses.getAddresses()).isNull();
    }

    @Test
    void customersTest() throws Exception {
        CustomerAddresses customerAddresses = getCustomerAddressesRandomSampleGenerator();
        Customers customersBack = getCustomersRandomSampleGenerator();

        customerAddresses.setCustomers(customersBack);
        assertThat(customerAddresses.getCustomers()).isEqualTo(customersBack);

        customerAddresses.customers(null);
        assertThat(customerAddresses.getCustomers()).isNull();
    }
}
