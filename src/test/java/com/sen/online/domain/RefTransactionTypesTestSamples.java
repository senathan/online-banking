package com.sen.online.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class RefTransactionTypesTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static RefTransactionTypes getRefTransactionTypesSample1() {
        return new RefTransactionTypes().id(1L).transactionTypeCode("transactionTypeCode1");
    }

    public static RefTransactionTypes getRefTransactionTypesSample2() {
        return new RefTransactionTypes().id(2L).transactionTypeCode("transactionTypeCode2");
    }

    public static RefTransactionTypes getRefTransactionTypesRandomSampleGenerator() {
        return new RefTransactionTypes().id(longCount.incrementAndGet()).transactionTypeCode(UUID.randomUUID().toString());
    }
}
