package com.sen.online.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class RefAccountTypesTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static RefAccountTypes getRefAccountTypesSample1() {
        return new RefAccountTypes().id(1L).accountTypeCode("accountTypeCode1");
    }

    public static RefAccountTypes getRefAccountTypesSample2() {
        return new RefAccountTypes().id(2L).accountTypeCode("accountTypeCode2");
    }

    public static RefAccountTypes getRefAccountTypesRandomSampleGenerator() {
        return new RefAccountTypes().id(longCount.incrementAndGet()).accountTypeCode(UUID.randomUUID().toString());
    }
}
