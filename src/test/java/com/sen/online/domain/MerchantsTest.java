package com.sen.online.domain;

import static com.sen.online.domain.MerchantsTestSamples.*;
import static com.sen.online.domain.ProductAndServicesTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MerchantsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Merchants.class);
        Merchants merchants1 = getMerchantsSample1();
        Merchants merchants2 = new Merchants();
        assertThat(merchants1).isNotEqualTo(merchants2);

        merchants2.setId(merchants1.getId());
        assertThat(merchants1).isEqualTo(merchants2);

        merchants2 = getMerchantsSample2();
        assertThat(merchants1).isNotEqualTo(merchants2);
    }

    @Test
    void merchantTest() throws Exception {
        Merchants merchants = getMerchantsRandomSampleGenerator();
        ProductAndServices productAndServicesBack = getProductAndServicesRandomSampleGenerator();

        merchants.setMerchant(productAndServicesBack);
        assertThat(merchants.getMerchant()).isEqualTo(productAndServicesBack);

        merchants.merchant(null);
        assertThat(merchants.getMerchant()).isNull();
    }
}
