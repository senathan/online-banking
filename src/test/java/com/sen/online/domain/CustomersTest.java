package com.sen.online.domain;

import static com.sen.online.domain.AccountsTestSamples.*;
import static com.sen.online.domain.CustomerAddressesTestSamples.*;
import static com.sen.online.domain.CustomerPurchaseTestSamples.*;
import static com.sen.online.domain.CustomersTestSamples.*;
import static com.sen.online.domain.RefCustomerTypesTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class CustomersTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Customers.class);
        Customers customers1 = getCustomersSample1();
        Customers customers2 = new Customers();
        assertThat(customers1).isNotEqualTo(customers2);

        customers2.setId(customers1.getId());
        assertThat(customers1).isEqualTo(customers2);

        customers2 = getCustomersSample2();
        assertThat(customers1).isNotEqualTo(customers2);
    }

    @Test
    void accountsTest() throws Exception {
        Customers customers = getCustomersRandomSampleGenerator();
        Accounts accountsBack = getAccountsRandomSampleGenerator();

        customers.addAccounts(accountsBack);
        assertThat(customers.getAccounts()).containsOnly(accountsBack);
        assertThat(accountsBack.getCustomers()).isEqualTo(customers);

        customers.removeAccounts(accountsBack);
        assertThat(customers.getAccounts()).doesNotContain(accountsBack);
        assertThat(accountsBack.getCustomers()).isNull();

        customers.accounts(new HashSet<>(Set.of(accountsBack)));
        assertThat(customers.getAccounts()).containsOnly(accountsBack);
        assertThat(accountsBack.getCustomers()).isEqualTo(customers);

        customers.setAccounts(new HashSet<>());
        assertThat(customers.getAccounts()).doesNotContain(accountsBack);
        assertThat(accountsBack.getCustomers()).isNull();
    }

    @Test
    void customerAddressesTest() throws Exception {
        Customers customers = getCustomersRandomSampleGenerator();
        CustomerAddresses customerAddressesBack = getCustomerAddressesRandomSampleGenerator();

        customers.addCustomerAddresses(customerAddressesBack);
        assertThat(customers.getCustomerAddresses()).containsOnly(customerAddressesBack);
        assertThat(customerAddressesBack.getCustomers()).isEqualTo(customers);

        customers.removeCustomerAddresses(customerAddressesBack);
        assertThat(customers.getCustomerAddresses()).doesNotContain(customerAddressesBack);
        assertThat(customerAddressesBack.getCustomers()).isNull();

        customers.customerAddresses(new HashSet<>(Set.of(customerAddressesBack)));
        assertThat(customers.getCustomerAddresses()).containsOnly(customerAddressesBack);
        assertThat(customerAddressesBack.getCustomers()).isEqualTo(customers);

        customers.setCustomerAddresses(new HashSet<>());
        assertThat(customers.getCustomerAddresses()).doesNotContain(customerAddressesBack);
        assertThat(customerAddressesBack.getCustomers()).isNull();
    }

    @Test
    void customerPurchaseTest() throws Exception {
        Customers customers = getCustomersRandomSampleGenerator();
        CustomerPurchase customerPurchaseBack = getCustomerPurchaseRandomSampleGenerator();

        customers.addCustomerPurchase(customerPurchaseBack);
        assertThat(customers.getCustomerPurchases()).containsOnly(customerPurchaseBack);
        assertThat(customerPurchaseBack.getCustomers()).isEqualTo(customers);

        customers.removeCustomerPurchase(customerPurchaseBack);
        assertThat(customers.getCustomerPurchases()).doesNotContain(customerPurchaseBack);
        assertThat(customerPurchaseBack.getCustomers()).isNull();

        customers.customerPurchases(new HashSet<>(Set.of(customerPurchaseBack)));
        assertThat(customers.getCustomerPurchases()).containsOnly(customerPurchaseBack);
        assertThat(customerPurchaseBack.getCustomers()).isEqualTo(customers);

        customers.setCustomerPurchases(new HashSet<>());
        assertThat(customers.getCustomerPurchases()).doesNotContain(customerPurchaseBack);
        assertThat(customerPurchaseBack.getCustomers()).isNull();
    }

    @Test
    void customerTest() throws Exception {
        Customers customers = getCustomersRandomSampleGenerator();
        RefCustomerTypes refCustomerTypesBack = getRefCustomerTypesRandomSampleGenerator();

        customers.setCustomer(refCustomerTypesBack);
        assertThat(customers.getCustomer()).isEqualTo(refCustomerTypesBack);

        customers.customer(null);
        assertThat(customers.getCustomer()).isNull();
    }
}
