package com.sen.online.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class BalanceHistoryTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static BalanceHistory getBalanceHistorySample1() {
        return new BalanceHistory().id(1L).balanceDate("balanceDate1");
    }

    public static BalanceHistory getBalanceHistorySample2() {
        return new BalanceHistory().id(2L).balanceDate("balanceDate2");
    }

    public static BalanceHistory getBalanceHistoryRandomSampleGenerator() {
        return new BalanceHistory().id(longCount.incrementAndGet()).balanceDate(UUID.randomUUID().toString());
    }
}
