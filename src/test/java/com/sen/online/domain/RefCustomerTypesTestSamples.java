package com.sen.online.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class RefCustomerTypesTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static RefCustomerTypes getRefCustomerTypesSample1() {
        return new RefCustomerTypes().id(1L).customerTypeCode("customerTypeCode1");
    }

    public static RefCustomerTypes getRefCustomerTypesSample2() {
        return new RefCustomerTypes().id(2L).customerTypeCode("customerTypeCode2");
    }

    public static RefCustomerTypes getRefCustomerTypesRandomSampleGenerator() {
        return new RefCustomerTypes().id(longCount.incrementAndGet()).customerTypeCode(UUID.randomUUID().toString());
    }
}
