package com.sen.online.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class RefAddressTypesTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static RefAddressTypes getRefAddressTypesSample1() {
        return new RefAddressTypes().id(1L).addressTypeCode("addressTypeCode1");
    }

    public static RefAddressTypes getRefAddressTypesSample2() {
        return new RefAddressTypes().id(2L).addressTypeCode("addressTypeCode2");
    }

    public static RefAddressTypes getRefAddressTypesRandomSampleGenerator() {
        return new RefAddressTypes().id(longCount.incrementAndGet()).addressTypeCode(UUID.randomUUID().toString());
    }
}
