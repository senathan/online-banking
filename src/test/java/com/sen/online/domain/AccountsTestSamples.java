package com.sen.online.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class AccountsTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Accounts getAccountsSample1() {
        return new Accounts().id(1L).accountDesc("accountDesc1");
    }

    public static Accounts getAccountsSample2() {
        return new Accounts().id(2L).accountDesc("accountDesc2");
    }

    public static Accounts getAccountsRandomSampleGenerator() {
        return new Accounts().id(longCount.incrementAndGet()).accountDesc(UUID.randomUUID().toString());
    }
}
