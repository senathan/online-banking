package com.sen.online.domain;

import static com.sen.online.domain.CustomerPurchaseTestSamples.*;
import static com.sen.online.domain.ProductAndServicesTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class ProductAndServicesTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductAndServices.class);
        ProductAndServices productAndServices1 = getProductAndServicesSample1();
        ProductAndServices productAndServices2 = new ProductAndServices();
        assertThat(productAndServices1).isNotEqualTo(productAndServices2);

        productAndServices2.setId(productAndServices1.getId());
        assertThat(productAndServices1).isEqualTo(productAndServices2);

        productAndServices2 = getProductAndServicesSample2();
        assertThat(productAndServices1).isNotEqualTo(productAndServices2);
    }

    @Test
    void customerPurchaseTest() throws Exception {
        ProductAndServices productAndServices = getProductAndServicesRandomSampleGenerator();
        CustomerPurchase customerPurchaseBack = getCustomerPurchaseRandomSampleGenerator();

        productAndServices.addCustomerPurchase(customerPurchaseBack);
        assertThat(productAndServices.getCustomerPurchases()).containsOnly(customerPurchaseBack);
        assertThat(customerPurchaseBack.getProductAndServices()).isEqualTo(productAndServices);

        productAndServices.removeCustomerPurchase(customerPurchaseBack);
        assertThat(productAndServices.getCustomerPurchases()).doesNotContain(customerPurchaseBack);
        assertThat(customerPurchaseBack.getProductAndServices()).isNull();

        productAndServices.customerPurchases(new HashSet<>(Set.of(customerPurchaseBack)));
        assertThat(productAndServices.getCustomerPurchases()).containsOnly(customerPurchaseBack);
        assertThat(customerPurchaseBack.getProductAndServices()).isEqualTo(productAndServices);

        productAndServices.setCustomerPurchases(new HashSet<>());
        assertThat(productAndServices.getCustomerPurchases()).doesNotContain(customerPurchaseBack);
        assertThat(customerPurchaseBack.getProductAndServices()).isNull();
    }
}
