package com.sen.online.domain;

import static com.sen.online.domain.CustomerPurchaseTestSamples.*;
import static com.sen.online.domain.CustomersTestSamples.*;
import static com.sen.online.domain.ProductAndServicesTestSamples.*;
import static com.sen.online.domain.TransactionsTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class CustomerPurchaseTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomerPurchase.class);
        CustomerPurchase customerPurchase1 = getCustomerPurchaseSample1();
        CustomerPurchase customerPurchase2 = new CustomerPurchase();
        assertThat(customerPurchase1).isNotEqualTo(customerPurchase2);

        customerPurchase2.setId(customerPurchase1.getId());
        assertThat(customerPurchase1).isEqualTo(customerPurchase2);

        customerPurchase2 = getCustomerPurchaseSample2();
        assertThat(customerPurchase1).isNotEqualTo(customerPurchase2);
    }

    @Test
    void customersTest() throws Exception {
        CustomerPurchase customerPurchase = getCustomerPurchaseRandomSampleGenerator();
        Customers customersBack = getCustomersRandomSampleGenerator();

        customerPurchase.setCustomers(customersBack);
        assertThat(customerPurchase.getCustomers()).isEqualTo(customersBack);

        customerPurchase.customers(null);
        assertThat(customerPurchase.getCustomers()).isNull();
    }

    @Test
    void productAndServicesTest() throws Exception {
        CustomerPurchase customerPurchase = getCustomerPurchaseRandomSampleGenerator();
        ProductAndServices productAndServicesBack = getProductAndServicesRandomSampleGenerator();

        customerPurchase.setProductAndServices(productAndServicesBack);
        assertThat(customerPurchase.getProductAndServices()).isEqualTo(productAndServicesBack);

        customerPurchase.productAndServices(null);
        assertThat(customerPurchase.getProductAndServices()).isNull();
    }

    @Test
    void transactionsTest() throws Exception {
        CustomerPurchase customerPurchase = getCustomerPurchaseRandomSampleGenerator();
        Transactions transactionsBack = getTransactionsRandomSampleGenerator();

        customerPurchase.addTransactions(transactionsBack);
        assertThat(customerPurchase.getTransactions()).containsOnly(transactionsBack);
        assertThat(transactionsBack.getCustomerPurchase()).isEqualTo(customerPurchase);

        customerPurchase.removeTransactions(transactionsBack);
        assertThat(customerPurchase.getTransactions()).doesNotContain(transactionsBack);
        assertThat(transactionsBack.getCustomerPurchase()).isNull();

        customerPurchase.transactions(new HashSet<>(Set.of(transactionsBack)));
        assertThat(customerPurchase.getTransactions()).containsOnly(transactionsBack);
        assertThat(transactionsBack.getCustomerPurchase()).isEqualTo(customerPurchase);

        customerPurchase.setTransactions(new HashSet<>());
        assertThat(customerPurchase.getTransactions()).doesNotContain(transactionsBack);
        assertThat(transactionsBack.getCustomerPurchase()).isNull();
    }
}
