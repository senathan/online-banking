package com.sen.online.domain;

import static com.sen.online.domain.AccountsTestSamples.*;
import static com.sen.online.domain.RefAccountTypesTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class RefAccountTypesTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RefAccountTypes.class);
        RefAccountTypes refAccountTypes1 = getRefAccountTypesSample1();
        RefAccountTypes refAccountTypes2 = new RefAccountTypes();
        assertThat(refAccountTypes1).isNotEqualTo(refAccountTypes2);

        refAccountTypes2.setId(refAccountTypes1.getId());
        assertThat(refAccountTypes1).isEqualTo(refAccountTypes2);

        refAccountTypes2 = getRefAccountTypesSample2();
        assertThat(refAccountTypes1).isNotEqualTo(refAccountTypes2);
    }

    @Test
    void accountsTest() throws Exception {
        RefAccountTypes refAccountTypes = getRefAccountTypesRandomSampleGenerator();
        Accounts accountsBack = getAccountsRandomSampleGenerator();

        refAccountTypes.addAccounts(accountsBack);
        assertThat(refAccountTypes.getAccounts()).containsOnly(accountsBack);
        assertThat(accountsBack.getAccount()).isEqualTo(refAccountTypes);

        refAccountTypes.removeAccounts(accountsBack);
        assertThat(refAccountTypes.getAccounts()).doesNotContain(accountsBack);
        assertThat(accountsBack.getAccount()).isNull();

        refAccountTypes.accounts(new HashSet<>(Set.of(accountsBack)));
        assertThat(refAccountTypes.getAccounts()).containsOnly(accountsBack);
        assertThat(accountsBack.getAccount()).isEqualTo(refAccountTypes);

        refAccountTypes.setAccounts(new HashSet<>());
        assertThat(refAccountTypes.getAccounts()).doesNotContain(accountsBack);
        assertThat(accountsBack.getAccount()).isNull();
    }
}
