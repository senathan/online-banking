package com.sen.online.domain;

import static com.sen.online.domain.AddressesTestSamples.*;
import static com.sen.online.domain.RefAddressTypesTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class RefAddressTypesTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RefAddressTypes.class);
        RefAddressTypes refAddressTypes1 = getRefAddressTypesSample1();
        RefAddressTypes refAddressTypes2 = new RefAddressTypes();
        assertThat(refAddressTypes1).isNotEqualTo(refAddressTypes2);

        refAddressTypes2.setId(refAddressTypes1.getId());
        assertThat(refAddressTypes1).isEqualTo(refAddressTypes2);

        refAddressTypes2 = getRefAddressTypesSample2();
        assertThat(refAddressTypes1).isNotEqualTo(refAddressTypes2);
    }

    @Test
    void addressesTest() throws Exception {
        RefAddressTypes refAddressTypes = getRefAddressTypesRandomSampleGenerator();
        Addresses addressesBack = getAddressesRandomSampleGenerator();

        refAddressTypes.addAddresses(addressesBack);
        assertThat(refAddressTypes.getAddresses()).containsOnly(addressesBack);
        assertThat(addressesBack.getAddress()).isEqualTo(refAddressTypes);

        refAddressTypes.removeAddresses(addressesBack);
        assertThat(refAddressTypes.getAddresses()).doesNotContain(addressesBack);
        assertThat(addressesBack.getAddress()).isNull();

        refAddressTypes.addresses(new HashSet<>(Set.of(addressesBack)));
        assertThat(refAddressTypes.getAddresses()).containsOnly(addressesBack);
        assertThat(addressesBack.getAddress()).isEqualTo(refAddressTypes);

        refAddressTypes.setAddresses(new HashSet<>());
        assertThat(refAddressTypes.getAddresses()).doesNotContain(addressesBack);
        assertThat(addressesBack.getAddress()).isNull();
    }
}
