package com.sen.online.domain;

import static com.sen.online.domain.AccountsTestSamples.*;
import static com.sen.online.domain.BalanceHistoryTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class BalanceHistoryTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BalanceHistory.class);
        BalanceHistory balanceHistory1 = getBalanceHistorySample1();
        BalanceHistory balanceHistory2 = new BalanceHistory();
        assertThat(balanceHistory1).isNotEqualTo(balanceHistory2);

        balanceHistory2.setId(balanceHistory1.getId());
        assertThat(balanceHistory1).isEqualTo(balanceHistory2);

        balanceHistory2 = getBalanceHistorySample2();
        assertThat(balanceHistory1).isNotEqualTo(balanceHistory2);
    }

    @Test
    void accountsTest() throws Exception {
        BalanceHistory balanceHistory = getBalanceHistoryRandomSampleGenerator();
        Accounts accountsBack = getAccountsRandomSampleGenerator();

        balanceHistory.setAccounts(accountsBack);
        assertThat(balanceHistory.getAccounts()).isEqualTo(accountsBack);

        balanceHistory.accounts(null);
        assertThat(balanceHistory.getAccounts()).isNull();
    }
}
