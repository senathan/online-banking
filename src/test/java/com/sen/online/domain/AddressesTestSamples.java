package com.sen.online.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class AddressesTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Addresses getAddressesSample1() {
        return new Addresses().id(1L).addressLine1("addressLine11").addressLine2("addressLine21").postcode("postcode1");
    }

    public static Addresses getAddressesSample2() {
        return new Addresses().id(2L).addressLine1("addressLine12").addressLine2("addressLine22").postcode("postcode2");
    }

    public static Addresses getAddressesRandomSampleGenerator() {
        return new Addresses()
            .id(longCount.incrementAndGet())
            .addressLine1(UUID.randomUUID().toString())
            .addressLine2(UUID.randomUUID().toString())
            .postcode(UUID.randomUUID().toString());
    }
}
