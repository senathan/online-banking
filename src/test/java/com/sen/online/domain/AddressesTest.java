package com.sen.online.domain;

import static com.sen.online.domain.AddressesTestSamples.*;
import static com.sen.online.domain.CustomerAddressesTestSamples.*;
import static com.sen.online.domain.RefAddressTypesTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class AddressesTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Addresses.class);
        Addresses addresses1 = getAddressesSample1();
        Addresses addresses2 = new Addresses();
        assertThat(addresses1).isNotEqualTo(addresses2);

        addresses2.setId(addresses1.getId());
        assertThat(addresses1).isEqualTo(addresses2);

        addresses2 = getAddressesSample2();
        assertThat(addresses1).isNotEqualTo(addresses2);
    }

    @Test
    void addressTest() throws Exception {
        Addresses addresses = getAddressesRandomSampleGenerator();
        RefAddressTypes refAddressTypesBack = getRefAddressTypesRandomSampleGenerator();

        addresses.setAddress(refAddressTypesBack);
        assertThat(addresses.getAddress()).isEqualTo(refAddressTypesBack);

        addresses.address(null);
        assertThat(addresses.getAddress()).isNull();
    }

    @Test
    void customerAddressesTest() throws Exception {
        Addresses addresses = getAddressesRandomSampleGenerator();
        CustomerAddresses customerAddressesBack = getCustomerAddressesRandomSampleGenerator();

        addresses.addCustomerAddresses(customerAddressesBack);
        assertThat(addresses.getCustomerAddresses()).containsOnly(customerAddressesBack);
        assertThat(customerAddressesBack.getAddresses()).isEqualTo(addresses);

        addresses.removeCustomerAddresses(customerAddressesBack);
        assertThat(addresses.getCustomerAddresses()).doesNotContain(customerAddressesBack);
        assertThat(customerAddressesBack.getAddresses()).isNull();

        addresses.customerAddresses(new HashSet<>(Set.of(customerAddressesBack)));
        assertThat(addresses.getCustomerAddresses()).containsOnly(customerAddressesBack);
        assertThat(customerAddressesBack.getAddresses()).isEqualTo(addresses);

        addresses.setCustomerAddresses(new HashSet<>());
        assertThat(addresses.getCustomerAddresses()).doesNotContain(customerAddressesBack);
        assertThat(customerAddressesBack.getAddresses()).isNull();
    }
}
