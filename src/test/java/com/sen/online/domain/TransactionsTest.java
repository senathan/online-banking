package com.sen.online.domain;

import static com.sen.online.domain.AccountsTestSamples.*;
import static com.sen.online.domain.CustomerPurchaseTestSamples.*;
import static com.sen.online.domain.RefTransactionTypesTestSamples.*;
import static com.sen.online.domain.TransactionsTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TransactionsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Transactions.class);
        Transactions transactions1 = getTransactionsSample1();
        Transactions transactions2 = new Transactions();
        assertThat(transactions1).isNotEqualTo(transactions2);

        transactions2.setId(transactions1.getId());
        assertThat(transactions1).isEqualTo(transactions2);

        transactions2 = getTransactionsSample2();
        assertThat(transactions1).isNotEqualTo(transactions2);
    }

    @Test
    void accountsTest() throws Exception {
        Transactions transactions = getTransactionsRandomSampleGenerator();
        Accounts accountsBack = getAccountsRandomSampleGenerator();

        transactions.setAccounts(accountsBack);
        assertThat(transactions.getAccounts()).isEqualTo(accountsBack);

        transactions.accounts(null);
        assertThat(transactions.getAccounts()).isNull();
    }

    @Test
    void customerPurchaseTest() throws Exception {
        Transactions transactions = getTransactionsRandomSampleGenerator();
        CustomerPurchase customerPurchaseBack = getCustomerPurchaseRandomSampleGenerator();

        transactions.setCustomerPurchase(customerPurchaseBack);
        assertThat(transactions.getCustomerPurchase()).isEqualTo(customerPurchaseBack);

        transactions.customerPurchase(null);
        assertThat(transactions.getCustomerPurchase()).isNull();
    }

    @Test
    void transactionTest() throws Exception {
        Transactions transactions = getTransactionsRandomSampleGenerator();
        RefTransactionTypes refTransactionTypesBack = getRefTransactionTypesRandomSampleGenerator();

        transactions.setTransaction(refTransactionTypesBack);
        assertThat(transactions.getTransaction()).isEqualTo(refTransactionTypesBack);

        transactions.transaction(null);
        assertThat(transactions.getTransaction()).isNull();
    }
}
