package com.sen.online.domain;

import static com.sen.online.domain.RefTransactionTypesTestSamples.*;
import static com.sen.online.domain.TransactionsTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.sen.online.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class RefTransactionTypesTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RefTransactionTypes.class);
        RefTransactionTypes refTransactionTypes1 = getRefTransactionTypesSample1();
        RefTransactionTypes refTransactionTypes2 = new RefTransactionTypes();
        assertThat(refTransactionTypes1).isNotEqualTo(refTransactionTypes2);

        refTransactionTypes2.setId(refTransactionTypes1.getId());
        assertThat(refTransactionTypes1).isEqualTo(refTransactionTypes2);

        refTransactionTypes2 = getRefTransactionTypesSample2();
        assertThat(refTransactionTypes1).isNotEqualTo(refTransactionTypes2);
    }

    @Test
    void transactionsTest() throws Exception {
        RefTransactionTypes refTransactionTypes = getRefTransactionTypesRandomSampleGenerator();
        Transactions transactionsBack = getTransactionsRandomSampleGenerator();

        refTransactionTypes.addTransactions(transactionsBack);
        assertThat(refTransactionTypes.getTransactions()).containsOnly(transactionsBack);
        assertThat(transactionsBack.getTransaction()).isEqualTo(refTransactionTypes);

        refTransactionTypes.removeTransactions(transactionsBack);
        assertThat(refTransactionTypes.getTransactions()).doesNotContain(transactionsBack);
        assertThat(transactionsBack.getTransaction()).isNull();

        refTransactionTypes.transactions(new HashSet<>(Set.of(transactionsBack)));
        assertThat(refTransactionTypes.getTransactions()).containsOnly(transactionsBack);
        assertThat(transactionsBack.getTransaction()).isEqualTo(refTransactionTypes);

        refTransactionTypes.setTransactions(new HashSet<>());
        assertThat(refTransactionTypes.getTransactions()).doesNotContain(transactionsBack);
        assertThat(transactionsBack.getTransaction()).isNull();
    }
}
