package com.sen.online.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sen.online.IntegrationTest;
import com.sen.online.domain.RefAddressTypes;
import com.sen.online.repository.RefAddressTypesRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link RefAddressTypesResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class RefAddressTypesResourceIT {

    private static final String DEFAULT_ADDRESS_TYPE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_TYPE_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/ref-address-types";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RefAddressTypesRepository refAddressTypesRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRefAddressTypesMockMvc;

    private RefAddressTypes refAddressTypes;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RefAddressTypes createEntity(EntityManager em) {
        RefAddressTypes refAddressTypes = new RefAddressTypes().addressTypeCode(DEFAULT_ADDRESS_TYPE_CODE);
        return refAddressTypes;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RefAddressTypes createUpdatedEntity(EntityManager em) {
        RefAddressTypes refAddressTypes = new RefAddressTypes().addressTypeCode(UPDATED_ADDRESS_TYPE_CODE);
        return refAddressTypes;
    }

    @BeforeEach
    public void initTest() {
        refAddressTypes = createEntity(em);
    }

    @Test
    @Transactional
    void createRefAddressTypes() throws Exception {
        int databaseSizeBeforeCreate = refAddressTypesRepository.findAll().size();
        // Create the RefAddressTypes
        restRefAddressTypesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refAddressTypes))
            )
            .andExpect(status().isCreated());

        // Validate the RefAddressTypes in the database
        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeCreate + 1);
        RefAddressTypes testRefAddressTypes = refAddressTypesList.get(refAddressTypesList.size() - 1);
        assertThat(testRefAddressTypes.getAddressTypeCode()).isEqualTo(DEFAULT_ADDRESS_TYPE_CODE);
    }

    @Test
    @Transactional
    void createRefAddressTypesWithExistingId() throws Exception {
        // Create the RefAddressTypes with an existing ID
        refAddressTypes.setId(1L);

        int databaseSizeBeforeCreate = refAddressTypesRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRefAddressTypesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refAddressTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefAddressTypes in the database
        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkAddressTypeCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = refAddressTypesRepository.findAll().size();
        // set the field null
        refAddressTypes.setAddressTypeCode(null);

        // Create the RefAddressTypes, which fails.

        restRefAddressTypesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refAddressTypes))
            )
            .andExpect(status().isBadRequest());

        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllRefAddressTypes() throws Exception {
        // Initialize the database
        refAddressTypesRepository.saveAndFlush(refAddressTypes);

        // Get all the refAddressTypesList
        restRefAddressTypesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(refAddressTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].addressTypeCode").value(hasItem(DEFAULT_ADDRESS_TYPE_CODE)));
    }

    @Test
    @Transactional
    void getRefAddressTypes() throws Exception {
        // Initialize the database
        refAddressTypesRepository.saveAndFlush(refAddressTypes);

        // Get the refAddressTypes
        restRefAddressTypesMockMvc
            .perform(get(ENTITY_API_URL_ID, refAddressTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(refAddressTypes.getId().intValue()))
            .andExpect(jsonPath("$.addressTypeCode").value(DEFAULT_ADDRESS_TYPE_CODE));
    }

    @Test
    @Transactional
    void getNonExistingRefAddressTypes() throws Exception {
        // Get the refAddressTypes
        restRefAddressTypesMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingRefAddressTypes() throws Exception {
        // Initialize the database
        refAddressTypesRepository.saveAndFlush(refAddressTypes);

        int databaseSizeBeforeUpdate = refAddressTypesRepository.findAll().size();

        // Update the refAddressTypes
        RefAddressTypes updatedRefAddressTypes = refAddressTypesRepository.findById(refAddressTypes.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedRefAddressTypes are not directly saved in db
        em.detach(updatedRefAddressTypes);
        updatedRefAddressTypes.addressTypeCode(UPDATED_ADDRESS_TYPE_CODE);

        restRefAddressTypesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedRefAddressTypes.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedRefAddressTypes))
            )
            .andExpect(status().isOk());

        // Validate the RefAddressTypes in the database
        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeUpdate);
        RefAddressTypes testRefAddressTypes = refAddressTypesList.get(refAddressTypesList.size() - 1);
        assertThat(testRefAddressTypes.getAddressTypeCode()).isEqualTo(UPDATED_ADDRESS_TYPE_CODE);
    }

    @Test
    @Transactional
    void putNonExistingRefAddressTypes() throws Exception {
        int databaseSizeBeforeUpdate = refAddressTypesRepository.findAll().size();
        refAddressTypes.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRefAddressTypesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, refAddressTypes.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(refAddressTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefAddressTypes in the database
        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchRefAddressTypes() throws Exception {
        int databaseSizeBeforeUpdate = refAddressTypesRepository.findAll().size();
        refAddressTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefAddressTypesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(refAddressTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefAddressTypes in the database
        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRefAddressTypes() throws Exception {
        int databaseSizeBeforeUpdate = refAddressTypesRepository.findAll().size();
        refAddressTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefAddressTypesMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refAddressTypes))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RefAddressTypes in the database
        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateRefAddressTypesWithPatch() throws Exception {
        // Initialize the database
        refAddressTypesRepository.saveAndFlush(refAddressTypes);

        int databaseSizeBeforeUpdate = refAddressTypesRepository.findAll().size();

        // Update the refAddressTypes using partial update
        RefAddressTypes partialUpdatedRefAddressTypes = new RefAddressTypes();
        partialUpdatedRefAddressTypes.setId(refAddressTypes.getId());

        partialUpdatedRefAddressTypes.addressTypeCode(UPDATED_ADDRESS_TYPE_CODE);

        restRefAddressTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRefAddressTypes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRefAddressTypes))
            )
            .andExpect(status().isOk());

        // Validate the RefAddressTypes in the database
        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeUpdate);
        RefAddressTypes testRefAddressTypes = refAddressTypesList.get(refAddressTypesList.size() - 1);
        assertThat(testRefAddressTypes.getAddressTypeCode()).isEqualTo(UPDATED_ADDRESS_TYPE_CODE);
    }

    @Test
    @Transactional
    void fullUpdateRefAddressTypesWithPatch() throws Exception {
        // Initialize the database
        refAddressTypesRepository.saveAndFlush(refAddressTypes);

        int databaseSizeBeforeUpdate = refAddressTypesRepository.findAll().size();

        // Update the refAddressTypes using partial update
        RefAddressTypes partialUpdatedRefAddressTypes = new RefAddressTypes();
        partialUpdatedRefAddressTypes.setId(refAddressTypes.getId());

        partialUpdatedRefAddressTypes.addressTypeCode(UPDATED_ADDRESS_TYPE_CODE);

        restRefAddressTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRefAddressTypes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRefAddressTypes))
            )
            .andExpect(status().isOk());

        // Validate the RefAddressTypes in the database
        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeUpdate);
        RefAddressTypes testRefAddressTypes = refAddressTypesList.get(refAddressTypesList.size() - 1);
        assertThat(testRefAddressTypes.getAddressTypeCode()).isEqualTo(UPDATED_ADDRESS_TYPE_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingRefAddressTypes() throws Exception {
        int databaseSizeBeforeUpdate = refAddressTypesRepository.findAll().size();
        refAddressTypes.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRefAddressTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, refAddressTypes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refAddressTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefAddressTypes in the database
        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRefAddressTypes() throws Exception {
        int databaseSizeBeforeUpdate = refAddressTypesRepository.findAll().size();
        refAddressTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefAddressTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refAddressTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefAddressTypes in the database
        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRefAddressTypes() throws Exception {
        int databaseSizeBeforeUpdate = refAddressTypesRepository.findAll().size();
        refAddressTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefAddressTypesMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refAddressTypes))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RefAddressTypes in the database
        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteRefAddressTypes() throws Exception {
        // Initialize the database
        refAddressTypesRepository.saveAndFlush(refAddressTypes);

        int databaseSizeBeforeDelete = refAddressTypesRepository.findAll().size();

        // Delete the refAddressTypes
        restRefAddressTypesMockMvc
            .perform(delete(ENTITY_API_URL_ID, refAddressTypes.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RefAddressTypes> refAddressTypesList = refAddressTypesRepository.findAll();
        assertThat(refAddressTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
