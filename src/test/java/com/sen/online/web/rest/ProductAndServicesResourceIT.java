package com.sen.online.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sen.online.IntegrationTest;
import com.sen.online.domain.ProductAndServices;
import com.sen.online.repository.ProductAndServicesRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ProductAndServicesResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ProductAndServicesResourceIT {

    private static final String ENTITY_API_URL = "/api/product-and-services";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ProductAndServicesRepository productAndServicesRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProductAndServicesMockMvc;

    private ProductAndServices productAndServices;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductAndServices createEntity(EntityManager em) {
        ProductAndServices productAndServices = new ProductAndServices();
        return productAndServices;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductAndServices createUpdatedEntity(EntityManager em) {
        ProductAndServices productAndServices = new ProductAndServices();
        return productAndServices;
    }

    @BeforeEach
    public void initTest() {
        productAndServices = createEntity(em);
    }

    @Test
    @Transactional
    void createProductAndServices() throws Exception {
        int databaseSizeBeforeCreate = productAndServicesRepository.findAll().size();
        // Create the ProductAndServices
        restProductAndServicesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(productAndServices))
            )
            .andExpect(status().isCreated());

        // Validate the ProductAndServices in the database
        List<ProductAndServices> productAndServicesList = productAndServicesRepository.findAll();
        assertThat(productAndServicesList).hasSize(databaseSizeBeforeCreate + 1);
        ProductAndServices testProductAndServices = productAndServicesList.get(productAndServicesList.size() - 1);
    }

    @Test
    @Transactional
    void createProductAndServicesWithExistingId() throws Exception {
        // Create the ProductAndServices with an existing ID
        productAndServices.setId(1L);

        int databaseSizeBeforeCreate = productAndServicesRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductAndServicesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(productAndServices))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductAndServices in the database
        List<ProductAndServices> productAndServicesList = productAndServicesRepository.findAll();
        assertThat(productAndServicesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllProductAndServices() throws Exception {
        // Initialize the database
        productAndServicesRepository.saveAndFlush(productAndServices);

        // Get all the productAndServicesList
        restProductAndServicesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productAndServices.getId().intValue())));
    }

    @Test
    @Transactional
    void getProductAndServices() throws Exception {
        // Initialize the database
        productAndServicesRepository.saveAndFlush(productAndServices);

        // Get the productAndServices
        restProductAndServicesMockMvc
            .perform(get(ENTITY_API_URL_ID, productAndServices.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(productAndServices.getId().intValue()));
    }

    @Test
    @Transactional
    void getNonExistingProductAndServices() throws Exception {
        // Get the productAndServices
        restProductAndServicesMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingProductAndServices() throws Exception {
        // Initialize the database
        productAndServicesRepository.saveAndFlush(productAndServices);

        int databaseSizeBeforeUpdate = productAndServicesRepository.findAll().size();

        // Update the productAndServices
        ProductAndServices updatedProductAndServices = productAndServicesRepository.findById(productAndServices.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedProductAndServices are not directly saved in db
        em.detach(updatedProductAndServices);

        restProductAndServicesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedProductAndServices.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedProductAndServices))
            )
            .andExpect(status().isOk());

        // Validate the ProductAndServices in the database
        List<ProductAndServices> productAndServicesList = productAndServicesRepository.findAll();
        assertThat(productAndServicesList).hasSize(databaseSizeBeforeUpdate);
        ProductAndServices testProductAndServices = productAndServicesList.get(productAndServicesList.size() - 1);
    }

    @Test
    @Transactional
    void putNonExistingProductAndServices() throws Exception {
        int databaseSizeBeforeUpdate = productAndServicesRepository.findAll().size();
        productAndServices.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductAndServicesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, productAndServices.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(productAndServices))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductAndServices in the database
        List<ProductAndServices> productAndServicesList = productAndServicesRepository.findAll();
        assertThat(productAndServicesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchProductAndServices() throws Exception {
        int databaseSizeBeforeUpdate = productAndServicesRepository.findAll().size();
        productAndServices.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductAndServicesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(productAndServices))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductAndServices in the database
        List<ProductAndServices> productAndServicesList = productAndServicesRepository.findAll();
        assertThat(productAndServicesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamProductAndServices() throws Exception {
        int databaseSizeBeforeUpdate = productAndServicesRepository.findAll().size();
        productAndServices.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductAndServicesMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(productAndServices))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ProductAndServices in the database
        List<ProductAndServices> productAndServicesList = productAndServicesRepository.findAll();
        assertThat(productAndServicesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateProductAndServicesWithPatch() throws Exception {
        // Initialize the database
        productAndServicesRepository.saveAndFlush(productAndServices);

        int databaseSizeBeforeUpdate = productAndServicesRepository.findAll().size();

        // Update the productAndServices using partial update
        ProductAndServices partialUpdatedProductAndServices = new ProductAndServices();
        partialUpdatedProductAndServices.setId(productAndServices.getId());

        restProductAndServicesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProductAndServices.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedProductAndServices))
            )
            .andExpect(status().isOk());

        // Validate the ProductAndServices in the database
        List<ProductAndServices> productAndServicesList = productAndServicesRepository.findAll();
        assertThat(productAndServicesList).hasSize(databaseSizeBeforeUpdate);
        ProductAndServices testProductAndServices = productAndServicesList.get(productAndServicesList.size() - 1);
    }

    @Test
    @Transactional
    void fullUpdateProductAndServicesWithPatch() throws Exception {
        // Initialize the database
        productAndServicesRepository.saveAndFlush(productAndServices);

        int databaseSizeBeforeUpdate = productAndServicesRepository.findAll().size();

        // Update the productAndServices using partial update
        ProductAndServices partialUpdatedProductAndServices = new ProductAndServices();
        partialUpdatedProductAndServices.setId(productAndServices.getId());

        restProductAndServicesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProductAndServices.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedProductAndServices))
            )
            .andExpect(status().isOk());

        // Validate the ProductAndServices in the database
        List<ProductAndServices> productAndServicesList = productAndServicesRepository.findAll();
        assertThat(productAndServicesList).hasSize(databaseSizeBeforeUpdate);
        ProductAndServices testProductAndServices = productAndServicesList.get(productAndServicesList.size() - 1);
    }

    @Test
    @Transactional
    void patchNonExistingProductAndServices() throws Exception {
        int databaseSizeBeforeUpdate = productAndServicesRepository.findAll().size();
        productAndServices.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductAndServicesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, productAndServices.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(productAndServices))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductAndServices in the database
        List<ProductAndServices> productAndServicesList = productAndServicesRepository.findAll();
        assertThat(productAndServicesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchProductAndServices() throws Exception {
        int databaseSizeBeforeUpdate = productAndServicesRepository.findAll().size();
        productAndServices.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductAndServicesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(productAndServices))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductAndServices in the database
        List<ProductAndServices> productAndServicesList = productAndServicesRepository.findAll();
        assertThat(productAndServicesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamProductAndServices() throws Exception {
        int databaseSizeBeforeUpdate = productAndServicesRepository.findAll().size();
        productAndServices.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductAndServicesMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(productAndServices))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ProductAndServices in the database
        List<ProductAndServices> productAndServicesList = productAndServicesRepository.findAll();
        assertThat(productAndServicesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteProductAndServices() throws Exception {
        // Initialize the database
        productAndServicesRepository.saveAndFlush(productAndServices);

        int databaseSizeBeforeDelete = productAndServicesRepository.findAll().size();

        // Delete the productAndServices
        restProductAndServicesMockMvc
            .perform(delete(ENTITY_API_URL_ID, productAndServices.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductAndServices> productAndServicesList = productAndServicesRepository.findAll();
        assertThat(productAndServicesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
