package com.sen.online.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sen.online.IntegrationTest;
import com.sen.online.domain.RefAccountTypes;
import com.sen.online.repository.RefAccountTypesRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link RefAccountTypesResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class RefAccountTypesResourceIT {

    private static final String DEFAULT_ACCOUNT_TYPE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_TYPE_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/ref-account-types";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RefAccountTypesRepository refAccountTypesRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRefAccountTypesMockMvc;

    private RefAccountTypes refAccountTypes;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RefAccountTypes createEntity(EntityManager em) {
        RefAccountTypes refAccountTypes = new RefAccountTypes().accountTypeCode(DEFAULT_ACCOUNT_TYPE_CODE);
        return refAccountTypes;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RefAccountTypes createUpdatedEntity(EntityManager em) {
        RefAccountTypes refAccountTypes = new RefAccountTypes().accountTypeCode(UPDATED_ACCOUNT_TYPE_CODE);
        return refAccountTypes;
    }

    @BeforeEach
    public void initTest() {
        refAccountTypes = createEntity(em);
    }

    @Test
    @Transactional
    void createRefAccountTypes() throws Exception {
        int databaseSizeBeforeCreate = refAccountTypesRepository.findAll().size();
        // Create the RefAccountTypes
        restRefAccountTypesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refAccountTypes))
            )
            .andExpect(status().isCreated());

        // Validate the RefAccountTypes in the database
        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeCreate + 1);
        RefAccountTypes testRefAccountTypes = refAccountTypesList.get(refAccountTypesList.size() - 1);
        assertThat(testRefAccountTypes.getAccountTypeCode()).isEqualTo(DEFAULT_ACCOUNT_TYPE_CODE);
    }

    @Test
    @Transactional
    void createRefAccountTypesWithExistingId() throws Exception {
        // Create the RefAccountTypes with an existing ID
        refAccountTypes.setId(1L);

        int databaseSizeBeforeCreate = refAccountTypesRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRefAccountTypesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refAccountTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefAccountTypes in the database
        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkAccountTypeCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = refAccountTypesRepository.findAll().size();
        // set the field null
        refAccountTypes.setAccountTypeCode(null);

        // Create the RefAccountTypes, which fails.

        restRefAccountTypesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refAccountTypes))
            )
            .andExpect(status().isBadRequest());

        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllRefAccountTypes() throws Exception {
        // Initialize the database
        refAccountTypesRepository.saveAndFlush(refAccountTypes);

        // Get all the refAccountTypesList
        restRefAccountTypesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(refAccountTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].accountTypeCode").value(hasItem(DEFAULT_ACCOUNT_TYPE_CODE)));
    }

    @Test
    @Transactional
    void getRefAccountTypes() throws Exception {
        // Initialize the database
        refAccountTypesRepository.saveAndFlush(refAccountTypes);

        // Get the refAccountTypes
        restRefAccountTypesMockMvc
            .perform(get(ENTITY_API_URL_ID, refAccountTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(refAccountTypes.getId().intValue()))
            .andExpect(jsonPath("$.accountTypeCode").value(DEFAULT_ACCOUNT_TYPE_CODE));
    }

    @Test
    @Transactional
    void getNonExistingRefAccountTypes() throws Exception {
        // Get the refAccountTypes
        restRefAccountTypesMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingRefAccountTypes() throws Exception {
        // Initialize the database
        refAccountTypesRepository.saveAndFlush(refAccountTypes);

        int databaseSizeBeforeUpdate = refAccountTypesRepository.findAll().size();

        // Update the refAccountTypes
        RefAccountTypes updatedRefAccountTypes = refAccountTypesRepository.findById(refAccountTypes.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedRefAccountTypes are not directly saved in db
        em.detach(updatedRefAccountTypes);
        updatedRefAccountTypes.accountTypeCode(UPDATED_ACCOUNT_TYPE_CODE);

        restRefAccountTypesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedRefAccountTypes.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedRefAccountTypes))
            )
            .andExpect(status().isOk());

        // Validate the RefAccountTypes in the database
        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeUpdate);
        RefAccountTypes testRefAccountTypes = refAccountTypesList.get(refAccountTypesList.size() - 1);
        assertThat(testRefAccountTypes.getAccountTypeCode()).isEqualTo(UPDATED_ACCOUNT_TYPE_CODE);
    }

    @Test
    @Transactional
    void putNonExistingRefAccountTypes() throws Exception {
        int databaseSizeBeforeUpdate = refAccountTypesRepository.findAll().size();
        refAccountTypes.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRefAccountTypesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, refAccountTypes.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(refAccountTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefAccountTypes in the database
        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchRefAccountTypes() throws Exception {
        int databaseSizeBeforeUpdate = refAccountTypesRepository.findAll().size();
        refAccountTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefAccountTypesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(refAccountTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefAccountTypes in the database
        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRefAccountTypes() throws Exception {
        int databaseSizeBeforeUpdate = refAccountTypesRepository.findAll().size();
        refAccountTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefAccountTypesMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refAccountTypes))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RefAccountTypes in the database
        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateRefAccountTypesWithPatch() throws Exception {
        // Initialize the database
        refAccountTypesRepository.saveAndFlush(refAccountTypes);

        int databaseSizeBeforeUpdate = refAccountTypesRepository.findAll().size();

        // Update the refAccountTypes using partial update
        RefAccountTypes partialUpdatedRefAccountTypes = new RefAccountTypes();
        partialUpdatedRefAccountTypes.setId(refAccountTypes.getId());

        partialUpdatedRefAccountTypes.accountTypeCode(UPDATED_ACCOUNT_TYPE_CODE);

        restRefAccountTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRefAccountTypes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRefAccountTypes))
            )
            .andExpect(status().isOk());

        // Validate the RefAccountTypes in the database
        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeUpdate);
        RefAccountTypes testRefAccountTypes = refAccountTypesList.get(refAccountTypesList.size() - 1);
        assertThat(testRefAccountTypes.getAccountTypeCode()).isEqualTo(UPDATED_ACCOUNT_TYPE_CODE);
    }

    @Test
    @Transactional
    void fullUpdateRefAccountTypesWithPatch() throws Exception {
        // Initialize the database
        refAccountTypesRepository.saveAndFlush(refAccountTypes);

        int databaseSizeBeforeUpdate = refAccountTypesRepository.findAll().size();

        // Update the refAccountTypes using partial update
        RefAccountTypes partialUpdatedRefAccountTypes = new RefAccountTypes();
        partialUpdatedRefAccountTypes.setId(refAccountTypes.getId());

        partialUpdatedRefAccountTypes.accountTypeCode(UPDATED_ACCOUNT_TYPE_CODE);

        restRefAccountTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRefAccountTypes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRefAccountTypes))
            )
            .andExpect(status().isOk());

        // Validate the RefAccountTypes in the database
        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeUpdate);
        RefAccountTypes testRefAccountTypes = refAccountTypesList.get(refAccountTypesList.size() - 1);
        assertThat(testRefAccountTypes.getAccountTypeCode()).isEqualTo(UPDATED_ACCOUNT_TYPE_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingRefAccountTypes() throws Exception {
        int databaseSizeBeforeUpdate = refAccountTypesRepository.findAll().size();
        refAccountTypes.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRefAccountTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, refAccountTypes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refAccountTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefAccountTypes in the database
        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRefAccountTypes() throws Exception {
        int databaseSizeBeforeUpdate = refAccountTypesRepository.findAll().size();
        refAccountTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefAccountTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refAccountTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefAccountTypes in the database
        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRefAccountTypes() throws Exception {
        int databaseSizeBeforeUpdate = refAccountTypesRepository.findAll().size();
        refAccountTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefAccountTypesMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refAccountTypes))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RefAccountTypes in the database
        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteRefAccountTypes() throws Exception {
        // Initialize the database
        refAccountTypesRepository.saveAndFlush(refAccountTypes);

        int databaseSizeBeforeDelete = refAccountTypesRepository.findAll().size();

        // Delete the refAccountTypes
        restRefAccountTypesMockMvc
            .perform(delete(ENTITY_API_URL_ID, refAccountTypes.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RefAccountTypes> refAccountTypesList = refAccountTypesRepository.findAll();
        assertThat(refAccountTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
