package com.sen.online.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sen.online.IntegrationTest;
import com.sen.online.domain.RefCustomerTypes;
import com.sen.online.repository.RefCustomerTypesRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link RefCustomerTypesResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class RefCustomerTypesResourceIT {

    private static final String DEFAULT_CUSTOMER_TYPE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_TYPE_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/ref-customer-types";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RefCustomerTypesRepository refCustomerTypesRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRefCustomerTypesMockMvc;

    private RefCustomerTypes refCustomerTypes;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RefCustomerTypes createEntity(EntityManager em) {
        RefCustomerTypes refCustomerTypes = new RefCustomerTypes().customerTypeCode(DEFAULT_CUSTOMER_TYPE_CODE);
        return refCustomerTypes;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RefCustomerTypes createUpdatedEntity(EntityManager em) {
        RefCustomerTypes refCustomerTypes = new RefCustomerTypes().customerTypeCode(UPDATED_CUSTOMER_TYPE_CODE);
        return refCustomerTypes;
    }

    @BeforeEach
    public void initTest() {
        refCustomerTypes = createEntity(em);
    }

    @Test
    @Transactional
    void createRefCustomerTypes() throws Exception {
        int databaseSizeBeforeCreate = refCustomerTypesRepository.findAll().size();
        // Create the RefCustomerTypes
        restRefCustomerTypesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refCustomerTypes))
            )
            .andExpect(status().isCreated());

        // Validate the RefCustomerTypes in the database
        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeCreate + 1);
        RefCustomerTypes testRefCustomerTypes = refCustomerTypesList.get(refCustomerTypesList.size() - 1);
        assertThat(testRefCustomerTypes.getCustomerTypeCode()).isEqualTo(DEFAULT_CUSTOMER_TYPE_CODE);
    }

    @Test
    @Transactional
    void createRefCustomerTypesWithExistingId() throws Exception {
        // Create the RefCustomerTypes with an existing ID
        refCustomerTypes.setId(1L);

        int databaseSizeBeforeCreate = refCustomerTypesRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRefCustomerTypesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refCustomerTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefCustomerTypes in the database
        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCustomerTypeCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = refCustomerTypesRepository.findAll().size();
        // set the field null
        refCustomerTypes.setCustomerTypeCode(null);

        // Create the RefCustomerTypes, which fails.

        restRefCustomerTypesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refCustomerTypes))
            )
            .andExpect(status().isBadRequest());

        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllRefCustomerTypes() throws Exception {
        // Initialize the database
        refCustomerTypesRepository.saveAndFlush(refCustomerTypes);

        // Get all the refCustomerTypesList
        restRefCustomerTypesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(refCustomerTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].customerTypeCode").value(hasItem(DEFAULT_CUSTOMER_TYPE_CODE)));
    }

    @Test
    @Transactional
    void getRefCustomerTypes() throws Exception {
        // Initialize the database
        refCustomerTypesRepository.saveAndFlush(refCustomerTypes);

        // Get the refCustomerTypes
        restRefCustomerTypesMockMvc
            .perform(get(ENTITY_API_URL_ID, refCustomerTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(refCustomerTypes.getId().intValue()))
            .andExpect(jsonPath("$.customerTypeCode").value(DEFAULT_CUSTOMER_TYPE_CODE));
    }

    @Test
    @Transactional
    void getNonExistingRefCustomerTypes() throws Exception {
        // Get the refCustomerTypes
        restRefCustomerTypesMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingRefCustomerTypes() throws Exception {
        // Initialize the database
        refCustomerTypesRepository.saveAndFlush(refCustomerTypes);

        int databaseSizeBeforeUpdate = refCustomerTypesRepository.findAll().size();

        // Update the refCustomerTypes
        RefCustomerTypes updatedRefCustomerTypes = refCustomerTypesRepository.findById(refCustomerTypes.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedRefCustomerTypes are not directly saved in db
        em.detach(updatedRefCustomerTypes);
        updatedRefCustomerTypes.customerTypeCode(UPDATED_CUSTOMER_TYPE_CODE);

        restRefCustomerTypesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedRefCustomerTypes.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedRefCustomerTypes))
            )
            .andExpect(status().isOk());

        // Validate the RefCustomerTypes in the database
        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeUpdate);
        RefCustomerTypes testRefCustomerTypes = refCustomerTypesList.get(refCustomerTypesList.size() - 1);
        assertThat(testRefCustomerTypes.getCustomerTypeCode()).isEqualTo(UPDATED_CUSTOMER_TYPE_CODE);
    }

    @Test
    @Transactional
    void putNonExistingRefCustomerTypes() throws Exception {
        int databaseSizeBeforeUpdate = refCustomerTypesRepository.findAll().size();
        refCustomerTypes.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRefCustomerTypesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, refCustomerTypes.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(refCustomerTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefCustomerTypes in the database
        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchRefCustomerTypes() throws Exception {
        int databaseSizeBeforeUpdate = refCustomerTypesRepository.findAll().size();
        refCustomerTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefCustomerTypesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(refCustomerTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefCustomerTypes in the database
        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRefCustomerTypes() throws Exception {
        int databaseSizeBeforeUpdate = refCustomerTypesRepository.findAll().size();
        refCustomerTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefCustomerTypesMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refCustomerTypes))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RefCustomerTypes in the database
        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateRefCustomerTypesWithPatch() throws Exception {
        // Initialize the database
        refCustomerTypesRepository.saveAndFlush(refCustomerTypes);

        int databaseSizeBeforeUpdate = refCustomerTypesRepository.findAll().size();

        // Update the refCustomerTypes using partial update
        RefCustomerTypes partialUpdatedRefCustomerTypes = new RefCustomerTypes();
        partialUpdatedRefCustomerTypes.setId(refCustomerTypes.getId());

        restRefCustomerTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRefCustomerTypes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRefCustomerTypes))
            )
            .andExpect(status().isOk());

        // Validate the RefCustomerTypes in the database
        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeUpdate);
        RefCustomerTypes testRefCustomerTypes = refCustomerTypesList.get(refCustomerTypesList.size() - 1);
        assertThat(testRefCustomerTypes.getCustomerTypeCode()).isEqualTo(DEFAULT_CUSTOMER_TYPE_CODE);
    }

    @Test
    @Transactional
    void fullUpdateRefCustomerTypesWithPatch() throws Exception {
        // Initialize the database
        refCustomerTypesRepository.saveAndFlush(refCustomerTypes);

        int databaseSizeBeforeUpdate = refCustomerTypesRepository.findAll().size();

        // Update the refCustomerTypes using partial update
        RefCustomerTypes partialUpdatedRefCustomerTypes = new RefCustomerTypes();
        partialUpdatedRefCustomerTypes.setId(refCustomerTypes.getId());

        partialUpdatedRefCustomerTypes.customerTypeCode(UPDATED_CUSTOMER_TYPE_CODE);

        restRefCustomerTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRefCustomerTypes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRefCustomerTypes))
            )
            .andExpect(status().isOk());

        // Validate the RefCustomerTypes in the database
        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeUpdate);
        RefCustomerTypes testRefCustomerTypes = refCustomerTypesList.get(refCustomerTypesList.size() - 1);
        assertThat(testRefCustomerTypes.getCustomerTypeCode()).isEqualTo(UPDATED_CUSTOMER_TYPE_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingRefCustomerTypes() throws Exception {
        int databaseSizeBeforeUpdate = refCustomerTypesRepository.findAll().size();
        refCustomerTypes.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRefCustomerTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, refCustomerTypes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refCustomerTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefCustomerTypes in the database
        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRefCustomerTypes() throws Exception {
        int databaseSizeBeforeUpdate = refCustomerTypesRepository.findAll().size();
        refCustomerTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefCustomerTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refCustomerTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefCustomerTypes in the database
        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRefCustomerTypes() throws Exception {
        int databaseSizeBeforeUpdate = refCustomerTypesRepository.findAll().size();
        refCustomerTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefCustomerTypesMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refCustomerTypes))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RefCustomerTypes in the database
        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteRefCustomerTypes() throws Exception {
        // Initialize the database
        refCustomerTypesRepository.saveAndFlush(refCustomerTypes);

        int databaseSizeBeforeDelete = refCustomerTypesRepository.findAll().size();

        // Delete the refCustomerTypes
        restRefCustomerTypesMockMvc
            .perform(delete(ENTITY_API_URL_ID, refCustomerTypes.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RefCustomerTypes> refCustomerTypesList = refCustomerTypesRepository.findAll();
        assertThat(refCustomerTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
