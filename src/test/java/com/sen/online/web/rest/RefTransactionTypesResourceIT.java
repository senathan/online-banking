package com.sen.online.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sen.online.IntegrationTest;
import com.sen.online.domain.RefTransactionTypes;
import com.sen.online.repository.RefTransactionTypesRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link RefTransactionTypesResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class RefTransactionTypesResourceIT {

    private static final String DEFAULT_TRANSACTION_TYPE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_TRANSACTION_TYPE_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/ref-transaction-types";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RefTransactionTypesRepository refTransactionTypesRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRefTransactionTypesMockMvc;

    private RefTransactionTypes refTransactionTypes;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RefTransactionTypes createEntity(EntityManager em) {
        RefTransactionTypes refTransactionTypes = new RefTransactionTypes().transactionTypeCode(DEFAULT_TRANSACTION_TYPE_CODE);
        return refTransactionTypes;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RefTransactionTypes createUpdatedEntity(EntityManager em) {
        RefTransactionTypes refTransactionTypes = new RefTransactionTypes().transactionTypeCode(UPDATED_TRANSACTION_TYPE_CODE);
        return refTransactionTypes;
    }

    @BeforeEach
    public void initTest() {
        refTransactionTypes = createEntity(em);
    }

    @Test
    @Transactional
    void createRefTransactionTypes() throws Exception {
        int databaseSizeBeforeCreate = refTransactionTypesRepository.findAll().size();
        // Create the RefTransactionTypes
        restRefTransactionTypesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refTransactionTypes))
            )
            .andExpect(status().isCreated());

        // Validate the RefTransactionTypes in the database
        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeCreate + 1);
        RefTransactionTypes testRefTransactionTypes = refTransactionTypesList.get(refTransactionTypesList.size() - 1);
        assertThat(testRefTransactionTypes.getTransactionTypeCode()).isEqualTo(DEFAULT_TRANSACTION_TYPE_CODE);
    }

    @Test
    @Transactional
    void createRefTransactionTypesWithExistingId() throws Exception {
        // Create the RefTransactionTypes with an existing ID
        refTransactionTypes.setId(1L);

        int databaseSizeBeforeCreate = refTransactionTypesRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRefTransactionTypesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refTransactionTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefTransactionTypes in the database
        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTransactionTypeCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = refTransactionTypesRepository.findAll().size();
        // set the field null
        refTransactionTypes.setTransactionTypeCode(null);

        // Create the RefTransactionTypes, which fails.

        restRefTransactionTypesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refTransactionTypes))
            )
            .andExpect(status().isBadRequest());

        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllRefTransactionTypes() throws Exception {
        // Initialize the database
        refTransactionTypesRepository.saveAndFlush(refTransactionTypes);

        // Get all the refTransactionTypesList
        restRefTransactionTypesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(refTransactionTypes.getId().intValue())))
            .andExpect(jsonPath("$.[*].transactionTypeCode").value(hasItem(DEFAULT_TRANSACTION_TYPE_CODE)));
    }

    @Test
    @Transactional
    void getRefTransactionTypes() throws Exception {
        // Initialize the database
        refTransactionTypesRepository.saveAndFlush(refTransactionTypes);

        // Get the refTransactionTypes
        restRefTransactionTypesMockMvc
            .perform(get(ENTITY_API_URL_ID, refTransactionTypes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(refTransactionTypes.getId().intValue()))
            .andExpect(jsonPath("$.transactionTypeCode").value(DEFAULT_TRANSACTION_TYPE_CODE));
    }

    @Test
    @Transactional
    void getNonExistingRefTransactionTypes() throws Exception {
        // Get the refTransactionTypes
        restRefTransactionTypesMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingRefTransactionTypes() throws Exception {
        // Initialize the database
        refTransactionTypesRepository.saveAndFlush(refTransactionTypes);

        int databaseSizeBeforeUpdate = refTransactionTypesRepository.findAll().size();

        // Update the refTransactionTypes
        RefTransactionTypes updatedRefTransactionTypes = refTransactionTypesRepository.findById(refTransactionTypes.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedRefTransactionTypes are not directly saved in db
        em.detach(updatedRefTransactionTypes);
        updatedRefTransactionTypes.transactionTypeCode(UPDATED_TRANSACTION_TYPE_CODE);

        restRefTransactionTypesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedRefTransactionTypes.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedRefTransactionTypes))
            )
            .andExpect(status().isOk());

        // Validate the RefTransactionTypes in the database
        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeUpdate);
        RefTransactionTypes testRefTransactionTypes = refTransactionTypesList.get(refTransactionTypesList.size() - 1);
        assertThat(testRefTransactionTypes.getTransactionTypeCode()).isEqualTo(UPDATED_TRANSACTION_TYPE_CODE);
    }

    @Test
    @Transactional
    void putNonExistingRefTransactionTypes() throws Exception {
        int databaseSizeBeforeUpdate = refTransactionTypesRepository.findAll().size();
        refTransactionTypes.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRefTransactionTypesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, refTransactionTypes.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(refTransactionTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefTransactionTypes in the database
        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchRefTransactionTypes() throws Exception {
        int databaseSizeBeforeUpdate = refTransactionTypesRepository.findAll().size();
        refTransactionTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefTransactionTypesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(refTransactionTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefTransactionTypes in the database
        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRefTransactionTypes() throws Exception {
        int databaseSizeBeforeUpdate = refTransactionTypesRepository.findAll().size();
        refTransactionTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefTransactionTypesMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refTransactionTypes))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RefTransactionTypes in the database
        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateRefTransactionTypesWithPatch() throws Exception {
        // Initialize the database
        refTransactionTypesRepository.saveAndFlush(refTransactionTypes);

        int databaseSizeBeforeUpdate = refTransactionTypesRepository.findAll().size();

        // Update the refTransactionTypes using partial update
        RefTransactionTypes partialUpdatedRefTransactionTypes = new RefTransactionTypes();
        partialUpdatedRefTransactionTypes.setId(refTransactionTypes.getId());

        partialUpdatedRefTransactionTypes.transactionTypeCode(UPDATED_TRANSACTION_TYPE_CODE);

        restRefTransactionTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRefTransactionTypes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRefTransactionTypes))
            )
            .andExpect(status().isOk());

        // Validate the RefTransactionTypes in the database
        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeUpdate);
        RefTransactionTypes testRefTransactionTypes = refTransactionTypesList.get(refTransactionTypesList.size() - 1);
        assertThat(testRefTransactionTypes.getTransactionTypeCode()).isEqualTo(UPDATED_TRANSACTION_TYPE_CODE);
    }

    @Test
    @Transactional
    void fullUpdateRefTransactionTypesWithPatch() throws Exception {
        // Initialize the database
        refTransactionTypesRepository.saveAndFlush(refTransactionTypes);

        int databaseSizeBeforeUpdate = refTransactionTypesRepository.findAll().size();

        // Update the refTransactionTypes using partial update
        RefTransactionTypes partialUpdatedRefTransactionTypes = new RefTransactionTypes();
        partialUpdatedRefTransactionTypes.setId(refTransactionTypes.getId());

        partialUpdatedRefTransactionTypes.transactionTypeCode(UPDATED_TRANSACTION_TYPE_CODE);

        restRefTransactionTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRefTransactionTypes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRefTransactionTypes))
            )
            .andExpect(status().isOk());

        // Validate the RefTransactionTypes in the database
        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeUpdate);
        RefTransactionTypes testRefTransactionTypes = refTransactionTypesList.get(refTransactionTypesList.size() - 1);
        assertThat(testRefTransactionTypes.getTransactionTypeCode()).isEqualTo(UPDATED_TRANSACTION_TYPE_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingRefTransactionTypes() throws Exception {
        int databaseSizeBeforeUpdate = refTransactionTypesRepository.findAll().size();
        refTransactionTypes.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRefTransactionTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, refTransactionTypes.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refTransactionTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefTransactionTypes in the database
        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRefTransactionTypes() throws Exception {
        int databaseSizeBeforeUpdate = refTransactionTypesRepository.findAll().size();
        refTransactionTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefTransactionTypesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refTransactionTypes))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefTransactionTypes in the database
        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRefTransactionTypes() throws Exception {
        int databaseSizeBeforeUpdate = refTransactionTypesRepository.findAll().size();
        refTransactionTypes.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefTransactionTypesMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refTransactionTypes))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RefTransactionTypes in the database
        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteRefTransactionTypes() throws Exception {
        // Initialize the database
        refTransactionTypesRepository.saveAndFlush(refTransactionTypes);

        int databaseSizeBeforeDelete = refTransactionTypesRepository.findAll().size();

        // Delete the refTransactionTypes
        restRefTransactionTypesMockMvc
            .perform(delete(ENTITY_API_URL_ID, refTransactionTypes.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RefTransactionTypes> refTransactionTypesList = refTransactionTypesRepository.findAll();
        assertThat(refTransactionTypesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
