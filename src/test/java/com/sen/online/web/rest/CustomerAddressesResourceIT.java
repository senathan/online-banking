package com.sen.online.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sen.online.IntegrationTest;
import com.sen.online.domain.CustomerAddresses;
import com.sen.online.repository.CustomerAddressesRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CustomerAddressesResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CustomerAddressesResourceIT {

    private static final String ENTITY_API_URL = "/api/customer-addresses";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CustomerAddressesRepository customerAddressesRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCustomerAddressesMockMvc;

    private CustomerAddresses customerAddresses;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerAddresses createEntity(EntityManager em) {
        CustomerAddresses customerAddresses = new CustomerAddresses();
        return customerAddresses;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerAddresses createUpdatedEntity(EntityManager em) {
        CustomerAddresses customerAddresses = new CustomerAddresses();
        return customerAddresses;
    }

    @BeforeEach
    public void initTest() {
        customerAddresses = createEntity(em);
    }

    @Test
    @Transactional
    void createCustomerAddresses() throws Exception {
        int databaseSizeBeforeCreate = customerAddressesRepository.findAll().size();
        // Create the CustomerAddresses
        restCustomerAddressesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(customerAddresses))
            )
            .andExpect(status().isCreated());

        // Validate the CustomerAddresses in the database
        List<CustomerAddresses> customerAddressesList = customerAddressesRepository.findAll();
        assertThat(customerAddressesList).hasSize(databaseSizeBeforeCreate + 1);
        CustomerAddresses testCustomerAddresses = customerAddressesList.get(customerAddressesList.size() - 1);
    }

    @Test
    @Transactional
    void createCustomerAddressesWithExistingId() throws Exception {
        // Create the CustomerAddresses with an existing ID
        customerAddresses.setId(1L);

        int databaseSizeBeforeCreate = customerAddressesRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomerAddressesMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(customerAddresses))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerAddresses in the database
        List<CustomerAddresses> customerAddressesList = customerAddressesRepository.findAll();
        assertThat(customerAddressesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCustomerAddresses() throws Exception {
        // Initialize the database
        customerAddressesRepository.saveAndFlush(customerAddresses);

        // Get all the customerAddressesList
        restCustomerAddressesMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customerAddresses.getId().intValue())));
    }

    @Test
    @Transactional
    void getCustomerAddresses() throws Exception {
        // Initialize the database
        customerAddressesRepository.saveAndFlush(customerAddresses);

        // Get the customerAddresses
        restCustomerAddressesMockMvc
            .perform(get(ENTITY_API_URL_ID, customerAddresses.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(customerAddresses.getId().intValue()));
    }

    @Test
    @Transactional
    void getNonExistingCustomerAddresses() throws Exception {
        // Get the customerAddresses
        restCustomerAddressesMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCustomerAddresses() throws Exception {
        // Initialize the database
        customerAddressesRepository.saveAndFlush(customerAddresses);

        int databaseSizeBeforeUpdate = customerAddressesRepository.findAll().size();

        // Update the customerAddresses
        CustomerAddresses updatedCustomerAddresses = customerAddressesRepository.findById(customerAddresses.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedCustomerAddresses are not directly saved in db
        em.detach(updatedCustomerAddresses);

        restCustomerAddressesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCustomerAddresses.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCustomerAddresses))
            )
            .andExpect(status().isOk());

        // Validate the CustomerAddresses in the database
        List<CustomerAddresses> customerAddressesList = customerAddressesRepository.findAll();
        assertThat(customerAddressesList).hasSize(databaseSizeBeforeUpdate);
        CustomerAddresses testCustomerAddresses = customerAddressesList.get(customerAddressesList.size() - 1);
    }

    @Test
    @Transactional
    void putNonExistingCustomerAddresses() throws Exception {
        int databaseSizeBeforeUpdate = customerAddressesRepository.findAll().size();
        customerAddresses.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustomerAddressesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, customerAddresses.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(customerAddresses))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerAddresses in the database
        List<CustomerAddresses> customerAddressesList = customerAddressesRepository.findAll();
        assertThat(customerAddressesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCustomerAddresses() throws Exception {
        int databaseSizeBeforeUpdate = customerAddressesRepository.findAll().size();
        customerAddresses.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerAddressesMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(customerAddresses))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerAddresses in the database
        List<CustomerAddresses> customerAddressesList = customerAddressesRepository.findAll();
        assertThat(customerAddressesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCustomerAddresses() throws Exception {
        int databaseSizeBeforeUpdate = customerAddressesRepository.findAll().size();
        customerAddresses.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerAddressesMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(customerAddresses))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CustomerAddresses in the database
        List<CustomerAddresses> customerAddressesList = customerAddressesRepository.findAll();
        assertThat(customerAddressesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCustomerAddressesWithPatch() throws Exception {
        // Initialize the database
        customerAddressesRepository.saveAndFlush(customerAddresses);

        int databaseSizeBeforeUpdate = customerAddressesRepository.findAll().size();

        // Update the customerAddresses using partial update
        CustomerAddresses partialUpdatedCustomerAddresses = new CustomerAddresses();
        partialUpdatedCustomerAddresses.setId(customerAddresses.getId());

        restCustomerAddressesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCustomerAddresses.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCustomerAddresses))
            )
            .andExpect(status().isOk());

        // Validate the CustomerAddresses in the database
        List<CustomerAddresses> customerAddressesList = customerAddressesRepository.findAll();
        assertThat(customerAddressesList).hasSize(databaseSizeBeforeUpdate);
        CustomerAddresses testCustomerAddresses = customerAddressesList.get(customerAddressesList.size() - 1);
    }

    @Test
    @Transactional
    void fullUpdateCustomerAddressesWithPatch() throws Exception {
        // Initialize the database
        customerAddressesRepository.saveAndFlush(customerAddresses);

        int databaseSizeBeforeUpdate = customerAddressesRepository.findAll().size();

        // Update the customerAddresses using partial update
        CustomerAddresses partialUpdatedCustomerAddresses = new CustomerAddresses();
        partialUpdatedCustomerAddresses.setId(customerAddresses.getId());

        restCustomerAddressesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCustomerAddresses.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCustomerAddresses))
            )
            .andExpect(status().isOk());

        // Validate the CustomerAddresses in the database
        List<CustomerAddresses> customerAddressesList = customerAddressesRepository.findAll();
        assertThat(customerAddressesList).hasSize(databaseSizeBeforeUpdate);
        CustomerAddresses testCustomerAddresses = customerAddressesList.get(customerAddressesList.size() - 1);
    }

    @Test
    @Transactional
    void patchNonExistingCustomerAddresses() throws Exception {
        int databaseSizeBeforeUpdate = customerAddressesRepository.findAll().size();
        customerAddresses.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustomerAddressesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, customerAddresses.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(customerAddresses))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerAddresses in the database
        List<CustomerAddresses> customerAddressesList = customerAddressesRepository.findAll();
        assertThat(customerAddressesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCustomerAddresses() throws Exception {
        int databaseSizeBeforeUpdate = customerAddressesRepository.findAll().size();
        customerAddresses.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerAddressesMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(customerAddresses))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerAddresses in the database
        List<CustomerAddresses> customerAddressesList = customerAddressesRepository.findAll();
        assertThat(customerAddressesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCustomerAddresses() throws Exception {
        int databaseSizeBeforeUpdate = customerAddressesRepository.findAll().size();
        customerAddresses.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerAddressesMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(customerAddresses))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CustomerAddresses in the database
        List<CustomerAddresses> customerAddressesList = customerAddressesRepository.findAll();
        assertThat(customerAddressesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCustomerAddresses() throws Exception {
        // Initialize the database
        customerAddressesRepository.saveAndFlush(customerAddresses);

        int databaseSizeBeforeDelete = customerAddressesRepository.findAll().size();

        // Delete the customerAddresses
        restCustomerAddressesMockMvc
            .perform(delete(ENTITY_API_URL_ID, customerAddresses.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CustomerAddresses> customerAddressesList = customerAddressesRepository.findAll();
        assertThat(customerAddressesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
