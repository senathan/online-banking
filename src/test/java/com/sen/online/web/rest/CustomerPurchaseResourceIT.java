package com.sen.online.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sen.online.IntegrationTest;
import com.sen.online.domain.CustomerPurchase;
import com.sen.online.repository.CustomerPurchaseRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CustomerPurchaseResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CustomerPurchaseResourceIT {

    private static final String ENTITY_API_URL = "/api/customer-purchases";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CustomerPurchaseRepository customerPurchaseRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCustomerPurchaseMockMvc;

    private CustomerPurchase customerPurchase;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerPurchase createEntity(EntityManager em) {
        CustomerPurchase customerPurchase = new CustomerPurchase();
        return customerPurchase;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerPurchase createUpdatedEntity(EntityManager em) {
        CustomerPurchase customerPurchase = new CustomerPurchase();
        return customerPurchase;
    }

    @BeforeEach
    public void initTest() {
        customerPurchase = createEntity(em);
    }

    @Test
    @Transactional
    void createCustomerPurchase() throws Exception {
        int databaseSizeBeforeCreate = customerPurchaseRepository.findAll().size();
        // Create the CustomerPurchase
        restCustomerPurchaseMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(customerPurchase))
            )
            .andExpect(status().isCreated());

        // Validate the CustomerPurchase in the database
        List<CustomerPurchase> customerPurchaseList = customerPurchaseRepository.findAll();
        assertThat(customerPurchaseList).hasSize(databaseSizeBeforeCreate + 1);
        CustomerPurchase testCustomerPurchase = customerPurchaseList.get(customerPurchaseList.size() - 1);
    }

    @Test
    @Transactional
    void createCustomerPurchaseWithExistingId() throws Exception {
        // Create the CustomerPurchase with an existing ID
        customerPurchase.setId(1L);

        int databaseSizeBeforeCreate = customerPurchaseRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomerPurchaseMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(customerPurchase))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerPurchase in the database
        List<CustomerPurchase> customerPurchaseList = customerPurchaseRepository.findAll();
        assertThat(customerPurchaseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCustomerPurchases() throws Exception {
        // Initialize the database
        customerPurchaseRepository.saveAndFlush(customerPurchase);

        // Get all the customerPurchaseList
        restCustomerPurchaseMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customerPurchase.getId().intValue())));
    }

    @Test
    @Transactional
    void getCustomerPurchase() throws Exception {
        // Initialize the database
        customerPurchaseRepository.saveAndFlush(customerPurchase);

        // Get the customerPurchase
        restCustomerPurchaseMockMvc
            .perform(get(ENTITY_API_URL_ID, customerPurchase.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(customerPurchase.getId().intValue()));
    }

    @Test
    @Transactional
    void getNonExistingCustomerPurchase() throws Exception {
        // Get the customerPurchase
        restCustomerPurchaseMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCustomerPurchase() throws Exception {
        // Initialize the database
        customerPurchaseRepository.saveAndFlush(customerPurchase);

        int databaseSizeBeforeUpdate = customerPurchaseRepository.findAll().size();

        // Update the customerPurchase
        CustomerPurchase updatedCustomerPurchase = customerPurchaseRepository.findById(customerPurchase.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedCustomerPurchase are not directly saved in db
        em.detach(updatedCustomerPurchase);

        restCustomerPurchaseMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCustomerPurchase.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCustomerPurchase))
            )
            .andExpect(status().isOk());

        // Validate the CustomerPurchase in the database
        List<CustomerPurchase> customerPurchaseList = customerPurchaseRepository.findAll();
        assertThat(customerPurchaseList).hasSize(databaseSizeBeforeUpdate);
        CustomerPurchase testCustomerPurchase = customerPurchaseList.get(customerPurchaseList.size() - 1);
    }

    @Test
    @Transactional
    void putNonExistingCustomerPurchase() throws Exception {
        int databaseSizeBeforeUpdate = customerPurchaseRepository.findAll().size();
        customerPurchase.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustomerPurchaseMockMvc
            .perform(
                put(ENTITY_API_URL_ID, customerPurchase.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(customerPurchase))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerPurchase in the database
        List<CustomerPurchase> customerPurchaseList = customerPurchaseRepository.findAll();
        assertThat(customerPurchaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCustomerPurchase() throws Exception {
        int databaseSizeBeforeUpdate = customerPurchaseRepository.findAll().size();
        customerPurchase.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerPurchaseMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(customerPurchase))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerPurchase in the database
        List<CustomerPurchase> customerPurchaseList = customerPurchaseRepository.findAll();
        assertThat(customerPurchaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCustomerPurchase() throws Exception {
        int databaseSizeBeforeUpdate = customerPurchaseRepository.findAll().size();
        customerPurchase.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerPurchaseMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(customerPurchase))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CustomerPurchase in the database
        List<CustomerPurchase> customerPurchaseList = customerPurchaseRepository.findAll();
        assertThat(customerPurchaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCustomerPurchaseWithPatch() throws Exception {
        // Initialize the database
        customerPurchaseRepository.saveAndFlush(customerPurchase);

        int databaseSizeBeforeUpdate = customerPurchaseRepository.findAll().size();

        // Update the customerPurchase using partial update
        CustomerPurchase partialUpdatedCustomerPurchase = new CustomerPurchase();
        partialUpdatedCustomerPurchase.setId(customerPurchase.getId());

        restCustomerPurchaseMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCustomerPurchase.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCustomerPurchase))
            )
            .andExpect(status().isOk());

        // Validate the CustomerPurchase in the database
        List<CustomerPurchase> customerPurchaseList = customerPurchaseRepository.findAll();
        assertThat(customerPurchaseList).hasSize(databaseSizeBeforeUpdate);
        CustomerPurchase testCustomerPurchase = customerPurchaseList.get(customerPurchaseList.size() - 1);
    }

    @Test
    @Transactional
    void fullUpdateCustomerPurchaseWithPatch() throws Exception {
        // Initialize the database
        customerPurchaseRepository.saveAndFlush(customerPurchase);

        int databaseSizeBeforeUpdate = customerPurchaseRepository.findAll().size();

        // Update the customerPurchase using partial update
        CustomerPurchase partialUpdatedCustomerPurchase = new CustomerPurchase();
        partialUpdatedCustomerPurchase.setId(customerPurchase.getId());

        restCustomerPurchaseMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCustomerPurchase.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCustomerPurchase))
            )
            .andExpect(status().isOk());

        // Validate the CustomerPurchase in the database
        List<CustomerPurchase> customerPurchaseList = customerPurchaseRepository.findAll();
        assertThat(customerPurchaseList).hasSize(databaseSizeBeforeUpdate);
        CustomerPurchase testCustomerPurchase = customerPurchaseList.get(customerPurchaseList.size() - 1);
    }

    @Test
    @Transactional
    void patchNonExistingCustomerPurchase() throws Exception {
        int databaseSizeBeforeUpdate = customerPurchaseRepository.findAll().size();
        customerPurchase.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustomerPurchaseMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, customerPurchase.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(customerPurchase))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerPurchase in the database
        List<CustomerPurchase> customerPurchaseList = customerPurchaseRepository.findAll();
        assertThat(customerPurchaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCustomerPurchase() throws Exception {
        int databaseSizeBeforeUpdate = customerPurchaseRepository.findAll().size();
        customerPurchase.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerPurchaseMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(customerPurchase))
            )
            .andExpect(status().isBadRequest());

        // Validate the CustomerPurchase in the database
        List<CustomerPurchase> customerPurchaseList = customerPurchaseRepository.findAll();
        assertThat(customerPurchaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCustomerPurchase() throws Exception {
        int databaseSizeBeforeUpdate = customerPurchaseRepository.findAll().size();
        customerPurchase.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerPurchaseMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(customerPurchase))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CustomerPurchase in the database
        List<CustomerPurchase> customerPurchaseList = customerPurchaseRepository.findAll();
        assertThat(customerPurchaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCustomerPurchase() throws Exception {
        // Initialize the database
        customerPurchaseRepository.saveAndFlush(customerPurchase);

        int databaseSizeBeforeDelete = customerPurchaseRepository.findAll().size();

        // Delete the customerPurchase
        restCustomerPurchaseMockMvc
            .perform(delete(ENTITY_API_URL_ID, customerPurchase.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CustomerPurchase> customerPurchaseList = customerPurchaseRepository.findAll();
        assertThat(customerPurchaseList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
