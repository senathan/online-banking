package com.sen.online.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sen.online.IntegrationTest;
import com.sen.online.domain.Merchants;
import com.sen.online.repository.MerchantsRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MerchantsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MerchantsResourceIT {

    private static final String ENTITY_API_URL = "/api/merchants";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MerchantsRepository merchantsRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMerchantsMockMvc;

    private Merchants merchants;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Merchants createEntity(EntityManager em) {
        Merchants merchants = new Merchants();
        return merchants;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Merchants createUpdatedEntity(EntityManager em) {
        Merchants merchants = new Merchants();
        return merchants;
    }

    @BeforeEach
    public void initTest() {
        merchants = createEntity(em);
    }

    @Test
    @Transactional
    void createMerchants() throws Exception {
        int databaseSizeBeforeCreate = merchantsRepository.findAll().size();
        // Create the Merchants
        restMerchantsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(merchants)))
            .andExpect(status().isCreated());

        // Validate the Merchants in the database
        List<Merchants> merchantsList = merchantsRepository.findAll();
        assertThat(merchantsList).hasSize(databaseSizeBeforeCreate + 1);
        Merchants testMerchants = merchantsList.get(merchantsList.size() - 1);
    }

    @Test
    @Transactional
    void createMerchantsWithExistingId() throws Exception {
        // Create the Merchants with an existing ID
        merchants.setId(1L);

        int databaseSizeBeforeCreate = merchantsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMerchantsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(merchants)))
            .andExpect(status().isBadRequest());

        // Validate the Merchants in the database
        List<Merchants> merchantsList = merchantsRepository.findAll();
        assertThat(merchantsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMerchants() throws Exception {
        // Initialize the database
        merchantsRepository.saveAndFlush(merchants);

        // Get all the merchantsList
        restMerchantsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(merchants.getId().intValue())));
    }

    @Test
    @Transactional
    void getMerchants() throws Exception {
        // Initialize the database
        merchantsRepository.saveAndFlush(merchants);

        // Get the merchants
        restMerchantsMockMvc
            .perform(get(ENTITY_API_URL_ID, merchants.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(merchants.getId().intValue()));
    }

    @Test
    @Transactional
    void getNonExistingMerchants() throws Exception {
        // Get the merchants
        restMerchantsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingMerchants() throws Exception {
        // Initialize the database
        merchantsRepository.saveAndFlush(merchants);

        int databaseSizeBeforeUpdate = merchantsRepository.findAll().size();

        // Update the merchants
        Merchants updatedMerchants = merchantsRepository.findById(merchants.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedMerchants are not directly saved in db
        em.detach(updatedMerchants);

        restMerchantsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMerchants.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMerchants))
            )
            .andExpect(status().isOk());

        // Validate the Merchants in the database
        List<Merchants> merchantsList = merchantsRepository.findAll();
        assertThat(merchantsList).hasSize(databaseSizeBeforeUpdate);
        Merchants testMerchants = merchantsList.get(merchantsList.size() - 1);
    }

    @Test
    @Transactional
    void putNonExistingMerchants() throws Exception {
        int databaseSizeBeforeUpdate = merchantsRepository.findAll().size();
        merchants.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMerchantsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, merchants.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(merchants))
            )
            .andExpect(status().isBadRequest());

        // Validate the Merchants in the database
        List<Merchants> merchantsList = merchantsRepository.findAll();
        assertThat(merchantsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMerchants() throws Exception {
        int databaseSizeBeforeUpdate = merchantsRepository.findAll().size();
        merchants.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMerchantsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(merchants))
            )
            .andExpect(status().isBadRequest());

        // Validate the Merchants in the database
        List<Merchants> merchantsList = merchantsRepository.findAll();
        assertThat(merchantsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMerchants() throws Exception {
        int databaseSizeBeforeUpdate = merchantsRepository.findAll().size();
        merchants.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMerchantsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(merchants)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Merchants in the database
        List<Merchants> merchantsList = merchantsRepository.findAll();
        assertThat(merchantsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMerchantsWithPatch() throws Exception {
        // Initialize the database
        merchantsRepository.saveAndFlush(merchants);

        int databaseSizeBeforeUpdate = merchantsRepository.findAll().size();

        // Update the merchants using partial update
        Merchants partialUpdatedMerchants = new Merchants();
        partialUpdatedMerchants.setId(merchants.getId());

        restMerchantsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMerchants.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMerchants))
            )
            .andExpect(status().isOk());

        // Validate the Merchants in the database
        List<Merchants> merchantsList = merchantsRepository.findAll();
        assertThat(merchantsList).hasSize(databaseSizeBeforeUpdate);
        Merchants testMerchants = merchantsList.get(merchantsList.size() - 1);
    }

    @Test
    @Transactional
    void fullUpdateMerchantsWithPatch() throws Exception {
        // Initialize the database
        merchantsRepository.saveAndFlush(merchants);

        int databaseSizeBeforeUpdate = merchantsRepository.findAll().size();

        // Update the merchants using partial update
        Merchants partialUpdatedMerchants = new Merchants();
        partialUpdatedMerchants.setId(merchants.getId());

        restMerchantsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMerchants.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMerchants))
            )
            .andExpect(status().isOk());

        // Validate the Merchants in the database
        List<Merchants> merchantsList = merchantsRepository.findAll();
        assertThat(merchantsList).hasSize(databaseSizeBeforeUpdate);
        Merchants testMerchants = merchantsList.get(merchantsList.size() - 1);
    }

    @Test
    @Transactional
    void patchNonExistingMerchants() throws Exception {
        int databaseSizeBeforeUpdate = merchantsRepository.findAll().size();
        merchants.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMerchantsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, merchants.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(merchants))
            )
            .andExpect(status().isBadRequest());

        // Validate the Merchants in the database
        List<Merchants> merchantsList = merchantsRepository.findAll();
        assertThat(merchantsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMerchants() throws Exception {
        int databaseSizeBeforeUpdate = merchantsRepository.findAll().size();
        merchants.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMerchantsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(merchants))
            )
            .andExpect(status().isBadRequest());

        // Validate the Merchants in the database
        List<Merchants> merchantsList = merchantsRepository.findAll();
        assertThat(merchantsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMerchants() throws Exception {
        int databaseSizeBeforeUpdate = merchantsRepository.findAll().size();
        merchants.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMerchantsMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(merchants))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Merchants in the database
        List<Merchants> merchantsList = merchantsRepository.findAll();
        assertThat(merchantsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMerchants() throws Exception {
        // Initialize the database
        merchantsRepository.saveAndFlush(merchants);

        int databaseSizeBeforeDelete = merchantsRepository.findAll().size();

        // Delete the merchants
        restMerchantsMockMvc
            .perform(delete(ENTITY_API_URL_ID, merchants.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Merchants> merchantsList = merchantsRepository.findAll();
        assertThat(merchantsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
