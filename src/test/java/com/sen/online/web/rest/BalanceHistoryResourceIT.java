package com.sen.online.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sen.online.IntegrationTest;
import com.sen.online.domain.BalanceHistory;
import com.sen.online.repository.BalanceHistoryRepository;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BalanceHistoryResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class BalanceHistoryResourceIT {

    private static final String DEFAULT_BALANCE_DATE = "AAAAAAAAAA";
    private static final String UPDATED_BALANCE_DATE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/balance-histories";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private BalanceHistoryRepository balanceHistoryRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBalanceHistoryMockMvc;

    private BalanceHistory balanceHistory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BalanceHistory createEntity(EntityManager em) {
        BalanceHistory balanceHistory = new BalanceHistory().balanceDate(DEFAULT_BALANCE_DATE);
        return balanceHistory;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BalanceHistory createUpdatedEntity(EntityManager em) {
        BalanceHistory balanceHistory = new BalanceHistory().balanceDate(UPDATED_BALANCE_DATE);
        return balanceHistory;
    }

    @BeforeEach
    public void initTest() {
        balanceHistory = createEntity(em);
    }

    @Test
    @Transactional
    void createBalanceHistory() throws Exception {
        int databaseSizeBeforeCreate = balanceHistoryRepository.findAll().size();
        // Create the BalanceHistory
        restBalanceHistoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(balanceHistory))
            )
            .andExpect(status().isCreated());

        // Validate the BalanceHistory in the database
        List<BalanceHistory> balanceHistoryList = balanceHistoryRepository.findAll();
        assertThat(balanceHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        BalanceHistory testBalanceHistory = balanceHistoryList.get(balanceHistoryList.size() - 1);
        assertThat(testBalanceHistory.getBalanceDate()).isEqualTo(DEFAULT_BALANCE_DATE);
    }

    @Test
    @Transactional
    void createBalanceHistoryWithExistingId() throws Exception {
        // Create the BalanceHistory with an existing ID
        balanceHistory.setId(1L);

        int databaseSizeBeforeCreate = balanceHistoryRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restBalanceHistoryMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(balanceHistory))
            )
            .andExpect(status().isBadRequest());

        // Validate the BalanceHistory in the database
        List<BalanceHistory> balanceHistoryList = balanceHistoryRepository.findAll();
        assertThat(balanceHistoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllBalanceHistories() throws Exception {
        // Initialize the database
        balanceHistoryRepository.saveAndFlush(balanceHistory);

        // Get all the balanceHistoryList
        restBalanceHistoryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(balanceHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].balanceDate").value(hasItem(DEFAULT_BALANCE_DATE)));
    }

    @Test
    @Transactional
    void getBalanceHistory() throws Exception {
        // Initialize the database
        balanceHistoryRepository.saveAndFlush(balanceHistory);

        // Get the balanceHistory
        restBalanceHistoryMockMvc
            .perform(get(ENTITY_API_URL_ID, balanceHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(balanceHistory.getId().intValue()))
            .andExpect(jsonPath("$.balanceDate").value(DEFAULT_BALANCE_DATE));
    }

    @Test
    @Transactional
    void getNonExistingBalanceHistory() throws Exception {
        // Get the balanceHistory
        restBalanceHistoryMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingBalanceHistory() throws Exception {
        // Initialize the database
        balanceHistoryRepository.saveAndFlush(balanceHistory);

        int databaseSizeBeforeUpdate = balanceHistoryRepository.findAll().size();

        // Update the balanceHistory
        BalanceHistory updatedBalanceHistory = balanceHistoryRepository.findById(balanceHistory.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedBalanceHistory are not directly saved in db
        em.detach(updatedBalanceHistory);
        updatedBalanceHistory.balanceDate(UPDATED_BALANCE_DATE);

        restBalanceHistoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedBalanceHistory.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedBalanceHistory))
            )
            .andExpect(status().isOk());

        // Validate the BalanceHistory in the database
        List<BalanceHistory> balanceHistoryList = balanceHistoryRepository.findAll();
        assertThat(balanceHistoryList).hasSize(databaseSizeBeforeUpdate);
        BalanceHistory testBalanceHistory = balanceHistoryList.get(balanceHistoryList.size() - 1);
        assertThat(testBalanceHistory.getBalanceDate()).isEqualTo(UPDATED_BALANCE_DATE);
    }

    @Test
    @Transactional
    void putNonExistingBalanceHistory() throws Exception {
        int databaseSizeBeforeUpdate = balanceHistoryRepository.findAll().size();
        balanceHistory.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBalanceHistoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, balanceHistory.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(balanceHistory))
            )
            .andExpect(status().isBadRequest());

        // Validate the BalanceHistory in the database
        List<BalanceHistory> balanceHistoryList = balanceHistoryRepository.findAll();
        assertThat(balanceHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchBalanceHistory() throws Exception {
        int databaseSizeBeforeUpdate = balanceHistoryRepository.findAll().size();
        balanceHistory.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBalanceHistoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(balanceHistory))
            )
            .andExpect(status().isBadRequest());

        // Validate the BalanceHistory in the database
        List<BalanceHistory> balanceHistoryList = balanceHistoryRepository.findAll();
        assertThat(balanceHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamBalanceHistory() throws Exception {
        int databaseSizeBeforeUpdate = balanceHistoryRepository.findAll().size();
        balanceHistory.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBalanceHistoryMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(balanceHistory)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the BalanceHistory in the database
        List<BalanceHistory> balanceHistoryList = balanceHistoryRepository.findAll();
        assertThat(balanceHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateBalanceHistoryWithPatch() throws Exception {
        // Initialize the database
        balanceHistoryRepository.saveAndFlush(balanceHistory);

        int databaseSizeBeforeUpdate = balanceHistoryRepository.findAll().size();

        // Update the balanceHistory using partial update
        BalanceHistory partialUpdatedBalanceHistory = new BalanceHistory();
        partialUpdatedBalanceHistory.setId(balanceHistory.getId());

        partialUpdatedBalanceHistory.balanceDate(UPDATED_BALANCE_DATE);

        restBalanceHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBalanceHistory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBalanceHistory))
            )
            .andExpect(status().isOk());

        // Validate the BalanceHistory in the database
        List<BalanceHistory> balanceHistoryList = balanceHistoryRepository.findAll();
        assertThat(balanceHistoryList).hasSize(databaseSizeBeforeUpdate);
        BalanceHistory testBalanceHistory = balanceHistoryList.get(balanceHistoryList.size() - 1);
        assertThat(testBalanceHistory.getBalanceDate()).isEqualTo(UPDATED_BALANCE_DATE);
    }

    @Test
    @Transactional
    void fullUpdateBalanceHistoryWithPatch() throws Exception {
        // Initialize the database
        balanceHistoryRepository.saveAndFlush(balanceHistory);

        int databaseSizeBeforeUpdate = balanceHistoryRepository.findAll().size();

        // Update the balanceHistory using partial update
        BalanceHistory partialUpdatedBalanceHistory = new BalanceHistory();
        partialUpdatedBalanceHistory.setId(balanceHistory.getId());

        partialUpdatedBalanceHistory.balanceDate(UPDATED_BALANCE_DATE);

        restBalanceHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBalanceHistory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBalanceHistory))
            )
            .andExpect(status().isOk());

        // Validate the BalanceHistory in the database
        List<BalanceHistory> balanceHistoryList = balanceHistoryRepository.findAll();
        assertThat(balanceHistoryList).hasSize(databaseSizeBeforeUpdate);
        BalanceHistory testBalanceHistory = balanceHistoryList.get(balanceHistoryList.size() - 1);
        assertThat(testBalanceHistory.getBalanceDate()).isEqualTo(UPDATED_BALANCE_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingBalanceHistory() throws Exception {
        int databaseSizeBeforeUpdate = balanceHistoryRepository.findAll().size();
        balanceHistory.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBalanceHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, balanceHistory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(balanceHistory))
            )
            .andExpect(status().isBadRequest());

        // Validate the BalanceHistory in the database
        List<BalanceHistory> balanceHistoryList = balanceHistoryRepository.findAll();
        assertThat(balanceHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchBalanceHistory() throws Exception {
        int databaseSizeBeforeUpdate = balanceHistoryRepository.findAll().size();
        balanceHistory.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBalanceHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(balanceHistory))
            )
            .andExpect(status().isBadRequest());

        // Validate the BalanceHistory in the database
        List<BalanceHistory> balanceHistoryList = balanceHistoryRepository.findAll();
        assertThat(balanceHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamBalanceHistory() throws Exception {
        int databaseSizeBeforeUpdate = balanceHistoryRepository.findAll().size();
        balanceHistory.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBalanceHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(balanceHistory))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the BalanceHistory in the database
        List<BalanceHistory> balanceHistoryList = balanceHistoryRepository.findAll();
        assertThat(balanceHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteBalanceHistory() throws Exception {
        // Initialize the database
        balanceHistoryRepository.saveAndFlush(balanceHistory);

        int databaseSizeBeforeDelete = balanceHistoryRepository.findAll().size();

        // Delete the balanceHistory
        restBalanceHistoryMockMvc
            .perform(delete(ENTITY_API_URL_ID, balanceHistory.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BalanceHistory> balanceHistoryList = balanceHistoryRepository.findAll();
        assertThat(balanceHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
